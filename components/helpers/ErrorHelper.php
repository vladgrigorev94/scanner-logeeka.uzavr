<?php


namespace app\components\helpers;


class ErrorHelper
{
    const SUCCESS = 0;
    const FAILURE = 1;

    public static function convertErrorsToString($errors)
    {
        $message = '';

        foreach ($errors as $errorText) {
            $message .= $errorText[0] . ' ';
        }

        return $message;
    }
}