<?php

namespace app\components\helpers;

use yii\base\BaseObject;

class HierarchySort extends BaseObject
{
    /**
     * @var array Массив записей
     */
    public $data = [];

    /**
     * @var string Префикс для подуровней
     */
    public $prefix = '   ';

    /**
     * @var string Наименование поля с названием текущей записи
     */
    public $fieldTitle = 'title';

    /**
     * @var string Наименование поля со ссылкой на радителя
     */
    public $fieldParent = 'parent_id';

    /**
     * @param $category_id
     * @param string $prefix
     * @return bool|string
     */
    protected function getPath($category_id, $prefix = '')
    {
        foreach ($this->data as $item) {
            if ($category_id === $item['id']) {
                $prefix = $prefix
                    ? $this->prefix . $prefix
                    : $item[$this->fieldTitle];

                if ($item[$this->fieldParent]) {
                    return $this->getPath($item[$this->fieldParent], $prefix);
                }

                return $prefix;
            }
        }
        return '';
    }

    /**
     * @param int $parent_id
     * @return array
     */
    public function getList($parent_id = 0)
    {
        $data = [];

        foreach ($this->data as $datum) {
            if ($parent_id == $datum[$this->fieldParent]) {
                $data[] = [
                    'id' => $datum['id'],
                    'name' => $this->getPath($datum['id'])
                ];
                $data = array_merge($data, $this->getList($datum['id']));
            }
        }

        return $data;
    }
}
