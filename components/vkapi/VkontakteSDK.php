<?php
namespace app\components\vkapi;

use Yii;

class VkontakteSDK
{
    private static $API_TOKEN;
    private static $API_DEFAULT_TOKEN = "ce9cece41f6a10407dc0688c7208edf35eb77794162b9dde753342df23f7398095cf1d05306a92ccc7d35";
    private static $API_REQUSET_ADDRESS = "https://api.vk.com/method/";
    private static $API_VERSION = "5.101";
    private static $API_EXTENDED = "1";
    private static $attempts = 20;
    private static $errorExceptions = [6,29]; // Попробовать еще раз
    private static $errorExceptionsBreak = [15,30]; // Доступ запрещен, нет смысла пробовать

    public static function makeQuery($methodName, $parameters = null)
    {
        if (!is_null($parameters)) {
            if (!self::isAssoc($parameters)) {
                return null;
            }
        } else {
            $parameters = array();
        }

        if (!self::$API_TOKEN) {
            self::getToken();
        }

        $attempts = self::$attempts;
        $curl = curl_init(self::$API_REQUSET_ADDRESS.$methodName);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        do {
            $parameters["access_token"] = self::$API_TOKEN;
            $parameters["extended"] = self::$API_EXTENDED;
            $parameters["v"] = self::$API_VERSION;
            $requestString = "";
            foreach($parameters as $key=>$value) {
                $requestString .= $key . "=" . urlencode($value) . "&";
            }
            curl_setopt($curl, CURLOPT_POSTFIELDS, $requestString);
            $resultObject = json_decode(curl_exec($curl));

            if (is_object($resultObject) &&
                property_exists($resultObject, 'error') &&
                in_array($resultObject->error->error_code, self::$errorExceptions))
            {
                self::getNewToken();
                $resultObject = false;
                usleep(500000);
            } elseif (is_object($resultObject) &&
                property_exists($resultObject, 'error') &&
                in_array($resultObject->error->error_code, self::$errorExceptionsBreak))
            {
                $resultObject = false;
                break;
            }  elseif (is_object($resultObject) &&
                property_exists($resultObject, 'error'))
            {
                $resultObject = false;
            }
            $attempts--;
        } while (!is_object($resultObject) && $attempts);
        curl_close($curl);
        if (is_object($resultObject) && property_exists($resultObject, 'response')) {
            return $resultObject->response;
        } else {
            return false;
        }
    }

    public static function makeExecute($code) {
        if (!$code) {
            return false;
        }
        if (!self::$API_TOKEN) {
            self::getToken();
        }
        $attempts = self::$attempts;
        $curl = curl_init(self::$API_REQUSET_ADDRESS."execute");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        do {
            curl_setopt($curl, CURLOPT_POSTFIELDS,
                "access_token=".self::$API_TOKEN.
                "&v=".self::$API_VERSION.
                "&code=".urlencode($code)
            );
            $resultObject = json_decode(curl_exec($curl));
            if (is_object($resultObject) &&
                property_exists($resultObject, 'error') &&
                in_array($resultObject->error->error_code, self::$errorExceptions))
            {
                self::getNewToken();
                $resultObject = false;
                usleep(500000);
            }
            $attempts--;
        } while (!is_object($resultObject) && $attempts);
        curl_close($curl);
        if (is_object($resultObject)) {
            return $resultObject;
        }
        return false;
    }

    private static function isAssoc($arr)
    {
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    private static function getToken()
    {
        $query = Yii::$app->db->createCommand("SELECT access_token FROM soc_networks_api_config WHERE (soc_network_id = 1) AND (active = 1)")->queryOne();
        self::$API_TOKEN = isset($query['access_token'])?$query['access_token']:self::$API_DEFAULT_TOKEN;
    }

    private static function getNewToken()
    {
        $tokens = [];
        $currentIndex = 0;
        $query = Yii::$app->db->createCommand("SELECT access_token FROM soc_networks_api_config WHERE soc_network_id = 1 ORDER BY id")->queryAll();
        if (is_array($query) && count($query)) {
            foreach ($query as $oneToken) {
                $tokens[] = $oneToken['access_token'];
                if ($oneToken['access_token'] == self::$API_TOKEN) {
                    $currentIndex = array_search(self::$API_TOKEN, $tokens);
                }
            }
            $newToken = isset($tokens[$currentIndex+1])?$tokens[$currentIndex+1]:$tokens[0];
            Yii::$app->db->createCommand("UPDATE soc_networks_api_config SET active = 1 WHERE (soc_network_id = 1) AND (access_token = '".$newToken."')")->execute();
            Yii::$app->db->createCommand("UPDATE soc_networks_api_config SET active = NULL WHERE (soc_network_id = 1) AND (access_token = '".self::$API_TOKEN."')")->execute();
        } else {
            $newToken = self::$API_DEFAULT_TOKEN;
        }

        self::$API_TOKEN = $newToken;
    }
}