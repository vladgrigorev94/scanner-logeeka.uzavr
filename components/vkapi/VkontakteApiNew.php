<?php

namespace app\components\vkapi;

use app\helpers\HelperFunctions;
use Yii;
use yii\db\Query;

class VkontakteApiNew
{
    const USERS_LIMIT = 1000;
    const POSTS_LIMIT = 100;
    const GROUPS_LIMIT = 1000;
    const FRIENDS_LIMIT = 5000;
    const FOLLOWERS_LIMIT = 1000;
    const EXECUTE_RATIO = 25;

    public static function executeUsersGet(array $userIds, array $fields = [])
    {
        if (!count($userIds))
            return false;

        $arUsers = array_chunk($userIds, self::USERS_LIMIT);
        $users = [];
        foreach ($arUsers as $usersArray) {
            $users[] = implode(',', $usersArray);
        }

        if (!count($users) || count($users) > self::EXECUTE_RATIO)
            return false;

        $arUsers = json_encode($users);

        return 'var result = [];
            var i = 0;
            var arUsers = '.$arUsers.';
            while (i < arUsers.length) {
                var request = API.users.get({"user_ids":arUsers[i],"fields":"'.implode(',', $fields).'"});
                if (request.length) {
                    result = result + request;
                    i = i + 1;
                } else {
                    return result;
                }
            };
            return result;';
    }

    public static function executeWallGet(int $userId, int $offset = 0)
    {
        if (!$userId)
            return false;

        return 'var result = [];
            var offset = '.$offset.';
            var maxOffset = offset + ('.self::POSTS_LIMIT.' * '.self::EXECUTE_RATIO.');
            while (offset < maxOffset) {
                var request = API.wall.get({"owner_id":'.$userId.',"count":'.self::POSTS_LIMIT.',"offset":offset,"extended":1}).items;
                if (request.length) {
                    result = result + request;
                    offset = offset + '.self::POSTS_LIMIT.';
                } else {
                    return result;
                }
            };
            return result;';
    }

    public static function executeGroupsGet(int $userId, int $offset = 0, array $fields = [])
    {
        if (!$userId)
            return false;

        return 'var result = [];
            var offset = '.$offset.';
            var maxOffset = offset + ('.self::GROUPS_LIMIT.' * '.self::EXECUTE_RATIO.');
            while (offset < maxOffset) {
                var request = API.groups.get({"user_id":'.$userId.',"count":'.self::GROUPS_LIMIT.',"offset":offset,"fields":"'.implode(',', $fields).'"}).items;
                if (request.length) {
                    result = result + request;
                    offset = offset + '.self::GROUPS_LIMIT.';
                } else {
                    return result;
                }
            };
            return result;';
    }

    public static function executeFriendsGet(int $userId, int $offset = 0, array $fields = [])
    {
        if (!$userId)
            return false;

        return 'var result = [];
            var offset = '.$offset.';
            var maxOffset = offset + ('.self::FRIENDS_LIMIT.' * '.self::EXECUTE_RATIO.');
            while (offset < maxOffset) {
                var request = API.friends.get({"user_id":'.$userId.',"count":'.self::FRIENDS_LIMIT.',"offset":offset,"fields":"'.implode(',', $fields).'"}).items;
                if (request.length) {
                    result = result + request;
                    offset = offset + '.self::FRIENDS_LIMIT.';
                } else {
                    return result;
                }
            };
            return result;';
    }

    public static function executeFollowersGet(int $userId, int $offset = 0, array $fields = [])
    {
        if (!$userId)
            return false;

        return 'var result = [];
            var offset = '.$offset.';
            var maxOffset = offset + ('.self::FOLLOWERS_LIMIT.' * '.self::EXECUTE_RATIO.');
            while (offset < maxOffset) {
                var request = API.users.getFollowers({"user_id":'.$userId.',"count":'.self::FOLLOWERS_LIMIT.',"offset":offset,"fields":"'.implode(',', $fields).'"}).items;
                if (request.length) {
                    result = result + request;
                    offset = offset + '.self::FOLLOWERS_LIMIT.';
                } else {
                    return result;
                }
            };
            return result;';
    }

    public static function getUser(int $userId, array $fields = [])
    {
        if (!$userId)
            return false;

        $result = VkontakteSDK::makeExecute(self::executeUsersGet([$userId], $fields));

        return property_exists($result,'response')?$result->response:false;
    }

    public static function getUsers(array $userIds, array $fields = [])
    {
        if (!is_array($userIds))
            return false;

        $result = [];
        $usersChunked = array_chunk($userIds, self::USERS_LIMIT * self::EXECUTE_RATIO);
        foreach ($usersChunked as $users) {
            $response = VkontakteSDK::makeExecute(self::executeUsersGet($users, $fields));
            $result = array_merge($result, property_exists($response,'response')?$response->response:[]);
        }

        return $result;
    }



    public static function loadVkUsers(array $userIds)
    {

        $arResult = [];
        $socNetworkId = 1;

        $resultQueryUsers = VkontakteSDK::makeQuery('users.get', [
            'user_ids' => implode(',', $userIds),
            'fields' => "photo_max,photo_max_orig"
        ]);

        if ($resultQueryUsers) {
            foreach ($resultQueryUsers as $oneData) {
                $temp = $tempIU = [];
                if (property_exists($oneData, 'photo_max_orig'))
                    $temp['photo_url'] = $tempIU['avatar'] = $oneData->photo_max_orig;
                if (property_exists($oneData, 'photo_max'))
                    $temp['avatar'] = $oneData->photo_max;
                if (property_exists($oneData, 'first_name'))
                    $temp['first_name'] = $oneData->first_name;
                else
                    $temp['first_name'] = '-';
                if (property_exists($oneData, 'last_name'))
                    $temp['last_name'] = $oneData->last_name;
                else
                    $temp['last_name'] = '-';
                if (property_exists($oneData, 'deactivated'))
                    $temp['deactivated'] = $oneData->deactivated;

                $tempIU['name'] = $temp['last_name']." ".$temp['first_name'];

                $temp['id'] = $oneData->id;

                $temp['soc'] = 'vk';
                $temp['soc_id'] = $socNetworkId;
                if (isset($temp['deactivated']) && $temp['deactivated'] == "deleted") {
                    $tempIU['active'] = 0;
                    $temp['first_name'] = "PAGE";
                    $temp['last_name'] = "DELETED";
                    $tempIU['name'] = $temp['last_name']."_".$temp['first_name'];
                    $temp['photo_url'] = $tempIU['avatar'] = $temp['avatar'] = '/resources/admin/img/noimg.png';
                    $temp['DELETED_PAGE'] = 1;
                }

                /*Internet-User Update*/
                Yii::$app->db->createCommand()->update('internet_users', $tempIU, '(soc_network_user_id = ' . $temp['id'] . ') AND (soc_network_id = 1)')->execute();

                $arResult[] = $temp;
            }

        }

        return $arResult;

    }

    public static function loadVkUser(int $userId, $arGroups = [], $arFriends = [], $arFollowers = [])
    {

        $arUser = $arResult = $extraData = [];
        $socNetworkId = 1;

        $resultQueryUser = VkontakteSDK::makeQuery('users.get', [
            'user_ids' => $userId,
            'fields' => "city,counters,education,career,bdate,photo_max,photo_max_orig,sex,connections,contacts"
        ]);

        if(is_array($resultQueryUser) && count($resultQueryUser)) {

            $oneData = reset($resultQueryUser);

            if (property_exists($oneData, 'photo_max_orig'))
                $arUser['photo_url'] = $arResult['internet_users']['avatar'] = $oneData->photo_max_orig;
            if (property_exists($oneData, 'photo_max'))
                $arUser['avatar'] = $oneData->photo_max;
            if (property_exists($oneData, 'first_name'))
                $arUser['first_name'] = $oneData->first_name;
            else
                $arUser['first_name'] = '-';
            if (property_exists($oneData, 'last_name'))
                $arUser['last_name'] = $oneData->last_name;
            else
                $arUser['last_name'] = '-';
            if (property_exists($oneData, 'deactivated'))
                $arUser['deactivated'] = $oneData->deactivated;
            if (property_exists($oneData, 'bdate')) {
                if (strlen($oneData->bdate) > 5 && $oneData->bdate != '0000-00-00') {
                    $arResult['internet_users']['bday'] = (strtotime($oneData->bdate)>-2051222400)?strtotime($oneData->bdate):'';
                    $arUser['bday'] = date('d.m.Y', strtotime($oneData->bdate));
                    $tmpBDay = explode(".", $arUser['bday']);
                    if (count($tmpBDay) == 3) {
                        $arUser['age'] = HelperFunctions::getFullYears($oneData->bdate);
                        $arUser['age_word'] = HelperFunctions::yearTextArg($arUser['age']);
                    }
                }
            }
            if (property_exists($oneData, 'city')) {
                $arUser['city'] = $oneData->city->title;
                $city_id = (new Query())->select([
                    '`ID`'
                ])->from('city')->where(['LIKE', '`title`', $arUser['city'].'%', false])->one();
                $arResult['internet_users']['city_id'] = isset($city_id['ID'])?$city_id['ID']:0;
            }
            if (property_exists($oneData, 'university') && ($oneData->university > 0)) {
                $extraData['univ_id'] = $oneData->university;
                $arUser['univer'] = $extraData['univ_name'] = $oneData->university_name;
            }
            if (property_exists($oneData, 'career') && count($oneData->career) > 0 && isset($oneData->career[0]->group_id))
                $extraData['job_id'] = $oneData->career[0]->group_id;
            if (property_exists($oneData, 'home_phone') && strlen($oneData->home_phone))
                $arUser['home_phone'] = $extraData['home_phone'] = $oneData->home_phone;
            if (property_exists($oneData, 'mobile_phone') && strlen($oneData->mobile_phone))
                $arUser['mobile_phone'] = $extraData['mobile_phone'] = $oneData->mobile_phone;
            if (property_exists($oneData, 'skype') && strlen($oneData->skype))
                $arUser['skype'] = $extraData['skype'] = $oneData->skype;
            if (property_exists($oneData, 'facebook') && strlen($oneData->facebook))
                $arUser['facebook'] = $extraData['facebook'] = $oneData->facebook;
            if (property_exists($oneData, 'livejournal') && strlen($oneData->livejournal))
                $arUser['livejournal'] = $extraData['livejournal'] = $oneData->livejournal;
            if (property_exists($oneData, 'twitter') && strlen($oneData->twitter))
                $arUser['twitter'] = $extraData['twitter'] = $oneData->twitter;
            if (property_exists($oneData, 'instagram') && strlen($oneData->instagram))
                $arUser['instagram'] = $extraData['instagram'] = $oneData->instagram;
            if (property_exists($oneData, 'sex')) {
                switch ($oneData->sex) {
                    case 1:
                        $sex = 'f';
                        break;
                    case 2:
                        $sex = 'm';
                        break;
                    default:
                        $sex = '';
                }
                $arResult['internet_users']['sex'] = $sex;
            }

            $arUser['id'] = $arResult['internet_users']['soc_network_user_id'] = $userId;
            $arResult['internet_users']['name'] = $arUser['last_name']." ".$arUser['first_name'];
            $arResult['internet_users']['soc_network_id'] = $socNetworkId;
            $arUser['soc'] = 'vk';
            $arUser['soc_id'] = $socNetworkId;

            $arUser['cnt_friends'] = $arResult['internet_users']['cnt_friends'] = count($arFriends)?count($arFriends):0;
            $arUser['cnt_followers'] = $arResult['internet_users']['cnt_followers'] = count($arFollowers)?count($arFollowers):0;
            if (isset($arUser['deactivated']) && $arUser['deactivated'] == "deleted") {
                $arResult['internet_users']['active'] = 0;
                $arResult['internet_users']['need_update'] = 0;
                $arUser['first_name'] = "PAGE";
                $arUser['last_name'] = "DELETED";
                $arResult['internet_users']['name'] = $arUser['last_name']."_".$arUser['first_name'];
                $arUser['photo_url'] = $arResult['internet_users']['avatar'] = $arUser['avatar'] = '/resources/admin/img/noimg.png';
                $arUser['DELETED_PAGE'] = 1;
            }

            /*Internet-User Save*/
            if (isset($arResult['internet_users']) && count($arResult['internet_users'])) {
                $checkInternetUser = Yii::$app->db->createCommand("SELECT id, groups_count, ext_data FROM internet_users WHERE (soc_network_user_id = " . $userId . ") AND (soc_network_id = 1)")->queryOne();
                if ($checkInternetUser && $checkInternetUser['id']) {
                    $currentExtraData = [];
                    if (!empty($checkInternetUser['ext_data'])) {
                        $currentExtraData = json_decode($checkInternetUser['ext_data'], true);
                    }
                    if (count($extraData)) {
                        $currentExtraData = array_merge($currentExtraData, $extraData);
                    }
                    if (count($currentExtraData)) {
                        $arResult['internet_users']['ext_data'] = json_encode($currentExtraData);
                    }
                    $arUser['groups_count'] = $checkInternetUser['groups_count']?$checkInternetUser['groups_count']:0;
                    Yii::$app->db->createCommand()->update('internet_users', $arResult['internet_users'], '(soc_network_user_id = ' . $userId . ') AND (soc_network_id = 1)')->execute();
                    $arUser['internet_user_id'] = $checkInternetUser['id'];
                } else {
                    $arResult['internet_users']['need_update'] = 1;
                    $arResult['internet_users']['active'] = 0;
                    if (count($extraData)) {
                        $arResult['internet_users']['ext_data'] = json_encode($extraData);
                    }
                    Yii::$app->db->createCommand()->insert('internet_users', $arResult['internet_users'])->execute();
                    $arUser['internet_user_id'] = Yii::$app->db->getLastInsertID();
                }
            }

            return $arUser;

        } else {

            return false;

        }
    }

    public static function loadVkGroups(int $userId, array $arExistGroups = [])
    {
        $offset = 0;
        $count = 1000;
        $arGroups = [];
        do {

            $resultQuery = VkontakteSDK::makeQuery('groups.get', [
                'user_id' => $userId,
                'offset' => $offset,
                'count' => $count,
                'fields' => "members_count"
            ]);

            if (is_object($resultQuery) &&
                property_exists($resultQuery, 'count') &&
                intval($resultQuery->count)) {
                $countQuery = intval($resultQuery->count);
            } else {
                $countQuery = 0;
            }
            if (is_object($resultQuery) &&
                property_exists($resultQuery, 'items') &&
                is_array($resultQuery->items) && count($resultQuery->items)) {

                $offset += $count;
                $items = $resultQuery->items;
                $itemsId = [];
                foreach ($items as $item) {
                    $itemsId[] = $item->id;
                }

                $checkPresentUserGroup = $checkPresentGroup = [];
                $queryPresent = Yii::$app->db->createCommand("SELECT vk_groups_id FROM vk_users_groups WHERE (vk_users_id = {$userId}) AND (vk_groups_id IN (" . implode(",", $itemsId) . "))")->queryAll();
                foreach ($queryPresent as $onePresent) {
                    $checkPresentUserGroup[] = $onePresent['vk_groups_id'];
                }
                $queryPresent = Yii::$app->db->createCommand("SELECT vk_id FROM vk_groups WHERE vk_id IN (" . implode(",", $itemsId) . ")")->queryAll();
                foreach ($queryPresent as $onePresentGroup) {
                    $checkPresentGroup[] = $onePresentGroup['vk_id'];
                }

                foreach ($items as $oneItem) {
                    if (!in_array($oneItem->id, $checkPresentUserGroup)) {
                        $temp = [];
                        $temp['vk_groups_id'] = $oneItem->id;
                        $temp['vk_users_id'] = $userId;
                        $temp['proc'] = 1;
                        $arResult['vk_users_groups'][$oneItem->id] = $temp;
                    }
                    if (!in_array($oneItem->id, $checkPresentGroup)) {
                        $temp = [];
                        $temp['vk_id'] = $oneItem->id;
                        $temp['title'] = HelperFunctions::cleanString($oneItem->name);
                        $temp['members'] = property_exists($oneItem, 'members_count')?$oneItem->members_count:0;
                        $temp['type'] = $oneItem->type;
                        $temp['date'] = time();
                        $arResult['vk_groups'][$oneItem->id] = $temp;
                    }
                    $arGroups[] = $oneItem->id;
                }
                unset($resultQuery, $queryPresent, $onePresent, $items, $oneItem);
            }
        } while ($offset < $countQuery);

        /*Groups Save*/
        if (isset($arResult['vk_groups']) && count($arResult['vk_groups'])) {
            $arInsertGroups = array_chunk($arResult['vk_groups'], 1000);
            foreach ($arInsertGroups as $groups) {
                foreach ($groups as &$oneGroup) {
                    $oneGroup = "(".$oneGroup['vk_id'].",'".$oneGroup['title']."',".$oneGroup['members'].",'".$oneGroup['type']."',".$oneGroup['date'].")";
                }
                Yii::$app->db->createCommand("INSERT INTO vk_groups (vk_id,title,members,type,date) VALUES ".implode(',', $groups))->execute();
            }
        }

        /*User-Groups Save*/
        if (count($arGroups)) {
            $delete = [];
            $queryDelete = Yii::$app->db->createCommand("SELECT id, vk_groups_id FROM vk_users_groups WHERE (vk_users_id = {$userId})")->queryAll();
            if (count($queryDelete)) {
                foreach ($queryDelete as $presentItem) {
                    if (!in_array($presentItem['vk_groups_id'], $arGroups)) {
                        $delete[] = $presentItem['id'];
                    }
                }
            }
            if (count($delete)) {
                Yii::$app->db->createCommand("DELETE FROM vk_users_groups WHERE id IN (".implode(',', $delete).")")->execute();
            }
        }

        if (count($arExistGroups)) {
            $checkExistPresentUserGroup = [];
            $queryExistPresent = Yii::$app->db->createCommand("SELECT vk_groups_id FROM vk_users_groups WHERE (vk_users_id = {$userId}) AND (vk_groups_id IN (" . implode(",", $arExistGroups) . "))")->queryAll();
            foreach ($queryExistPresent as $oneExistPresent) {
                $checkExistPresentUserGroup[] = $oneExistPresent['vk_groups_id'];
            }
            foreach ($arExistGroups as $oneExistGroup) {
                if (!in_array($oneExistGroup, $checkExistPresentUserGroup)) {
                    $temp = [];
                    $temp['vk_groups_id'] = $oneExistGroup;
                    $temp['vk_users_id'] = $userId;
                    $temp['proc'] = 1;
                    $arResult['vk_users_groups'][$oneExistGroup] = $temp;
                    $arGroups[] = $oneExistGroup;
                }
            }
        }
        if (isset($arResult['vk_users_groups']) && count($arResult['vk_users_groups'])) {
            $arInsertUsersGroups = array_chunk($arResult['vk_users_groups'], 1000);
            foreach ($arInsertUsersGroups as $usersGroups) {
                foreach ($usersGroups as &$oneUserGroup) {
                    $oneUserGroup = "(".implode(',', $oneUserGroup).")";
                }
                Yii::$app->db->createCommand("INSERT INTO vk_users_groups (vk_groups_id,vk_users_id,proc) VALUES ".implode(',', $usersGroups))->execute();
            }
        }

        return $arGroups;

    }

    public static function loadVkFriends(int $userId)
    {
        /*Friends Block*/
        $offset = 0;
        $count = 5000;
        $arFriends = [];
        do {

            $resultQuery = VkontakteSDK::makeQuery('friends.get', [
                'user_id' => $userId,
                'offset' => $offset,
                'count' => $count,
                'fields' => ""
            ]);

//            echo $userId." Friends API-Query ".(microtime(true)-$startTime)."\n";
//            $startTime = microtime(true);
//            if ($offset)
//                usleep(400000);

            if (is_object($resultQuery) &&
                property_exists($resultQuery, 'count') &&
                intval($resultQuery->count)) {
                $countQuery = intval($resultQuery->count);
            } else {
                $countQuery = 0;
            }
            if (is_object($resultQuery) &&
                property_exists($resultQuery, 'items') &&
                is_array($resultQuery->items) && count($resultQuery->items)) {

                $offset += $count;
                $items = $resultQuery->items;

                $checkPresent = [];
                $queryPresent = Yii::$app->db->createCommand("SELECT friend_id FROM vk_users_friends WHERE (user_id = {$userId}) AND (friend_id IN (" . implode(",", $items) . "))")->queryAll();
                foreach ($queryPresent as $onePresent) {
                    $checkPresent[] = $onePresent['friend_id'];
                }

                foreach ($items as $oneItem) {
                    if (!in_array($oneItem, $checkPresent)) {
                        $temp = [];
                        $temp['user_id'] = $userId;
                        $temp['friend_id'] = $oneItem;
                        $arResult['vk_users_friends'][$oneItem] = $temp;
                    }
                    $arFriends[] = $oneItem;
                }
                unset($resultQuery, $queryPresent, $onePresent, $items, $oneItem);

//                echo $userId." Friends Make Data ".(microtime(true)-$startTime)."\n";
//                $startTime = microtime(true);

            }
        } while ($offset < $countQuery);

        $checkAlignment = Yii::$app->db->createCommand("SELECT id FROM findface WHERE (soc_network = 'vk') AND (user_id = {$userId}) AND (alignment = 'alignment') LIMIT 1")->queryOne();
        if (isset($checkAlignment['id'])) {
            /*Friends Save*/
            if (count($arFriends)) {
                $delete = [];
                $queryDelete = Yii::$app->db->createCommand("SELECT id FROM vk_users_friends WHERE (user_id = {$userId}) AND (friend_id NOT IN (" . implode(",", $arFriends) . "))")->queryAll();
                foreach ($queryDelete as $deleteItem) {
                    $delete[] = $deleteItem['id'];
                }
                if (count($delete)) {
                    Yii::$app->db->createCommand("DELETE FROM vk_users_friends WHERE id IN (".implode(',', $delete).")")->execute();
                }
            }
            if (isset($arResult['vk_users_friends']) && count($arResult['vk_users_friends'])) {
                $arInsertUsersFriends = array_chunk($arResult['vk_users_friends'], 1000);
                foreach ($arInsertUsersFriends as $usersFriends) {
                    foreach ($usersFriends as &$oneUserFriend) {
                        $oneUserFriend = "(".implode(',', $oneUserFriend).")";
                    }
                    Yii::$app->db->createCommand("INSERT INTO vk_users_friends (user_id,friend_id) VALUES ".implode(',', $usersFriends))->execute();
                }
                unset($arResult['vk_users_friends']);
            }
        }

//        echo $userId." Save Friends ".(microtime(true)-$startTime)."\n";

        return $arFriends;

    }

    public static function loadVkFollowers(int $userId)
    {
//        $startTime = microtime(true);
        /*Follovers Block*/
        $offset = 0;
        $count = 1000;
        $arFollowers = [];
        do {

            $resultQuery = VkontakteSDK::makeQuery('users.getFollowers', [
                'user_id' => $userId,
                'offset' => $offset,
                'count' => $count,
                'fields' => ""
            ]);

//            echo $userId." Followers API-Query ".(microtime(true)-$startTime)."\n";
//            $startTime = microtime(true);
//            if ($offset)
//                usleep(400000);

            if (is_object($resultQuery) &&
                property_exists($resultQuery, 'count') &&
                intval($resultQuery->count)) {
                $countQuery = intval($resultQuery->count);
            } else {
                $countQuery = 0;
            }
            if (is_object($resultQuery) &&
                property_exists($resultQuery, 'items') &&
                is_array($resultQuery->items) && count($resultQuery->items)) {

                $offset += $count;
                $items = $resultQuery->items;

                $checkPresent = [];
                $queryPresent = Yii::$app->db->createCommand("SELECT follower_id FROM vk_users_followers WHERE (user_id = {$userId}) AND (follower_id IN (" . implode(",", $items) . "))")->queryAll();
                foreach ($queryPresent as $onePresent) {
                    $checkPresent[] = $onePresent['follower_id'];
                }

                foreach ($items as $oneItem) {
                    if (!in_array($oneItem, $checkPresent)) {
                        $temp = [];
                        $temp['user_id'] = $userId;
                        $temp['follower_id'] = $oneItem;
                        $arResult['vk_users_followers'][$oneItem] = $temp;
                    }
                    $arFollowers[] = $oneItem;
                }
                unset($resultQuery, $queryPresent, $onePresent, $items, $oneItem);

//                echo $userId." Followers Make Data ".(microtime(true)-$startTime)."\n";
//                $startTime = microtime(true);

            }
        } while ($offset < $countQuery);

        $checkAlignment = Yii::$app->db->createCommand("SELECT id FROM findface WHERE (soc_network = 'vk') AND (user_id = {$userId}) AND (alignment = 'alignment') LIMIT 1")->queryOne();
        if (isset($checkAlignment['id'])) {
            /*Followers Save*/
            if (count($arFollowers)) {
                $delete = [];
                $queryDelete = Yii::$app->db->createCommand("SELECT id FROM vk_users_followers WHERE (user_id = {$userId}) AND (follower_id NOT IN (" . implode(",", $arFollowers) . "))")->queryAll();
                foreach ($queryDelete as $deleteItem) {
                    $delete[] = $deleteItem['id'];
                }
                if (count($delete)) {
                    Yii::$app->db->createCommand("DELETE FROM vk_users_followers WHERE id IN (".implode(',', $delete).")")->execute();
                }
            }
            if (isset($arResult['vk_users_followers']) && count($arResult['vk_users_followers'])) {
                $arInsertUsersFollowers = array_chunk($arResult['vk_users_followers'], 1000);
                foreach ($arInsertUsersFollowers as $usersFollowers) {
                    foreach ($usersFollowers as &$oneUserFollower) {
                        $oneUserFollower = "(".implode(',', $oneUserFollower).")";
                    }
                    Yii::$app->db->createCommand("INSERT INTO vk_users_followers (user_id,follower_id) VALUES ".implode(',', $usersFollowers))->execute();
                }
                unset($arResult['vk_users_followers']);
            }
        }

//        echo $userId." Save Followers ".(microtime(true)-$startTime)."\n";

        return $arFollowers;

    }

    public static function getWatchedUserGroups(array $arGroups)
    {
        $groups = [];
        if (count($arGroups)) {
            $resReqGroups = Yii::$app->db->createCommand(
                "SELECT vk_id AS id, title, members FROM vk_groups WHERE watch = 1"
            )->queryAll();
            foreach ($resReqGroups as $oneRegGr) {
                if (in_array($oneRegGr['id'], $arGroups)) {
                    $groups[] = $oneRegGr;
                }
            }
        }

        return $groups;

    }

    public static function getCountFriendsInScanner(array $arFriends)
    {
        $friends = 0;
        if (count($arFriends)) {
            $resFriends = Yii::$app->db->createCommand("SELECT COUNT(id) AS CNT FROM internet_users WHERE (soc_network_id = 1) AND (soc_network_user_id IN (" . implode(",", $arFriends) . "))")->queryOne();
            $friends = intval($resFriends['CNT']);
        }

        return $friends;

    }

    public static function getCountFriendsInScannerAlignment(array $arFriends)
    {
        $friends = 0;
        if (count($arFriends)) {
            $resFriends = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT user_id) AS CNT FROM findface WHERE (soc_network = 'vk') AND (alignment = 'alignment') AND (user_id IN (" . implode(",", $arFriends) . "))")->queryOne();
            $friends = intval($resFriends['CNT']);
        }

        return $friends;

    }

    public static function loadUsers(array $userIds)
    {
        if (count($userIds) > 900) { // ограничение API(принудительно return чтобы можно было отловить блокировку с стороны API)
            return ['errors' => ['To many user IDs in one request!']];
        }

        $arResult = [];
        $socNetworkId = 1;

        $resultQueryUsers = VkontakteSDK::makeQuery('users.get', [
            'user_ids' => implode(',', $userIds),
            'fields' => "city,counters,education,career,bdate,photo_max,photo_max_orig,sex,connections,contacts"
        ]);

        if(is_array($resultQueryUsers) && count($resultQueryUsers)) {

            foreach ($resultQueryUsers as $oneData) {

                $arUser = $extraData = [];

                if (property_exists($oneData, 'photo_max_orig'))
                    $arUser['avatar'] = $oneData->photo_max_orig;
                elseif (property_exists($oneData, 'photo_max'))
                    $arUser['avatar'] = $oneData->photo_max;
                if (property_exists($oneData, 'first_name'))
                    $arUserFirstName = $oneData->first_name;
                else
                    $arUserFirstName = '-';
                if (property_exists($oneData, 'last_name'))
                    $arUserLastName = $oneData->last_name;
                else
                    $arUserLastName = '-';
                if (property_exists($oneData, 'deactivated'))
                    $arUser['deactivated'] = $oneData->deactivated;
                if (property_exists($oneData, 'bdate')) {
                    if (strlen($oneData->bdate) > 5 && $oneData->bdate != '0000-00-00') {
                        $arUser['bday'] = (strtotime($oneData->bdate)>-2051222400)?strtotime($oneData->bdate):'';
                    }
                }
                if (property_exists($oneData, 'city')) {
                    $city_id = (new Query())->select([
                        '`ID`'
                    ])->from('city')->where(['LIKE', '`title`', $oneData->city->title.'%', false])->one();
                    $arUser['city_id'] = isset($city_id['ID'])?$city_id['ID']:0;
                }
                if (property_exists($oneData, 'university') && ($oneData->university > 0)) {
                    $extraData['univ_id'] = $oneData->university;
                    $extraData['univer'] = $extraData['univ_name'] = $oneData->university_name;
                }
                if (property_exists($oneData, 'career') && count($oneData->career) > 0 && isset($oneData->career[0]->group_id))
                    $extraData['job_id'] = $oneData->career[0]->group_id;
                if (property_exists($oneData, 'home_phone') && strlen($oneData->home_phone))
                    $extraData['home_phone'] = $oneData->home_phone;
                if (property_exists($oneData, 'mobile_phone') && strlen($oneData->mobile_phone))
                    $extraData['mobile_phone'] = $oneData->mobile_phone;
                if (property_exists($oneData, 'skype') && strlen($oneData->skype))
                    $extraData['skype'] = $oneData->skype;
                if (property_exists($oneData, 'facebook') && strlen($oneData->facebook))
                    $extraData['facebook'] = $oneData->facebook;
                if (property_exists($oneData, 'livejournal') && strlen($oneData->livejournal))
                    $extraData['livejournal'] = $oneData->livejournal;
                if (property_exists($oneData, 'twitter') && strlen($oneData->twitter))
                    $extraData['twitter'] = $oneData->twitter;
                if (property_exists($oneData, 'instagram') && strlen($oneData->instagram))
                    $extraData['instagram'] = $oneData->instagram;
                if (property_exists($oneData, 'sex')) {
                    switch ($oneData->sex) {
                        case 1:
                            $sex = 'f';
                            break;
                        case 2:
                            $sex = 'm';
                            break;
                        default:
                            $sex = '';
                    }
                    $arUser['sex'] = $sex;
                }

                $arUser['name'] = HelperFunctions::replace4ByteCharacters($arUserLastName." ".$arUserFirstName);
                $arUser['soc_network_user_id'] = $oneData->id;
                $arUser['soc_network_id'] = $socNetworkId;
                $arUser['ext_data'] = $extraData;
                if (!isset($arUser['deactivated'])) {
                    $arResult[$arUser['soc_network_user_id']] = $arUser;
                }
            }

            /*Internet-Users Save*/
            if (count($arResult)) {

                $presentUsers = [];
                $checkInternetUser = Yii::$app->db->createCommand("SELECT id, soc_network_user_id, ext_data FROM internet_users WHERE (soc_network_user_id IN (" . implode(',', $userIds) . ")) AND (soc_network_id = {$socNetworkId})")->queryAll();

                if (count($checkInternetUser)) {
                    foreach ($checkInternetUser as $presentUser) {
                        $presentUsers[$presentUser['soc_network_user_id']] = $presentUser;
                    }
                }

                foreach ($arResult as $oneUserId => &$oneUser) {
                    if (isset($presentUsers[$oneUserId])) {
                        $currentExtraData = [];
                        if (!empty($presentUsers[$oneUserId]['ext_data'])) {
                            $currentExtraData = json_decode($presentUsers[$oneUserId]['ext_data'], true);
                        }
                        if (count($oneUser['ext_data'])) {
                            $currentExtraData = array_merge($currentExtraData, $oneUser['ext_data']);
                        }
                        if (count($currentExtraData)) {
                            $oneUser['ext_data'] = json_encode($currentExtraData);
                        } else {
                            unset($oneUser['ext_data']);
                        }
                        Yii::$app->db->createCommand()->update('internet_users', $oneUser, 'id = '.$presentUsers[$oneUserId]['id'])->execute();
                        $oneUser['internet_user_id'] = $presentUsers[$oneUserId]['id'];
                    } else {
                        $oneUser['need_update'] = 1;
                        $oneUser['active'] = 0;
                        if (count($oneUser['ext_data'])) {
                            $oneUser['ext_data'] = json_encode($oneUser['ext_data']);
                        } else {
                            unset($oneUser['ext_data']);
                        }
                        Yii::$app->db->createCommand()->insert('internet_users', $oneUser)->execute();
                        $oneUser['internet_user_id'] = Yii::$app->db->getLastInsertID();
                    }
                }
            }

            return $arResult;
        }

        return false;
    }

}