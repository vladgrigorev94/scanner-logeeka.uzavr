<?php

namespace app\components;

use SimpleXLSX;
use SocialApiLibrary\api\SocialApi;
use SocialApiLibrary\api\VkontakteApi;
use SocialApiLibrary\SocialFactory;

class ExcelParser
{
    const S1VK_WALL_VIDEO = 's1vk wall video';
    const S2VK_VIDEO = 's2vk video';
    const S3VK_PODCAST = 's3vk podcast';
    const S7VK_CLIP = 's7vk clip';
    const S8VK_CLIPS = 's8vk clips';
    const S10VK_STICK = 's10vk stick';
    const S11VK_GL_STR = 's11vk gl str';

    const TEMPLATES_PARSE_STRINGS = [
        self::S1VK_WALL_VIDEO => [
            'wall-',
            'getWallVideoPost'
        ],
        self::S2VK_VIDEO => [
            'video',
            'getVideo'
        ],
        self::S3VK_PODCAST => [
            'podcast-',
            'getPodcast'
        ],
        self::S7VK_CLIP => [
            'clip',
            'getVideo'
        ],
        self::S10VK_STICK => [
            'album',
            'getSticks'
        ],
        self::S8VK_CLIPS => [
            'z=clip',
            'getVideo'
        ],
    ];

    /**
     * @var VkontakteApi
     */
    private $vkSocial;

    private $data;

    public function __construct($token)
    {
        $this->vkSocial = SocialFactory::factory(
            SocialApi::ID_VK,
            [
                'access_token' => $token,
            ]
        );
    }

    public function parseAndGetFunctionsWithData(string $filepath): \Generator
    {
        if ($xlsx = SimpleXLSX::parse($filepath)) {
            foreach ($xlsx->rows() as $r => $row) {
                if ($r === 0) {
                    continue;
                }
                $url = $row[1];
                $detectUrlTemplate = '';
                $detectUrlTemplateValue = '';
                foreach (self::TEMPLATES_PARSE_STRINGS as $index => $item) {
                    if (strpos($url, $item[0])) {
                        $detectUrlTemplate = $index;
                        $detectUrlTemplateValue = $item;
                        break;
                    }
                }

                $this->resetData();

                if (!$detectUrlTemplateValue) {
                    yield [self::S11VK_GL_STR, $this->getGroup($this->getGroupId($url))];
                } else {
                    [$targetString, $function] = $this->getTargetStringAndFunction($url, $detectUrlTemplateValue);

                    yield [$detectUrlTemplate, $this->$function($targetString)];
                }
            }
        } else {
            echo SimpleXLSX::parseError();
        }
    }

    private function getGroupId($url): string
    {
        $explodeArray = explode('/', $url);

        return array_pop($explodeArray);
    }

    private function resetData()
    {
        $this->data = [
            'code' => VkontakteApi::TOO_MANY_REQUESTS_CODE
        ];
    }

    private function getTargetStringAndFunction($url, $detectUrlTemplateValue): array
    {
        $needle = $detectUrlTemplateValue[0];
        $pos = strpos($url, $needle) + strlen($needle);

        return [substr($url, $pos), $detectUrlTemplateValue[1]];
    }

    private function getWallVideoPost($targetString): array
    {
        list($ownerId, $postId) = explode('_', $targetString);

        while ($this->tooManyRequestsError()) {
            $this->data = $this->vkSocial->getGroupPostsById([
                'posts' => '-' . $ownerId . '_' . $postId,
            ]);
        }

        $this->data['postId'] = $postId;
        $this->data['ownerId'] = '-' . $ownerId;

        return $this->data;
    }

    private function getVideo($targetString): array
    {
        while ($this->tooManyRequestsError()) {
            $this->data = $this->vkSocial->getVideo([
                'videos' => $targetString,
            ]);
        }

        return $this->data;
    }

    private function getPodcast($targetString)
    {
        return null;
    }

    private function getSticks($targetString)
    {
        list($ownerId, $albumId) = explode('_', $targetString);

        while ($this->tooManyRequestsError()) {
            $this->data = $this->vkSocial->getPhotos([
                'owner_id' => $ownerId,
                'album_id' => $albumId,
            ]);
        }

        $this->data['albumId'] = $albumId;
        $this->data['ownerId'] = $ownerId;

        return $this->data;
    }

    private function getGroup($targetString)
    {
        while ($this->tooManyRequestsError()) {
            $this->data = $this->vkSocial->getGroupById([
                'group_id' => $targetString,
            ]);
        }

        return $this->data;
    }

    private function tooManyRequestsError(): bool
    {
        return $this->data['code'] === VkontakteApi::TOO_MANY_REQUESTS_CODE;
    }
}