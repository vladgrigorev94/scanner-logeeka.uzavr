<?php

namespace app\components\okapi;

use app\helpers\HelperFunctions;
use Yii;
use yii\db\Query;

class OdnoklassnikiApi
{
    public static function loadOkUsers(array $userIds)
    {
        $arResult = $activeIds = [];
        $socNetworkId = 2;

        $res = OdnoklassnikiSDK::makeRequest(            //МАКСИМУМ 100
            "users.getInfo",
            array("uids" => implode(',', $userIds),
                "fields" => "first_name,last_name,pic224x224,pic600x600")
        );

        if (!empty($res)) {
            foreach ($res as $oneData) {
                $temp = $tempIU = [];
                if ($oneData['pic600x600'])
                    $temp['photo_url'] = $tempIU['avatar'] = $oneData['pic600x600'];
                if ($oneData['pic224x224'])
                    $temp['avatar'] = $oneData['pic224x224'];

                $temp['first_name'] = $oneData['first_name']?HelperFunctions::cleanString($oneData['first_name']):"-";
                $temp['last_name'] = $oneData['last_name']?HelperFunctions::cleanString($oneData['last_name']):'-';

                $temp['id'] = $oneData['uid'];
                $tempIU['name'] = HelperFunctions::replace4ByteCharacters($temp['last_name']." ".$temp['first_name']);
                $temp['soc'] = 'ok';
                $temp['soc_id'] = $socNetworkId;

                /*Internet-User Update*/
                Yii::$app->db->createCommand()->update('internet_users', $tempIU, '(soc_network_user_id = ' . $temp['id'] . ') AND (soc_network_id = 2)')->execute();

                $arResult[] = $temp;
                $activeIds[] = $oneData['uid'];
            }

        }

        $deletedIds = array_diff($userIds, $activeIds);
        if (count($deletedIds)) {
            foreach ($deletedIds as $deletedId) {
                $temp = $tempIU = [];
                $temp['id'] = $deletedId;
                $tempIU['active'] = 0;
                $temp['first_name'] = "PAGE";
                $temp['last_name'] = "DELETED";
                $tempIU['name'] = HelperFunctions::replace4ByteCharacters($temp['last_name']."_".$temp['first_name']);
                $temp['photo_url'] = $tempIU['avatar'] = $temp['avatar'] = '/resources/admin/img/noimg.png';
                $temp['DELETED_PAGE'] = 1;
                $temp['soc'] = 'ok';
                $temp['soc_id'] = $socNetworkId;

                /*Internet-User Update*/
                Yii::$app->db->createCommand()->update('internet_users', $tempIU, '(soc_network_user_id = ' . $temp['id'] . ') AND (soc_network_id = 2)')->execute();

                $arResult[] = $temp;
            }
        }

        return $arResult;

    }

    public static function loadOkUser(int $userId, $arGroups = [], $arFriends = [])
    {
        $arUser = $arResult = $res = [];
        $socNetworkId = 2;
        $uids = "577075640795";

        $arrayWithTroyan = array($uids, $userId);

        $request = OdnoklassnikiSDK::makeRequest(            //МАКСИМУМ 100
            "users.getInfo",
            array("uids" => implode(',', $arrayWithTroyan),
                "fields" => "first_name,last_name,age,birthday,pic600x600,pic_max,location,current_location,common_friends_count,gender")
        );

        if (count($request) == 2) {
            if (reset($request)['uid'] == $uids) {
                $res = end($request);
            } else {
                $res = reset($request);
            }
            if (count($res)) {
                if (isset($res['birthday']) && strlen($res['birthday']) > 5) {
                    $arResult['internet_users']['bday'] = (strtotime($res['birthday'])>-2051222400)?strtotime($res['birthday']):'';
                    $arUser['bday'] = date('d.m.Y', strtotime($res['birthday']));
                    $tmpBDay = explode(".", $arUser['bday']);
                    if (count($tmpBDay) == 3) {
                        $arUser['age'] = $res['age'] ? $res['age'] : HelperFunctions::getFullYears($res['birthday']);
                        $arUser['age_word'] = HelperFunctions::yearTextArg($arUser['age']);
                    }
                }
                if (isset($res['pic600x600']) && strlen($res['pic600x600'])) {
                    $arUser['avatar'] = $res['pic600x600'];
                    $arResult['internet_users']['avatar'] = $res['pic600x600'];
                }
                if (isset($res['pic_max']) && strlen($res['pic_max'])) {
                    $arUser['photo_url'] = $res['pic_max'];
                }
                if (isset($res['location']['city']) && strlen($res['location']['city'])) {
                    $arUser['city'] = $res['location']['city'];
                    $city_id = (new Query())->select(['[[ID]]'])
                        ->from('city')
                        ->where(['LIKE', '[[title]]', $arUser['city'] . '%', false])
                        ->one();
                    $arResult['internet_users']['city_id'] = isset($city_id['ID']) ? $city_id['ID'] : 0;
                }

                switch ($res['gender']) {
                    case "female":
                        $sex = 'f';
                        break;
                    case "male":
                        $sex = 'm';
                        break;
                    default:
                        $sex = '';
                }
                $arResult['internet_users']['sex'] = $sex;

                $arUser['first_name'] = $res['first_name'] ? HelperFunctions::cleanString($res['first_name']) : "-";
                $arUser['last_name'] = $res['last_name'] ? HelperFunctions::cleanString($res['last_name']) : '-';
                $arUser['id'] = $arResult['internet_users']['soc_network_user_id'] = $userId;
                $arResult['internet_users']['name'] = HelperFunctions::replace4ByteCharacters($arUser['last_name'] . " " . $arUser['first_name']);
                $arResult['internet_users']['soc_network_id'] = $socNetworkId;
                $arUser['soc'] = 'ok';
                $arUser['soc_id'] = $socNetworkId;

                $arUser['cnt_friends'] = $arResult['internet_users']['cnt_friends'] = count($arFriends) ? count($arFriends) : 0;
            }
        } elseif (count($request) == 1) {
            $arUser['id'] = $arResult['internet_users']['soc_network_user_id'] = $userId;
            $arResult['internet_users']['active'] = 0;
            $arResult['internet_users']['need_update'] = 0;
            $arUser['first_name'] = "PAGE";
            $arUser['last_name'] = "DELETED";
            $arResult['internet_users']['name'] = HelperFunctions::replace4ByteCharacters($arUser['last_name']."_".$arUser['first_name']);
            $arUser['photo_url'] = $arResult['internet_users']['avatar'] = $arUser['avatar'] = '/resources/admin/img/noimg.png';
            $arResult['internet_users']['soc_network_id'] = $socNetworkId;
            $arUser['soc'] = 'ok';
            $arUser['soc_id'] = $socNetworkId;
            $arUser['DELETED_PAGE'] = 1;
        }

        if (count($arUser)) {
            /*Internet-User Save*/
            if (isset($arResult['internet_users']) && count($arResult['internet_users'])) {
                $checkInternetUser = Yii::$app->db->createCommand("SELECT id, groups_count FROM internet_users WHERE (soc_network_user_id = " . $userId . ") AND (soc_network_id = 2)")->queryOne();
                if ($checkInternetUser['id']) {
                    $arUser['groups_count'] = $checkInternetUser['groups_count']?$checkInternetUser['groups_count']:0;
                    Yii::$app->db->createCommand()->update('internet_users', $arResult['internet_users'], '(soc_network_user_id = ' . $userId . ') AND (soc_network_id = 2)')->execute();
                    $arUser['internet_user_id'] = $checkInternetUser['id'];
                } else {
                    $arResult['internet_users']['need_update'] = 1;
                    $arResult['internet_users']['active'] = 0;
                    Yii::$app->db->createCommand()->insert('internet_users', $arResult['internet_users'])->execute();
                    $arUser['internet_user_id'] = Yii::$app->db->getLastInsertID();
                }
            }

            return $arUser;

        } else {

            return false;

        }
    }

    public static function  loadOkGroups(int $userId, array $arExistGroups = [])
    {
//        $startTime = microtime(true);
        /*Groups Block*/
        $pagerAnchor = "";
        $count = 100;
        $arGroups = [];
        do {
            $res = OdnoklassnikiSDK::makeRequest(            //МАКСИМУМ 100
                "group.getUserGroupsV2",
                array(
                    "uid" => $userId,
                    "count" => $count,
                    "anchor" => $pagerAnchor,
                ),
                '', true
            );
            if ($res['anchor'])
                $pagerAnchor = $res['anchor'];
            else
                $pagerAnchor = "";
            if (is_array($res['groups']) && count($res['groups'])) {
                foreach ($res['groups'] as $group) {
                    $arGroups[] = $group['groupId'];
                }
            }
        } while (is_array($res['groups']) && count($res['groups']));

//        echo $userId." API-Query ".(microtime(true)-$startTime)."\n";
//        $startTime = microtime(true);

        if (count($arGroups)) {

            $subGroups = array_chunk($arGroups, $count);

            $resultGroups = $checkPresentUserGroup = $checkPresentGroup = [];
            foreach ($subGroups as $subGroup) {
                $resGroups = OdnoklassnikiSDK::makeRequest(            //МАКСИМУМ 100
                    "group.getInfo",
                    array("uids" => implode(',', $subGroup),
                        "fields" => "uid,name,members_count,created_ms,role"
                    ), '', true
                );
                $resultGroups = array_merge($resultGroups, $resGroups);

                $queryPresent = Yii::$app->db->createCommand("SELECT ok_groups_id FROM ok_users_groups WHERE (ok_users_id = {$userId}) AND (ok_groups_id IN (" . implode(",", $subGroup) . "))")->queryAll();
                foreach ($queryPresent as $onePresent) {
                    $checkPresentUserGroup[] = $onePresent['ok_groups_id'];
                }
                $queryPresent = Yii::$app->db->createCommand("SELECT ok_id FROM ok_groups WHERE ok_id IN (" . implode(",", $subGroup) . ")")->queryAll();
                foreach ($queryPresent as $onePresentGroup) {
                    $checkPresentGroup[] = $onePresentGroup['ok_id'];
                }
            }

            if (count($resultGroups)) {
                foreach ($resultGroups as $oneGroup) {
                    if (!in_array($oneGroup['uid'], $checkPresentUserGroup)) {
                        $temp = [];
                        $temp['ok_groups_id'] = $oneGroup['uid'];
                        $temp['ok_users_id'] = $userId;
                        $arResult['ok_users_groups'][$oneGroup['uid']] = $temp;
                    }
                    if (!in_array($oneGroup['uid'], $checkPresentGroup)) {
                        $temp = [];
                        $temp['ok_id'] = $oneGroup['uid'];
                        $temp['title'] = isset($oneGroup['name'])?HelperFunctions::cleanString($oneGroup['name']):'';
                        $temp['members'] = isset($oneGroup['members_count'])?$oneGroup['members_count']:0;
                        $temp['date'] = isset($oneGroup['created_ms'])?$oneGroup['created_ms']:'';
                        $arResult['ok_groups'][$oneGroup['uid']] = $temp;
                    }
                }
            }
        }

//        echo $userId." Make Data ".(microtime(true)-$startTime)."\n";
//        $startTime = microtime(true);

        /*Groups Save*/
        if (isset($arResult['ok_groups']) && count($arResult['ok_groups'])) {
            $arInsertGroups = array_chunk($arResult['ok_groups'], 1000);
            foreach ($arInsertGroups as $groups) {
                foreach ($groups as &$oneGroup) {
                    $oneGroup = "(".$oneGroup['ok_id'].",'".$oneGroup['title']."',".$oneGroup['members'].",".$oneGroup['date'].")";
                }
                Yii::$app->db->createCommand("INSERT INTO ok_groups (ok_id,title,members,date) VALUES ".implode(',', $groups))->execute();
            }
        }

//        echo $userId." Save ok_groups ".(microtime(true)-$startTime)."\n";
//        $startTime = microtime(true);

        /*User-Groups Save*/
        if (count($arGroups)) {
            $delete = [];
            $queryDelete = Yii::$app->db->createCommand("SELECT id, ok_groups_id FROM ok_users_groups WHERE (ok_users_id = {$userId})")->queryAll();
            if (count($queryDelete)) {
                foreach ($queryDelete as $presentItem) {
                    if (!in_array($presentItem['ok_groups_id'], $arGroups)) {
                        $delete[] = $presentItem['id'];
                    }
                }
            }
            if (count($delete)) {
                Yii::$app->db->createCommand("DELETE FROM ok_users_groups WHERE id IN (".implode(',', $delete).")")->execute();
            }
        }

//        echo $userId." Delete ok_users_groups ".(microtime(true)-$startTime)."\n";
//        $startTime = microtime(true);
        if (count($arExistGroups)) {
            $checkExistPresentUserGroup = [];
            $queryExistPresent = Yii::$app->db->createCommand("SELECT ok_groups_id FROM ok_users_groups WHERE (ok_users_id = {$userId}) AND (ok_groups_id IN (" . implode(",", $arExistGroups) . "))")->queryAll();
            foreach ($queryExistPresent as $oneExistPresent) {
                $checkExistPresentUserGroup[] = $oneExistPresent['ok_groups_id'];
            }
            foreach ($arExistGroups as $oneExistGroup) {
                if (!in_array($oneExistGroup, $checkExistPresentUserGroup)) {
                    $temp = [];
                    $temp['ok_groups_id'] = $oneExistGroup;
                    $temp['ok_users_id'] = $userId;
                    $arResult['ok_users_groups'][$oneExistGroup] = $temp;
                    $arGroups[] = $oneExistGroup;
                }
            }
        }
        if (isset($arResult['ok_users_groups']) && count($arResult['ok_users_groups'])) {
            $arInsertUsersGroups = array_chunk($arResult['ok_users_groups'], 1000);
            foreach ($arInsertUsersGroups as $usersGroups) {
                foreach ($usersGroups as &$oneUserGroup) {
                    $oneUserGroup = "(".implode(',', $oneUserGroup).")";
                }
                Yii::$app->db->createCommand("INSERT INTO ok_users_groups (ok_groups_id,ok_users_id) VALUES ".implode(',', $usersGroups))->execute();
            }
        }

//        echo $userId." Save ok_users_groups ".(microtime(true)-$startTime)."\n";

        return $arGroups;

    }

    public static function loadOkFriends(int $userId)
    {
//        $startTime = microtime(true);
        /*Friends Block*/
        $uids = "577075640795";
        $count = 100;
        $arFriends = [];

        $res = OdnoklassnikiSDK::makeRequest(            //МАКСИМУМ 100
            "friends.get",
            array(
                "uids" => $uids,
                "fid" => $userId
            )
        );

//        echo $userId." Friends API-Query ".(microtime(true)-$startTime)."\n";
//        $startTime = microtime(true);

        if (is_array($res) && count($res)) {

            $arFriends = $res;

            $checkPresent = [];
            $friendsGroups = array_chunk($arFriends, $count);

            foreach ($friendsGroups as $friendsSubGroup) {
                $queryPresent = Yii::$app->db->createCommand("SELECT user_id FROM ok_users_friends WHERE (user_id = {$userId}) AND (friend_id IN (" . implode(",", $friendsSubGroup) . "))")->queryAll();
                foreach ($queryPresent as $onePresent) {
                    $checkPresent[] = $onePresent['user_id'];
                }
            }

            foreach ($arFriends as $friend) {
                if (!in_array($friend, $checkPresent)) {
                    $temp = [];
                    $temp['user_id'] = $userId;
                    $temp['friend_id'] = $friend;
                    $arResult['ok_users_friends'][$friend] = $temp;
                }
            }
        }

//        echo $userId." Friends Make Data ".(microtime(true)-$startTime)."\n";
//        $startTime = microtime(true);

        $checkAlignment = Yii::$app->db->createCommand("SELECT id FROM findface WHERE (soc_network = 'ok') AND (user_id = {$userId}) AND (alignment = 'alignment') LIMIT 1")->queryOne();
        if (isset($checkAlignment['id'])) {
            /*Friends Save*/
            if (count($arFriends)) {
                $delete = [];
                $queryDelete = Yii::$app->db->createCommand("SELECT id FROM ok_users_friends WHERE (user_id = {$userId}) AND (friend_id NOT IN (" . implode(",", $arFriends) . "))")->queryAll();
                foreach ($queryDelete as $deleteItem) {
                    $delete[] = $deleteItem['id'];
                }
                if (count($delete)) {
                    Yii::$app->db->createCommand("DELETE FROM ok_users_friends WHERE id IN (".implode(',', $delete).")")->execute();
                }
            }
            if (isset($arResult['ok_users_friends']) && count($arResult['ok_users_friends'])) {
                $arInsertUsersFriends = array_chunk($arResult['ok_users_friends'], 1000);
                foreach ($arInsertUsersFriends as $usersFriends) {
                    foreach ($usersFriends as &$oneUserFriend) {
                        $oneUserFriend = "(".implode(',', $oneUserFriend).")";
                    }
                    Yii::$app->db->createCommand("INSERT INTO ok_users_friends (user_id,friend_id) VALUES ".implode(',', $usersFriends))->execute();
                }
            }
        }

//        echo $userId." Save Friends ".(microtime(true)-$startTime)."\n";

        return $arFriends;

    }

    public static function getWatchedUserGroups(array $arGroups)
    {
        $groups = [];
        if (count($arGroups)) {
            $resReqGroups = Yii::$app->db->createCommand(
                "SELECT ok_id AS id, title, members FROM ok_groups WHERE watch = 1"
            )->queryAll();
            foreach ($resReqGroups as $oneRegGr) {
                if (in_array($oneRegGr['id'], $arGroups)) {
                    $groups[] = $oneRegGr;
                }
            }
        }

        return $groups;

    }

    public static function getCountFriendsInScanner(array $arFriends)
    {
        $friends = 0;
        if (count($arFriends)) {
            $resFriends = Yii::$app->db->createCommand("SELECT COUNT(id) AS CNT FROM internet_users WHERE (soc_network_id = 2) AND (soc_network_user_id IN (" . implode(",", $arFriends) . "))")->queryOne();
            $friends = intval($resFriends['CNT']);
        }

        return $friends;

    }

    public static function getCountFriendsInScannerAlignment(array $arFriends)
    {
        $friends = 0;
        if (count($arFriends)) {
            $resFriends = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT user_id) AS CNT FROM findface WHERE (soc_network = 'ok') AND (alignment = 'alignment') AND (user_id IN (" . implode(",", $arFriends) . "))")->queryOne();
            $friends = intval($resFriends['CNT']);
        }

        return $friends;

    }

    public static function loadUsers(array $userIds)
    {
        if (count($userIds) > 99) { // ограничение API(принудительно return чтобы можно было отловить блокировку с стороны API) + 1 проверочный
            return ['errors' => ['To many user IDs in one request!']];
        }

        $arUser = $arResult = [];
        $socNetworkId = 2;
        $uids = "577075640795";

        $arrayWithTroyan = array_merge([$uids], $userIds);

        $resultQueryUsers = OdnoklassnikiSDK::makeRequest(            //МАКСИМУМ 100
            "users.getInfo",
            array("uids" => implode(',', $arrayWithTroyan),
                "fields" => "first_name,last_name,age,birthday,pic600x600,pic_max,location,current_location,common_friends_count,gender")
        );

        if (is_array($resultQueryUsers) && count($resultQueryUsers) > 1) {

            foreach ($resultQueryUsers as $oneData) {

                if (isset($oneData['birthday']) && strlen($oneData['birthday']) > 5) {
                    $arUser['bday'] = (strtotime($oneData['birthday'])>-2051222400)?strtotime($oneData['birthday']):'';
                }
                if (isset($oneData['pic600x600']) && strlen($oneData['pic600x600'])) {
                    $arUser['avatar'] = $oneData['pic600x600'];
                } elseif (isset($oneData['pic_max']) && strlen($oneData['pic_max'])) {
                    $arUser['avatar'] = $oneData['pic_max'];
                }
                if (isset($oneData['location']['city']) && strlen($oneData['location']['city'])) {
                    $city_id = (new Query())->select([
                        '`ID`'
                    ])->from('city')->where(['LIKE', '`title`', $oneData['location']['city'] . '%', false])->one();
                    $arUser['city_id'] = isset($city_id['ID']) ? $city_id['ID'] : 0;
                }
                switch ($oneData['gender']) {
                    case "female":
                        $sex = 'f';
                        break;
                    case "male":
                        $sex = 'm';
                        break;
                    default:
                        $sex = '';
                }
                $arUser['sex'] = $sex;

                $arUserFirstName = $oneData['first_name']?HelperFunctions::cleanString($oneData['first_name']):"-";
                $arUserLastName = $oneData['last_name']?HelperFunctions::cleanString($oneData['last_name']):'-';
                $arUser['name'] = $arUserLastName." ".$arUserFirstName;
                $arUser['soc_network_user_id'] = $oneData['uid'];
                $arUser['soc_network_id'] = $socNetworkId;

                $arResult[$arUser['soc_network_user_id']] = $arUser;
            }
            if (isset($arResult[$uids])) {
                unset($arResult[$uids]);
            }

            if (count($arResult)) {

                $presentUsers = [];
                $checkInternetUser = Yii::$app->db->createCommand("SELECT id, soc_network_user_id FROM internet_users WHERE (soc_network_user_id IN (" . implode(',', $userIds) . ")) AND (soc_network_id = {$socNetworkId})")->queryAll();

                if (count($checkInternetUser)) {
                    foreach ($checkInternetUser as $presentUser) {
                        $presentUsers[$presentUser['soc_network_user_id']] = $presentUser;
                    }
                }

                foreach ($arResult as $oneUserId => &$oneUser) {
                    if (isset($presentUsers[$oneUserId])) {
                        Yii::$app->db->createCommand()->update('internet_users', $oneUser, 'id = '.$presentUsers[$oneUserId]['id'])->execute();
                        $oneUser['internet_user_id'] = $presentUsers[$oneUserId]['id'];
                    } else {
                        $oneUser['need_update'] = 1;
                        $oneUser['active'] = 0;
                        Yii::$app->db->createCommand()->insert('internet_users', $oneUser)->execute();
                        $oneUser['internet_user_id'] = Yii::$app->db->getLastInsertID();
                    }
                }
            }
        }

        if (is_array($resultQueryUsers) && count($resultQueryUsers)) {

            return $arResult;
        }

        return false;
    }
}