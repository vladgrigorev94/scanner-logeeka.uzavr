<?php

namespace app\components\socialapi;

use app\models\data\TempNewInternetUsers;
use app\models\data\InternetUser;
use app\components\okapi\OdnoklassnikiApi;
use yii\db\Query;

class Odnoklassniki implements SocialInterface
{
    public function checkAndCopyInternetUsersIdsToFindface(array $userIds): array
    {
        // TODO: Implement checkAndCopyInternetUsersIdsToFindface() method.

        $toFitdface = [];

        $arUserIdsChunked = array_chunk($userIds, 99); // Лимит API OK + 1 проверочный ИД

        foreach ($arUserIdsChunked as $arUserIds) {

            $presentUsers = $needToGet = [];

            $makeQuery = (new Query())->select(['id','soc_network_user_id'])
                ->from(InternetUser::tableName())
                ->where([
                    'IN',
                    'soc_network_user_id',
                    $arUserIds
                ])->andWhere([
                    'soc_network_id' => InternetUser::SOC_NETWORK_ID_OK
                ])->all();

            foreach ($makeQuery as $oneUser) {
                $presentUsers[] = $oneUser['soc_network_user_id'];
                $toFitdface[$oneUser['soc_network_user_id']] = $oneUser['id'];
            }
            $needToGet = array_diff($arUserIds, $presentUsers);

            if (count($needToGet)) {
                $arUsers = OdnoklassnikiApi::loadUsers($needToGet);
                if (is_array($arUsers) && !isset($arUsers['errors'])) {
                    foreach ($needToGet as $oneUser) {
                        $toFitdface[$oneUser] = isset($arUsers[$oneUser]['internet_user_id'])?$arUsers[$oneUser]['internet_user_id']:0;
                    }
                    foreach ($arUsers as $user) {
                        $newInternetUser = new TempNewInternetUsers();
                        $newInternetUser->internet_user_id = $user['internet_user_id'];
                        $newInternetUser->save();
                    }
                }
            }
        }

        return $toFitdface;
    }

    public function getNewInternetUsersGroupsFriendsFollowers(array $userIds): array
    {
        $successAddedIds = [];
        if (count($userIds)) {
            foreach ($userIds as $userId) {
                $arGroups = OdnoklassnikiApi::loadOkGroups($userId);
                $arFriends = OdnoklassnikiApi::loadOkFriends($userId);
                //if (count($arGroups) || count($arFriends)) {
                $successAddedIds[] = $userId;
                //}
            }
        }

        return $successAddedIds;
    }

    public function getInternetUsers(array $userIds): array
    {
        $successAddedIds = [];
        if (count($userIds)) {
            foreach ($userIds as $userId) {
                $arGroups = OdnoklassnikiApi::loadOkGroups($userId);
                $arFriends = OdnoklassnikiApi::loadOkFriends($userId);
                $arUser = OdnoklassnikiApi::loadOkUser($userId, $arGroups, $arFriends);
                if ($arUser) {
                    $successAddedIds[$userId] = $arUser['internet_user_id'];
                } else {
                    $successAddedIds[$userId] = false;
                }
            }
        }

        return $successAddedIds;
    }
}