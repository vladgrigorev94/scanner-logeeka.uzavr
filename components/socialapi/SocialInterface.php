<?php

namespace app\components\socialapi;

interface SocialInterface
{
    public function checkAndCopyInternetUsersIdsToFindface(array $userIds): array;

    public function getNewInternetUsersGroupsFriendsFollowers(array $userIds): array;

    public function getInternetUsers(array $userIds): array;
}