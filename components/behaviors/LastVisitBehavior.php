<?php

namespace app\components\behaviors;

use app\models\data\User;
use Yii;
use yii\base\Behavior;
use yii\web\Controller;

class LastVisitBehavior extends Behavior
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Controller::EVENT_AFTER_ACTION => 'updateLastVisit',
        ];
    }

    public function updateLastVisit()
    {
        if (Yii::$app->user->isGuest) {
            return;
        }

        User::setLastVisit(Yii::$app->user->identity->id);
    }
}
