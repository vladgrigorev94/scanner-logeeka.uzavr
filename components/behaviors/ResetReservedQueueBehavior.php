<?php

namespace app\components\behaviors;

use app\jobs\interfaces\RetryChainable;
use yii\base\Behavior;
use yii\queue\ExecEvent;
use yii\queue\JobInterface;
use yii\queue\Queue;

class ResetReservedQueueBehavior extends Behavior
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Queue::EVENT_AFTER_ERROR => 'resetReserved',
        ];
    }

    /**
     * @param ExecEvent $event
     */
    public function resetReserved(ExecEvent $event)
    {
        /** @var JobInterface $job */
        $job = $event->job;
        if ($job instanceof RetryChainable) {
            $job->afterError(
                sprintf(
                    '%s %s %s',
                    'Ошибка на попытке',
                    $job->getAttemptCount(),
                    $event->error !== null
                        ? $event->error->getMessage()
                        : ''
                )
            );

            if ($job->checkCanRetry()) {
                $event->retry = false;
                $queue = $event->sender;
                $queue->delay($job->getDelay())->push($job);
            }
        }
    }
}
