<?php

namespace app\components;

use app\models\data\Photos;
use app\models\data\Posts;
use app\models\data\VkGroups;
use SocialApiLibrary\api\SocialApi;

class SaveVkInfoToDb
{
    const HTTP_STATUS_CODE_OK = 200;

    const FUNCTIONS_ARRAY = [
        ExcelParser::S1VK_WALL_VIDEO => 'savePost',
        ExcelParser::S2VK_VIDEO => 'saveVideo',
        ExcelParser::S7VK_CLIP => 'saveVideo',
        ExcelParser::S10VK_STICK => 'saveSticks',
        ExcelParser::S11VK_GL_STR => 'saveGroup',
    ];

    public function selectFunction($funcStrIndex, $data)
    {
        $res = false;
        $funcStr = self::FUNCTIONS_ARRAY[$funcStrIndex];

        if ($data['code'] && $data['code'] === self::HTTP_STATUS_CODE_OK) {
            if ($data['result']) {

                if (!empty($data['result'][0])) {
                    $result = $data['result'][0];
                } else {
                    $result = $data['result'];
                }

                $res = $this->$funcStr($data, $result);
            }
        }

        return $res;
    }

    private function savePost($data, $result)
    {
        return Posts::saveDataByPostId(
            $data['postId'],
            $result['attachments'] ?? [],
            [
                'comments' => $result['comments']['count'],
                'likes' => $result['likes']['count'],
                'reposts' => $result['reposts']['count'],
                'views' => $result['views']['count'],
                'owner_id' => $data['ownerId'],
                'date' => $result['date'],
                'soc_network_id' => SocialApi::ID_VK,
                'group_id' => $data['ownerId'],
                'post_text' => $result['text'],
                'is_parent' => 0,
            ],
            1,
            $result
        );
    }

    private function saveVideo($data, $result)
    {
        if (count($result['items']) > 0) {
            $item = $result['items'][0];

            return Posts::saveDataByPostId(
                $data['postId'] ?? $item['owner_id'] ?? $item['from_id'],
                [
                    'video_views' => 0,
                    'comments' => $item['comments'] ?? 0,
                    'likes' => $item['likes'] ? $item['likes']['count'] : '',
                    'reposts' => $item['reposts'] ? $item['reposts']['count'] : '',
                    'views' => $item['views'] ?? '',
                    'owner_id' => $item['owner_id'] ?? '',
                    'date' => $item['date'] ?? '',
                    'soc_network_id' => SocialApi::ID_VK,
                    'group_id' => $item['owner_id'] ?? '',
                    'post_text' => $item['description'] ?? '',
                    'is_parent' => 0,
                ]
            );
        } else {
            return false;
        }
    }

    private function saveSticks($data, $result)
    {
        if (!empty($result['items'])) {
            $items = $result['items'];

            return Photos::savePhotos($items);
        } else {
            return false;
        }
    }

    private function saveGroup($data, $result)
    {
        return VkGroups::saveGroup($result);
    }
}
