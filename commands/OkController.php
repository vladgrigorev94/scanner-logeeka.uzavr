<?php

namespace app\commands;

use SocialApiLibrary\api\SocialApi;
use SocialApiLibrary\SocialFactory;
use yii\console\ExitCode;

class OkController extends SocialController
{
    private $okSocial;

    public function init()
    {
        parent::init();
        $this->okSocial = SocialFactory::factory(SocialApi::ID_OK, [
            'id' => 3,
            'app_id' => '512000051505',
            'app_public_key' => 'CCDKMFJGDIHBABABA',
            'app_secret_key' => 'CF213F6D9C94D89966E39558',
            'access_token' => 'tkn1YBPEYmBf2HJ7RKvoQgDkepMHovgEUcY3PgOORZbGQwgl9ec73ihykcwCduWFvLEZH',
            'refresh_token' => '0201875532046c5186f8e24a640667b0',
        ]);
        $this->okSocial->setOffsetAndCount($this->offset, $this->count);
    }

    public function actionGetGroupUsers(int $groupId)
    {
        $groupsUsers = $this->okSocial->getGroupUsers(['uid' => $groupId]);
        var_dump($groupsUsers);
        return $groupsUsers;
    }

    public function actionGetUserInfo(int $userId)
    {
        $users = $this->okSocial->getUserInfo(['uids' => [$userId]]);
        var_dump($users);
        return $users;
    }

    public function actionGetUserGroups(int $userId)
    {
        $users = $this->okSocial->getUserGroups(['uid' => $userId]);
        var_dump($users);
        return $users;
    }

    public function actionGetUserFriends(int $userId)
    {
        $users = $this->okSocial->getUserFriends([
            'uid' => $userId,
            'fid' => $userId,
        ]);
        var_dump($users);
        return $users;
    }


    public function actionGetUserFollowers(int $userId)
    {
        $this->okSocial->getUserFollowers([]);

        return ExitCode::UNSPECIFIED_ERROR;
    }

    public function actionGetGroupPosts(int $groupId)
    {
        $this->okSocial->getGroupPosts([]);

        return ExitCode::UNSPECIFIED_ERROR;
    }

    public function actionGetPostLikes(int $groupId, int $postId)
    {
        $this->okSocial->getPostLikes([]);

        return ExitCode::UNSPECIFIED_ERROR;
    }

    public function actionGetPostComments(int $groupId, int $postId)
    {
        $this->okSocial->getPostComments([]);

        return ExitCode::UNSPECIFIED_ERROR;
    }

    public function actionGetPostAttachments(int $groupId, int $postId)
    {
        $this->okSocial->getPostAttachments([]);

        return ExitCode::UNSPECIFIED_ERROR;
    }

    public function actionGetAlbums(int $ownerId)
    {
        $albums = $this->okSocial->getAlbums([
            'fid' => $ownerId,
        ]);
        var_dump($albums);
        return $albums;
    }

    public function actionGetPhotosByAlbumId(int $ownerId, int $albumId)
    {
        $photos = $this->okSocial->getPhotos([
            'fid' => $ownerId,
            'aid' => $albumId,
        ]);
        var_dump($photos);
        return $photos;
    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        return $result['code'] === 200 ? ExitCode::OK : ExitCode::UNSPECIFIED_ERROR;
    }
}
