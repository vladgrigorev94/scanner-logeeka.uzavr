<?php /** @noinspection PhpUnused */

namespace app\commands;

use app\models\data\InternetUsersStatistics;
use app\models\data\TempUsersAlignmentUsersLink;
use app\models\data\OkGroups;
use app\models\data\VkGroups;
use app\models\data\Options;
use app\models\data\TempNewInternetUsers;
use app\components\socialapi\SocialFactory;
use app\components\okapi\OdnoklassnikiApi;
use app\components\okapi\OdnoklassnikiSDK;
use app\components\vkapi\VkontakteSDK;
use app\components\vkapi\VkontakteApi;
use app\helpers\HelperFunctions;
use app\models\data\Findface;
use app\models\data\Galleries;
use app\models\data\InternetUser;
use yii\console\ExitCode;
use yii\console\Controller;
use Yii;
use yii\db\Query;
use app\models\data\Events;
use yii\helpers\Console;

class SchedulerController extends Controller
{
    public $processCount = 5;
    private $startTime = 0;

    public function actionIndex()
    {
        echo "This is SchedulerController";
        return ExitCode::OK;
    }

    private function execTime($action, $arArgs = []) {
        if (isset($arArgs) && is_array($arArgs) && array_search("exectime", $arArgs) !== false) {
            if ($action == 'start') {
                $this->startTime = microtime(true);
            } elseif ($action = 'end') {
                $etime = gmdate("H:i:s", microtime(true) - $this->startTime);
                $this->stdout("Execution time: ". $etime . PHP_EOL, Console::FG_GREEN);
            }
        }
    }


    private function loadVkAttachment(\stdClass $item, int $item_id, int $owner_id, int $comment_id = null)
    {
        if (property_exists($item, 'attachments') && $item_id && $owner_id) {
            $attachements = $item->attachments;
            foreach ($attachements as $oneAttach) {
                $tempAttach = [];
                $tempAttach['post_id'] = $item_id;
                if ($comment_id)
                    $tempAttach['comment_id'] = $comment_id;
                $tempAttach['soc_network_id'] = 1;
                if (property_exists($oneAttach, 'type'))
                    $tempAttach['type'] = $oneAttach->type;

                if (is_object($oneAttach->{$tempAttach['type']})) {

                    if (property_exists($oneAttach->{$tempAttach['type']}, 'id'))
                        $tempAttach['attachment_id'] = $oneAttach->{$tempAttach['type']}->id;
                    if (property_exists($oneAttach->{$tempAttach['type']}, 'owner_id'))
                        $tempAttach['owner_id'] = $oneAttach->{$tempAttach['type']}->owner_id;
                    if (property_exists($oneAttach->{$tempAttach['type']}, 'title'))
                        $tempAttach['title'] = HelperFunctions::cleanString($oneAttach->{$tempAttach['type']}->title);
                    if (property_exists($oneAttach->{$tempAttach['type']}, 'date'))
                        $tempAttach['date'] = $oneAttach->{$tempAttach['type']}->date;
                    if (property_exists($oneAttach->{$tempAttach['type']}, 'access_key'))
                        $tempAttach['access_key'] = $oneAttach->{$tempAttach['type']}->access_key;

                    switch ($tempAttach['type']) {
                        case 'photo':
                            $tmpWidth = 0;
                            foreach ($oneAttach->{$tempAttach['type']}->sizes as $oneSize) {
                                if ($oneSize->width > $tmpWidth)
                                    $tempAttach['url'] = $oneSize->url;
                            }
                            break;
                        case 'poll':
                            if (property_exists($oneAttach->{$tempAttach['type']}, 'question'))
                                $tempAttach['title'] = HelperFunctions::cleanString($oneAttach->{$tempAttach['type']}->question);
                            if (property_exists($oneAttach->{$tempAttach['type']}, 'created'))
                                $tempAttach['date'] = $oneAttach->{$tempAttach['type']}->created;
                            if (property_exists($oneAttach->{$tempAttach['type']}, 'answers'))
                                $tempAttach['attachment_content'] = json_encode($oneAttach->{$tempAttach['type']}->answers);
                            break;
                        case 'video':
                            //sleep(1);
                            $resultVideo = VkontakteSDK::makeQuery('video.get', [
                                'owner_id' => $tempAttach['owner_id'],
                                'videos' => $tempAttach['owner_id'] . '_' . $tempAttach['attachment_id'] . '_' . $tempAttach['access_key'],
                                'count' => 1,
                                'offset' => 0,
                            ]);
                            //usleep(400000);
                            if (is_object($resultVideo) && property_exists($resultVideo, 'items') && count($resultVideo->items)) {
                                $tempAttach['url'] = $resultVideo->items[0]->player;
                            }
                            break;
                        case 'sticker':
                            if (!property_exists($oneAttach->{$tempAttach['type']}, 'owner_id'))
                                $tempAttach['owner_id'] = $owner_id;
                            $tmpWidth = 0;
                            foreach ($oneAttach->{$tempAttach['type']}->images as $oneImage) {
                                if ($oneImage->width > $tmpWidth)
                                    $tempAttach['url'] = $oneImage->url;
                            }
                            break;
                        case 'audio':
                        case 'doc':
                        case 'link':
                            if (!property_exists($oneAttach->{$tempAttach['type']}, 'owner_id'))
                                $tempAttach['owner_id'] = $owner_id;
                            $tempAttach['url'] = $oneAttach->{$tempAttach['type']}->url;
                            break;
                        case 'page':
                            if (property_exists($oneAttach->{$tempAttach['type']}, 'group_id'))
                                $tempAttach['owner_id'] = "-".$oneAttach->{$tempAttach['type']}->group_id;
                            else
                                $tempAttach['owner_id'] = $owner_id;
                            if (property_exists($oneAttach->{$tempAttach['type']}, 'view_url'))
                                $tempAttach['url'] = $oneAttach->{$tempAttach['type']}->view_url;
                            break;
                        default:
                            if (property_exists($oneAttach->{$tempAttach['type']}, 'url'))
                                $tempAttach['url'] = $oneAttach->{$tempAttach['type']}->url;
                    }

                    if (!isset($tempAttach['attachment_id']) || empty($tempAttach['attachment_id'])) {
                        if (!isset($tempAttach['url']))
                            $tempAttach['url'] = "";
                        $tempAttach['attachment_id'] = HelperFunctions::numHash($owner_id.$item_id.$tempAttach['type'].$tempAttach['url'], 9);
                    }

                    Yii::$app->db->CreateCommand()->upsert('post_attachments', $tempAttach, false)->execute();
                }
            }
            return ExitCode::OK;
        } else {
            return false;
        }
    }

    public function actionGetPostsFromVkGroups()
    {
        exec("ps aux|grep 'scheduler/get-posts-from-vk-groups'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);
        /*Скрипт работает в 2х режимах, 1. прохождение ленты до конца с попыткой забрать все посты; 2. обновление ленты только новыми
        Режимы управляются флагом update_wall_mode*/
        $arGroups = $arGroupsPostsOffset = $arGroupsPostsCount = $arGroupsUpdateWall = [];
        $arCache = [];
        $socNetworkId = InternetUser::SOC_NETWORK_ID_VK;
        if (empty($arArgs)) {
            $makeQuery = Yii::$app->db->createCommand("SELECT vk_id, posts, update_wall_mode FROM vk_groups WHERE watch = 1")->queryAll();
        } else {
            $ids = [];
            foreach ($arArgs as &$oneArg) {
                if (strlen(trim($oneArg)) && is_numeric(trim($oneArg)))
                    $ids[] = intval(trim($oneArg));
            }
            if (count($ids))
                $makeQuery = Yii::$app->db->createCommand("SELECT vk_id, posts, update_wall_mode FROM vk_groups WHERE vk_id IN (". implode(',', $ids) .")")->queryAll();
            else
                $makeQuery = Yii::$app->db->createCommand("SELECT vk_id, posts, update_wall_mode FROM vk_groups WHERE watch = 1")->queryAll();
        }

        foreach ($makeQuery as $oneGroup) {
            $arGroups[] = $oneGroup['vk_id'];
            $arGroupsPostsCount[$oneGroup['vk_id']] = !empty($oneGroup['posts'])?$oneGroup['posts']:0;
            $arGroupsUpdateWall[$oneGroup['vk_id']] = intval($oneGroup['update_wall_mode'])?1:0;
        }

        /*Получаем настройку Оффсета*/
        $oneGroup = reset($arGroups);
        $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'scheduler_get_posts_from_vk_groups_offset'")->queryOne();
        if (isset($makeQuery['option_value']) && !empty($makeQuery['option_value'])) {
            $oneGroup = $makeQuery['option_value'];
        } elseif (!isset($makeQuery['option_value'])) {
            Yii::$app->db->createCommand()->insert('options', [
                'option_name' => 'scheduler_get_posts_from_vk_groups_offset',
                'option_value' => reset($arGroups)
            ])->execute();
        }

        /*Обновляем оффсет*/
        $currentGroupKey = array_search($oneGroup, $arGroups);
        if ($currentGroupKey == (count($arGroups)-1)) {
            $saveNewGroup = reset($arGroups);
        } else {
            $saveNewGroup = $arGroups[$currentGroupKey+1];
        }
        Yii::$app->db->createCommand()->update('options', ['option_value' => $saveNewGroup], ['option_name' => 'scheduler_get_posts_from_vk_groups_offset'])->execute();

        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        if($debug) {
            $postsGet = $postsGetParent = 0;
            $issetPosts = $newPosts = $issetParentPosts = $newParentPosts = [];
        }
        /*END DEBUG*/

        $itemsPerPage = 100; //максимальное значение для данного метода!!!!
        $offsetLimit = 200;
        //время в будущее для того чтобы скрипты забора комментов и лайков знали какие посты акруальны и какие обновлять
        $upTime = HelperFunctions::utimeAdd(time(), 0, 0, 0, 0, 30, 0);

        if ($oneGroup) {

            $makeQuery = Yii::$app->db->createCommand("SELECT posts_offset FROM vk_groups WHERE vk_id = ". $oneGroup)->queryOne();
            $arGroupsPostsOffset = !empty($makeQuery['posts_offset'])?$makeQuery['posts_offset']:0;

            $postsCount = $arGroupsPostsCount[$oneGroup];
            $offset = $arGroupsPostsOffset;
            $checkDate = 0;

            $maxOffset = $arGroupsPostsOffset + $offsetLimit;
            if ($maxOffset < $arGroupsPostsCount[$oneGroup] || ($arGroupsPostsCount[$oneGroup] == 0 && $arGroupsPostsOffset == 0))
                $saveCurrentOffset = $maxOffset;
            else
                $saveCurrentOffset = 0;
            Yii::$app->db->CreateCommand()->update('vk_groups', ['posts_offset' => $saveCurrentOffset], ['vk_id' => $oneGroup] )->execute();

            if ($arGroupsUpdateWall[$oneGroup]) {
                $lastDate = Yii::$app->db->createCommand("SELECT date FROM posts WHERE group_id = " . $oneGroup . " ORDER BY date DESC LIMIT 1")->queryOne();
                if (isset($lastDate['date'])) {
                    //предел времени по которому определяются актуальные посты
                    $checkDate = HelperFunctions::utimeAdd($lastDate['date'], 0, 0, 0, 0, -30, 0);
                }
            }

            do {

                $resultQuery = VkontakteSDK::makeQuery('wall.get', [
                    'owner_id' => '-'.$oneGroup,
                    'offset' => $offset,
                    'count' => $itemsPerPage,
                    'extended' => 1,
                    'fields' => 'city_name'
                ]);

                /*DEBUG*/
                if($debug) {
                    echo 'Get group ID -'.$oneGroup." / offset: ".$offset." / itemsPerPage: ".$itemsPerPage."\n";
                }
                /*END DEBUG*/

//                if ($offset)
//                    usleep(400000);

                $itemsArr = [];
                if(is_object($resultQuery) && property_exists($resultQuery, 'items') && count($resultQuery->items)) {
                    $offset += $itemsPerPage;
                    $itemsArr = $resultQuery->items;
                    $postsCount = $resultQuery->count;

                    foreach ($resultQuery->items as $oneData) {
                        $tempPost = $tempPostUpdate = $tempAttach = $tempComment = $tempLike = [];

                        if (property_exists($oneData, 'id'))
                            $tempPost['post_id'] = $oneData->id;
                        if (property_exists($oneData, 'owner_id'))
                            $tempPost['owner_id'] = $oneData->owner_id;
                        if (property_exists($oneData, 'date'))
                            $tempPost['date'] = $oneData->date;
                        if (property_exists($oneData, 'text'))
                            $tempPost['post_text'] = HelperFunctions::replace4ByteCharacters($oneData->text);
                        if (property_exists($oneData, 'comments'))
                            $tempPost['comments'] = $tempPostUpdate['comments'] = $oneData->comments->count;
                        if (property_exists($oneData, 'likes'))
                            $tempPost['likes'] = $tempPostUpdate['likes'] = $oneData->likes->count;
                        if (property_exists($oneData, 'reposts'))
                            $tempPost['reposts'] = $tempPostUpdate['reposts'] = $oneData->reposts->count;
                        if (property_exists($oneData, 'views'))
                            $tempPost['views'] = $tempPostUpdate['views'] = $oneData->views->count;

                        $tempPost['soc_network_id'] = $socNetworkId;
                        $tempPost['group_id'] = $oneGroup;
                        if ($tempPost['date'] > $checkDate)
                            $tempPost['up_time'] = $tempPostUpdate['up_time'] = $upTime;
                        //репост, не имеет лайков, коментов и просмотров
                        if (property_exists($oneData, 'copy_history')) {
                            foreach ( array_reverse($oneData->copy_history) as $parentPost) {
                                $tempParentPost = [];
                                if (property_exists($parentPost, 'id'))
                                    $tempParentPost['post_id'] = $parentPost->id;
                                if (property_exists($parentPost, 'owner_id'))
                                    $tempParentPost['owner_id'] = $parentPost->owner_id;
                                if (property_exists($parentPost, 'date'))
                                    $tempParentPost['date'] = $parentPost->date;
                                if (property_exists($parentPost, 'text'))
                                    $tempParentPost['post_text'] = HelperFunctions::replace4ByteCharacters($parentPost->text);

                                $tempParentPost['soc_network_id'] = $socNetworkId;
                                $tempParentPost['group_id'] = $oneGroup;
                                $tempParentPost['is_parent'] = 1;

                                if (isset($tempPost['parent_post_id']) && isset($tempPost['parent_owner_id'])) {
                                    $tempParentPost['parent_post_id'] = $tempPost['parent_post_id'];
                                    $tempParentPost['parent_owner_id'] = $tempPost['parent_owner_id'];
                                }

                                /*DEBUG*/
                                if($debug) {
                                    if (isset($tempPost['parent_post_id']) && isset($tempPost['parent_owner_id'])) {
                                        $res = Yii::$app->db->CreateCommand("SELECT id FROM posts WHERE owner_id = {$tempPost['parent_owner_id']} AND post_id = {$tempPost['parent_post_id']} AND soc_network_id = {$socNetworkId}")->queryOne();
                                    }
                                }
                                /*END DEBUG*/

                                if ($tempParentPost['owner_id'] > 0) {
                                    $internetUser = $tempParentPost['owner_id'];
                                    if (!isset($arCache['internet_users'][$internetUser])) {
                                        $iuId = (new Query())->select(['[[id]]'])
                                            ->from(InternetUser::tableName())
                                            ->where([
                                                '[[soc_network_id]]' => $socNetworkId,
                                                '[[soc_network_user_id]]' => $internetUser
                                            ])->one();
                                        if (isset($iuId['id']) && $iuId['id']) {
                                            $arCache['internet_users'][$internetUser] = $iuId['id'];
                                        }
                                    }
                                    if (isset($arCache['internet_users'][$internetUser])) {
                                        $tempParentPost['internet_users_id'] = $arCache['internet_users'][$internetUser];
                                    }
                                }

                                /*DEBUG*/
                                if($debug) {
                                    if (isset($tempParentPost['internet_users_id'])) {
                                        echo "\e[1;32mInternet User: ".$tempParentPost['internet_users_id']."\e[0m\n";
                                    }
                                }
                                /*END DEBUG*/

                                Yii::$app->db->CreateCommand()->upsert('posts', $tempParentPost, false)->execute();

                                /*DEBUG*/
                                if($debug) {
                                    if (isset($tempPost['post_id']) && isset($tempPost['owner_id'])) {

                                        if (isset($res['id']) && $res['id']) {
                                            $issetParentPosts[] = $res['id'];
                                        } else {
                                            $newParentPosts[] =  Yii::$app->db->getLastInsertID();
                                        }
                                        $postsGet++;
                                    }
                                }
                                /*END DEBUG*/

                                if (property_exists($parentPost, 'id') && property_exists($parentPost, 'owner_id')) {
                                    $tempPost['parent_post_id'] = $parentPost->id;
                                    $tempPost['parent_owner_id'] = $parentPost->owner_id;
                                }

                                self::loadVkAttachment($parentPost, $tempParentPost['post_id'], $tempParentPost['owner_id']);
                            }
                        }

                        /*DEBUG*/
                        if($debug) {
                            if (isset($tempPost['post_id']) && isset($tempPost['owner_id'])) {
                                $res = Yii::$app->db->CreateCommand("SELECT id FROM posts WHERE owner_id = {$tempPost['owner_id']} AND post_id = {$tempPost['post_id']} AND soc_network_id = 1")->queryOne();
                            }
                        }
                        /*END DEBUG*/

                        if ($tempPost['owner_id'] > 0) {
                            $internetUser = $tempPost['owner_id'];
                            if (!isset($arCache['internet_users'][$internetUser])) {
                                $iuId = (new Query())->select(['[[id]]'])
                                    ->from(InternetUser::tableName())
                                    ->where([
                                        '[[soc_network_id]]' => $socNetworkId,
                                        '[[soc_network_user_id]]' => $internetUser
                                    ])->one();
                                if (isset($iuId['id']) && $iuId['id']) {
                                    $arCache['internet_users'][$internetUser] = $iuId['id'];
                                }
                            }
                            if (isset($arCache['internet_users'][$internetUser])) {
                                $tempPost['internet_users_id'] = $arCache['internet_users'][$internetUser];
                            }
                        }

                        /*DEBUG*/
                        if($debug) {
                            if (isset($tempPost['internet_users_id'])) {
                                echo "\e[1;32mInternet User: ".$tempPost['internet_users_id']."\e[0m\n";
                            }
                        }
                        /*END DEBUG*/

                        Yii::$app->db->CreateCommand()->upsert('posts', $tempPost, $tempPostUpdate)->execute();

                        /*DEBUG*/
                        if($debug) {
                            if (isset($tempPost['post_id']) && isset($tempPost['owner_id'])) {

                                if (isset($res['id']) && $res['id']) {
                                    $issetPosts[] = $res['id'];
                                } else {
                                    $newPosts[] =  Yii::$app->db->getLastInsertID();
                                }
                                $postsGet++;
                            }
                        }
                        /*END DEBUG*/

                        self::loadVkAttachment($oneData, $tempPost['post_id'], $tempPost['owner_id']);

                        //отработает в том случае если флаг update_wall_mode = 1, так прерывает проход ниже по ленте для режима одновления ленты
                        if ($tempPost['date'] < $checkDate) {
                            Yii::$app->db->CreateCommand()->update('vk_groups', ['posts_offset' => 0], ['vk_id' => $oneGroup] )->execute();

                            /*DEBUG*/
                            if($debug) {
                                echo "\n"."GET BRAKE because: ".$tempPost['date']." < ".$checkDate." OR ".$offset." > ".$maxOffset."\n\n";
                            }
                            /*END DEBUG*/

                            break 2;
                        }
                    }
                }

            } while (count($itemsArr) && $offset < $maxOffset);

            if ($postsCount != $arGroupsPostsCount[$oneGroup]) {
                Yii::$app->db->CreateCommand()->update('vk_groups', ['posts' => $postsCount], ['vk_id' => $oneGroup] )->execute();
            }

            if (!$arGroupsUpdateWall[$oneGroup]) {
                $countPosts = Yii::$app->db->createCommand("SELECT COUNT(id) AS posts_number FROM posts WHERE (group_id = " . $oneGroup . ") AND (is_parent = 0)")->queryOne();
                if ($postsCount <= $countPosts['posts_number']) {
                    Yii::$app->db->CreateCommand()->update('vk_groups', ['update_wall_mode' => 1], ['vk_id' => $oneGroup])->execute();
                }
            }

        }

        /*DEBUG*/
        if($debug) {
            echo "LOADED posts from api: " . $postsGet . " / parent posts: " . $postsGetParent . "\n";
            echo "INSET NEW POSTS: " ./*implode(',', $newPosts)*/ count($newPosts) . "\n";
            echo "INSET NEW PARENT POSTS:" ./*implode(',', $newParentPosts)*/ count($newParentPosts) . "\n";
            echo "UPDATE POSTS: " ./*implode(',', $issetPosts)*/ count($issetPosts) . "\n";
            echo "UPDATE PARENT POSTS: " ./*implode(',', $issetParentPosts)*/ count($issetParentPosts) . "\n";
        }
        /*END DEBUG*/

        $this->execTime('end', $arArgs);

        return ExitCode::OK;

    }

    public function actionGetCommentsOfVkPosts()
    {
        exec("ps aux|grep 'scheduler/get-comments-of-vk-posts'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);

        $currentTime = time();
        $itemsPerPage = 50;
        $commentsCheck = [];
        $socNetworkId = InternetUser::SOC_NETWORK_ID_VK;
        $arCache = [];
        $counter = 0;

        //определяем новые комменты которых нет в таблице
        $commentsCheckTemp = Yii::$app->db->createCommand("SELECT posts.post_id, posts.owner_id, CONCAT(posts.owner_id, '_', posts.post_id) AS post_owner_id, posts.group_id AS post_group_id FROM posts LEFT JOIN post_comments ON (posts.post_id = post_comments.post_id AND posts.owner_id = post_comments.owner_id) WHERE (posts.soc_network_id = {$socNetworkId}) AND (posts.comments > 0) AND (post_comments.owner_id IS NULL) AND (post_comments.post_id IS NULL) ORDER BY posts.date ASC LIMIT {$itemsPerPage}")->queryAll();
        if (count($commentsCheckTemp))
            $commentsCheck = array_merge($commentsCheck, $commentsCheckTemp);

//        $t = [];
//        foreach ($commentsCheckTemp as $own_post) {
//            $t[] = $own_post['post_owner_id'];
//        }
//        echo "Finded new posts count:\n\n".implode(' | ', $t); sleep(5);

        //определяем записи которые есть в таблице, но информацию по которым нужно актуализировать
        $commentsCheckTemp = Yii::$app->db->createCommand("SELECT DISTINCT post_comments.post_id, post_comments.owner_id, CONCAT(post_comments.owner_id, '_', post_comments.post_id) AS post_owner_id, posts.group_id AS post_group_id FROM post_comments LEFT JOIN posts ON (posts.owner_id = post_comments.owner_id AND posts.post_id = post_comments.post_id AND posts.soc_network_id = post_comments.soc_network_id) WHERE (posts.up_time > {$currentTime}) AND (post_comments.soc_network_id = {$socNetworkId}) ORDER BY post_comments.update_time ASC LIMIT {$itemsPerPage}")->queryAll();
        if (count($commentsCheckTemp))
            $commentsCheck = array_merge($commentsCheck, $commentsCheckTemp);

//        $t = [];
//        foreach ($commentsCheckTemp as $own_post) {
//            $t[] = $own_post['post_owner_id'];
//        }
//        echo "\n\nFinded Existing posts for comment update:\n\n".implode(' | ', $t); sleep(5);

        if (count($commentsCheck)) {
            foreach ($commentsCheck as $commentCheck) {
                $offsetComments = 0;
                do {

                    $resultComments = VkontakteSDK::makeQuery('wall.getComments', [
                        'owner_id' => $commentCheck['owner_id'],
                        'post_id' => $commentCheck['post_id'],
                        'offset' => $offsetComments,
                        'count' => 100,
                        'extended' => 1,
                        'need_likes' => 1
                    ]);

//                    if ($offsetComments)
//                        usleep(400000);

                    $commentsArr = [];
                    $commentsCount = 0;
                    if(is_object($resultComments) && property_exists($resultComments, 'items') && count($resultComments->items)) {
                        $offsetComments += 100;
                        $commentsArr = $resultComments->items;
                        $commentsCount = $resultComments->count;

                        foreach ($resultComments->items as $oneComment) {
                            $tempComment = $tempCommentUpdate = [];
                            $tempComment['soc_network_id'] = $socNetworkId;
                            if (property_exists($oneComment, 'post_id'))
                                $tempComment['post_id'] = $oneComment->post_id;
                            if (property_exists($oneComment, 'id'))
                                $tempComment['comment_id'] = $oneComment->id;
                            if (property_exists($oneComment, 'owner_id'))
                                $tempComment['owner_id'] = $oneComment->owner_id;
                            if (property_exists($oneComment, 'from_id'))
                                $tempComment['from_user_id'] = $oneComment->from_id;
                            if (property_exists($oneComment, 'date'))
                                $tempComment['date'] = $oneComment->date;
                            if (property_exists($oneComment, 'text'))
                                $tempComment['comment_text'] = HelperFunctions::replace4ByteCharacters($oneComment->text);
                            if (property_exists($oneComment, 'likes'))
                                $tempComment['likes'] = $tempCommentUpdate['likes'] = $oneComment->likes->count;
                            $tempComment['update_time'] = $tempCommentUpdate['update_time'] = $currentTime;
                            $tempComment['group_id'] = $commentCheck['post_group_id'];
//                            echo ++$counter;
//                            echo ". Comment will be updated/inserted, owner_post_id: ".$commentCheck['owner_id']." ".$commentCheck['post_id']."\n";
//                            print_r($tempComment);

                            if (isset($tempComment['from_user_id']) && intval($tempComment['from_user_id'])) {

                                if ($tempComment['from_user_id'] > 0) {
                                    $internetUser = $tempComment['from_user_id'];
                                    if (!isset($arCache['internet_users'][$internetUser])) {
                                        $iuId = (new Query())->select(['[[id]]'])
                                            ->from(InternetUser::tableName())
                                            ->where([
                                                '[[soc_network_id]]' => $socNetworkId,
                                                '[[soc_network_user_id]]' => $internetUser
                                            ])->one();
                                        if (isset($iuId['id']) && $iuId['id']) {
                                            $arCache['internet_users'][$internetUser] = $iuId['id'];
                                        }
                                    }
                                    if (isset($arCache['internet_users'][$internetUser])) {
                                        $tempComment['internet_users_id'] = $arCache['internet_users'][$internetUser];
                                    }
                                }

                                Yii::$app->db->CreateCommand()->upsert('post_comments', $tempComment, $tempCommentUpdate)->execute();

                                self::loadVkAttachment($oneComment, $tempComment['post_id'], $tempComment['owner_id'], $tempComment['comment_id']);
                            }

                        }
                    } elseif (is_object($resultComments) && property_exists($resultComments, 'count') && ($resultComments->count == 0)) {
                        Yii::$app->db->CreateCommand()->update('posts', ['comments' => 0], ['post_id' => $commentCheck['post_id'], 'owner_id' => $commentCheck['owner_id'], 'soc_network_id' => $socNetworkId])->execute();
                    }
                } while (count($commentsArr) && ($offsetComments <= $commentsCount));
            }
        }

        $this->execTime('end', $arArgs);

        return ExitCode::OK;

    }

    public function actionGetLikesOfVkPostsComments()
    {
        exec("ps aux|grep 'scheduler/get-likes-of-vk-posts-comments'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);

        $currentTime = time();
        $itemsPerPage = 50;
        $likesCheck = [];
        $socNetworkId = InternetUser::SOC_NETWORK_ID_VK;
        $arCache = [];

        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        if($debug) {
            $likesGet = $likesAll = 0;
            $startTime = $firstStartTime = microtime(true);
        }
        /*END DEBUG*/

        //определяем новые посты которых нет в таблице
        $likesCheckTemp = Yii::$app->db->createCommand("SELECT posts.post_id AS liked_item_id, posts.owner_id, CONCAT(posts.owner_id, '_', posts.post_id) AS post_owner_id, 'post' AS `type`, posts.group_id AS post_group_id FROM posts LEFT JOIN post_comment_likes ON (posts.owner_id = post_comment_likes.owner_id AND posts.post_id = post_comment_likes.liked_item_id) WHERE (posts.soc_network_id = {$socNetworkId}) AND (posts.likes > 0) AND (post_comment_likes.owner_id IS NULL) AND (post_comment_likes.liked_item_id IS NULL) ORDER BY posts.date ASC LIMIT {$itemsPerPage}")->queryAll();
        if (count($likesCheckTemp))
            $likesCheck = array_merge($likesCheck, $likesCheckTemp);

        //определяем новые коменты которых нет в таблице
        $likesCheckTemp = Yii::$app->db->createCommand("SELECT post_comments.comment_id AS liked_item_id, post_comments.owner_id, CONCAT(post_comments.owner_id, '_', post_comments.comment_id) AS post_owner_id, 'comment' AS type, post_comments.group_id AS post_group_id FROM post_comments LEFT JOIN post_comment_likes ON (post_comments.owner_id = post_comment_likes.owner_id AND post_comments.comment_id = post_comment_likes.liked_item_id) WHERE (post_comments.soc_network_id = {$socNetworkId}) AND (post_comments.likes > 0) AND (post_comment_likes.owner_id IS NULL) AND (post_comment_likes.liked_item_id IS NULL) ORDER BY post_comments.date ASC LIMIT {$itemsPerPage}")->queryAll();
        if (count($likesCheckTemp))
            $likesCheck = array_merge($likesCheck, $likesCheckTemp);

        //определяем записи которые есть в таблице, но информацию по которым нужно актуализировать
        $likesCheckTemp = Yii::$app->db->createCommand("SELECT DISTINCT post_comment_likes.liked_item_id, post_comment_likes.owner_id, CONCAT(post_comment_likes.owner_id, '_', post_comment_likes.liked_item_id) AS post_owner_id, post_comment_likes.type, posts.group_id AS post_group_id FROM post_comment_likes LEFT JOIN posts ON (posts.owner_id = post_comment_likes.owner_id AND posts.post_id = post_comment_likes.liked_item_id AND posts.soc_network_id = post_comment_likes.soc_network_id) WHERE (posts.up_time > {$currentTime}) AND (post_comment_likes.soc_network_id = {$socNetworkId}) ORDER BY post_comment_likes.update_time ASC LIMIT {$itemsPerPage}")->queryAll();
        if (count($likesCheckTemp))
            $likesCheck = array_merge($likesCheck, $likesCheckTemp);

        if (count($likesCheck)) {
            foreach ($likesCheck as $likeCheck) {

                /*DEBUG*/
                if($debug) {
                    $likesGet = 0;
                }
                /*END DEBUG*/

                $offsetLikes = 0;
                do {

                    $resultLikes = VkontakteSDK::makeQuery('likes.getList', [
                        'owner_id' => $likeCheck['owner_id'],
                        'item_id' => $likeCheck['liked_item_id'],
                        'offset' => $offsetLikes,
                        'count' => 1000,
                        'extended' => 0,
                        'type' => $likeCheck['type']
                    ]);

//                    if ($offsetLikes)
//                        usleep(400000);

                    $likesArr = [];
                    $likesCount = 0;
                    if (is_object($resultLikes) && property_exists($resultLikes, 'items') && count($resultLikes->items)) {
                        $offsetLikes += 1000;
                        $likesArr = $resultLikes->items;
                        $likesCount = $resultLikes->count;

                        foreach ($resultLikes->items as $oneLike) {
                            $tempLike = $tempLikeUpdate = [];
                            $tempLike['soc_network_id'] = $socNetworkId;
                            $tempLike['liked_item_id'] = $likeCheck['liked_item_id'];
                            $tempLike['owner_id'] = $likeCheck['owner_id'];
                            $tempLike['from_user_id'] = $oneLike->id;
                            $tempLike['type'] = $likeCheck['type'];
                            $tempLike['update_time'] = $tempLikeUpdate['update_time'] = $currentTime;
                            $tempLike['group_id'] = $likeCheck['post_group_id'];

                            /*DEBUG*/
                            if($debug) {
                                $likesGet++;
                                $likesAll++;
                            }
                            /*END DEBUG*/

                            if ($tempLike['from_user_id'] > 0) {
                                $internetUser = $tempLike['from_user_id'];
                                if (!isset($arCache['internet_users'][$internetUser])) {
                                    $iuId = (new Query())->select(['[[id]]'])
                                        ->from(InternetUser::tableName())
                                        ->where([
                                            '[[soc_network_id]]' => $socNetworkId,
                                            '[[soc_network_user_id]]' => $internetUser
                                        ])->one();
                                    if (isset($iuId['id']) && $iuId['id']) {
                                        $arCache['internet_users'][$internetUser] = $iuId['id'];
                                    }
                                }
                                if (isset($arCache['internet_users'][$internetUser])) {
                                    $tempLike['internet_users_id'] = $arCache['internet_users'][$internetUser];
                                }
                            }

                            Yii::$app->db->CreateCommand()->upsert('post_comment_likes', $tempLike, $tempLikeUpdate)->execute();
                        }
                    } elseif (is_object($resultLikes) && property_exists($resultLikes, 'count') && ($resultLikes->count == 0)) {
                        if ($likeCheck['type'] == 'post') {
                            Yii::$app->db->CreateCommand()->update('posts', ['likes' => 0], ['post_id' => $likeCheck['liked_item_id'], 'owner_id' => $likeCheck['owner_id'], 'soc_network_id' => $socNetworkId])->execute();
                        }
                        if ($likeCheck['type'] == 'comment') {
                            Yii::$app->db->CreateCommand()->update('post_comments', ['likes' => 0], ['comment_id' => $likeCheck['liked_item_id'], 'owner_id' => $likeCheck['owner_id'], 'soc_network_id' => $socNetworkId])->execute();
                        }
                    }

                    /*DEBUG*/
                    if($debug) {
                        echo "\nQuery likes from API: OwnerID ".$likeCheck['owner_id']." / Liked ItemID ".$likeCheck['liked_item_id']." / Type ".$likeCheck['type']." / Offset ".$offsetLikes." / CountAll: ".$resultLikes->count."\n"."Time: ".gmdate("H:i:s", microtime(true) - $startTime)." / Time from start ".gmdate("H:i:s", microtime(true) - $firstStartTime)."\n";
                        $startTime = microtime(true);
                        echo "Get likes Per iteration: ".count($likesArr)."\n";
                        echo "Get likes ALL: ".$likesGet."\n";
                    }
                    /*END DEBUG*/

                } while (count($likesArr) && ($offsetLikes <= $likesCount));

                /*DEBUG*/
                if($debug) {
                    echo "Get likes SUMM: ".$likesGet."\n";
                }
                /*END DEBUG*/

            }
        }

        /*DEBUG*/
        if($debug) {
            echo "END: ".gmdate("H:i:s", microtime(true) - $startTime)."\n";
            echo "GET ALL LIKES FROM API:".$likesAll."\n";
        }
        /*END DEBUG*/

        $this->execTime('end', $arArgs);

        return ExitCode::OK;

    }

    public function actionCountFriendsAndFollowersWithAlignment()
    {
        exec("ps aux|grep 'scheduler/count-friends-and-followers-with-alignment'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);
        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        /*END DEBUG*/

        $itemsCheckPerPage = 1000;
        $indexCheck = 0;
        /*Check if is alignment*/
        $optionsQuery = (new Query())->select(['[[option_value]]'])
            ->from(Options::tableName())
            ->where(['[[option_name]]' => 'scheduler_count_friends_and_followers_with_alignment_check_index'])
            ->one();
        if (isset($optionsQuery['option_value']) && !empty($optionsQuery['option_value'])) {
            $indexCheck = intval($optionsQuery['option_value']);
        } elseif (!isset($optionsQuery['option_value'])) {
            $newOption = new Options();
            $newOption->option_name = 'scheduler_count_friends_and_followers_with_alignment_check_index';
            $newOption->option_value = '0';
            $newOption->save();
        }

        $makeQuery = (new Query())->select(['[[id]]','[[internet_users_id]]'])
            ->from(InternetUsersStatistics::tableName())
            ->where(['>','id',$indexCheck])
            ->andWhere('(cnt_friends_alignment > 0) OR (cnt_followers_alignment > 0)')
            ->orderBy('id')
            ->limit($itemsCheckPerPage)
            ->all();

        if (count($makeQuery)) {
            foreach ($makeQuery as $oneCheckId) {
                $arCheck[$oneCheckId['id']] = $oneCheckId['internet_users_id'];
            }

            /*DEBUG*/
            if ($debug) {
                echo "Check Users: ".count($arCheck)." / Index: ".$indexCheck."\n";
            }
            /*END DEBUG*/

            end($arCheck);
            /*Обновляем оффсет*/
            Options::updateAll([
                '[[option_value]]' => key($arCheck)
            ], [
                '[[option_name]]' => 'scheduler_count_friends_and_followers_with_alignment_check_index'
            ]);

            $arPresent = [];
            $makeQueryUsersLink = (new Query())->select(['[[internet_user_id]]'])
                ->from(TempUsersAlignmentUsersLink::tableName())
                ->where(['in','[[internet_user_id]]',$arCheck])
                ->all();
            if (count($makeQueryUsersLink)) {
                foreach ($makeQueryUsersLink as $presentInUsersLink) {
                    $arPresent[$presentInUsersLink['internet_user_id']] = $presentInUsersLink['internet_user_id'];
                }
            }
            $needToUpdate = array_diff($arCheck, $arPresent);

            /*DEBUG*/
            if ($debug) {
                echo "Need To Update not alignment users: ".count($needToUpdate)."\n";
                print_r($needToUpdate);
            }
            /*END DEBUG*/

            if (count($needToUpdate)) {
                InternetUsersStatistics::updateAll([
                    '[[cnt_friends_alignment]]' => 0,
                    '[[proc_friends_alignment]]' => 0,
                    '[[cnt_followers_alignment]]' => 0,
                    '[[proc_followers_alignment]]' => 0,
                ],[
                    '[[internet_users_id]]' => $needToUpdate
                ]);
            }
        } else {
            /*Обновляем оффсет*/
            Options::updateAll([
                '[[option_value]]' => '0'
            ], [
                '[[option_name]]' => 'scheduler_count_friends_and_followers_with_alignment_check_index'
            ]);
        }
        /*END Check if is alignment*/

        $arResult = $arIds = [];
        $itemsPerPage = 1000;
        $index = 0;

        /*Update data*/
        $optionsQuery = (new Query())->select(['[[option_value]]'])
            ->from(Options::tableName())
            ->where(['[[option_name]]' => 'scheduler_count_friends_and_followers_with_alignment_index'])
            ->one();
        if (isset($optionsQuery['option_value']) && !empty($optionsQuery['option_value'])) {
            $index = intval($optionsQuery['option_value']);
        } elseif (!isset($optionsQuery['option_value'])) {
            $newOption = new Options();
            $newOption->option_name = 'scheduler_count_friends_and_followers_with_alignment_index';
            $newOption->option_value = '0';
            $newOption->save();
        }

        $makeQuery = (new Query())->select([
            '[[id]]',
            '[[type]]',
            '[[internet_user_id]]',
            'COUNT([[alignment_internet_user_id]]) AS count_alignment'
        ])->from(TempUsersAlignmentUsersLink::tableName())
            ->where(['>','[[id]]',$index])
            ->groupBy(['[[type]]', '[[internet_user_id]]'])
            ->orderBy('id')
            ->limit($itemsPerPage)
            ->all();

        if (count($makeQuery)) {
            foreach ($makeQuery as $countResultAlignment) {
                $arResult[$countResultAlignment['internet_user_id']][$countResultAlignment['type']] = $countResultAlignment['count_alignment'];
                $arIds[$countResultAlignment['internet_user_id']] = $countResultAlignment['internet_user_id'];
            }
            $lastId = $countResultAlignment['id'];

            /*DEBUG*/
            if ($debug) {
                echo "Check Users last ID: ".$lastId." / Index: ".$index."\n";
                echo "Alignment Users:"."\n";
                print_r($arResult);
            }
            /*END DEBUG*/

            /*Обновляем индекс*/
            Options::updateAll([
                '[[option_value]]' => $lastId
            ], [
                '[[option_name]]' => 'scheduler_count_friends_and_followers_with_alignment_index'
            ]);

            $makeCountQuery = (new Query())->select([
                'iu.[[id]] AS internet_users_id',
                'iu.[[cnt_friends]]',
                'iu.[[cnt_followers]]',
                'ius.[[cnt_friends_alignment]]',
                'ius.[[cnt_followers_alignment]]',
                'ius.[[proc_friends_alignment]]',
                'ius.[[proc_followers_alignment]]'
                ])->from(['iu' => InternetUser::tableName()])
                ->leftJoin(['ius' => InternetUsersStatistics::tableName()], 'ius.[[internet_users_id]] = iu.[[id]]')
                ->where(['in','iu.[[id]]',$arIds])
                ->all();

            foreach ($makeCountQuery as $oneItem) {

                $result = [
                    'internet_users_id' => $oneItem['internet_users_id'],
                    'cnt_friends' => $oneItem['cnt_friends'],
                    'cnt_followers' => $oneItem['cnt_followers']
                ];
                if (isset($arResult[$oneItem['internet_users_id']]['fr'])) {
                    $result['cnt_friends_alignment'] = $arResult[$oneItem['internet_users_id']]['fr'];
                    $result['proc_friends_alignment'] = $oneItem['cnt_friends']?round(($result['cnt_friends_alignment']/$oneItem['cnt_friends'])*100, 1):0;
                }
                if (isset($arResult[$oneItem['internet_users_id']]['fl'])) {
                    $result['cnt_followers_alignment'] = $arResult[$oneItem['internet_users_id']]['fl'];
                    $result['proc_followers_alignment'] = $oneItem['cnt_followers']?round(($result['cnt_followers_alignment']/$oneItem['cnt_followers'])*100, 1):0;
                }

                $checkResult = array_filter($oneItem, function ($k) use ($result) {
                    return isset($result[$k]);
                }, ARRAY_FILTER_USE_KEY);

                /*DEBUG*/
                if ($debug) {
                    echo "User: ".$oneItem['internet_users_id']."\n";
                    echo "Old Values:\n";
                    print_r($oneItem);
                    echo "New Values:\n";
                    print_r($result);
                    echo "Check Values:\n";
                    print_r($checkResult);
                    echo "\n";
                }
                /*END DEBUG*/

                if (array_sum($checkResult) != array_sum($result)) {
                    Yii::$app->db->createCommand()->upsert('internet_users_statistics', $result, $result)->execute();

                    /*DEBUG*/
                    if ($debug) {
                        echo "\e[1;32mUpdated User: ".$oneItem['internet_users_id']."\e[0m\n";
                    }
                    /*END DEBUG*/
                }
            }
        } else {
            /*Обновляем оффсет*/
            Options::updateAll([
                '[[option_value]]' => '0'
            ], [
                '[[option_name]]' => 'scheduler_count_friends_and_followers_with_alignment_index'
            ]);
        }

        $this->execTime('end', $arArgs);

        return ExitCode::OK;

    }

    public function actionCountCommentsAndLikesWithAlignment()
    {
        exec("ps aux|grep 'scheduler/count-comments-and-likes-with-alignment'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);
        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        /*END DEBUG*/

        $itemsPerPage = 1000;
        $index = 0;

        /*Получаем настройку Оффсета*/
        $optionsQuery = (new Query())->select(['[[option_value]]'])
            ->from(Options::tableName())
            ->where(['[[option_name]]' => 'scheduler_count_comments_and_likes_with_alignment_index'])
            ->one();
        if (isset($optionsQuery['option_value']) && !empty($optionsQuery['option_value'])) {
            $index = intval($optionsQuery['option_value']);
        } elseif (!isset($optionsQuery['option_value'])) {
            $newOption = new Options();
            $newOption->option_name = 'scheduler_count_comments_and_likes_with_alignment_index';
            $newOption->option_value = '0';
            $newOption->save();
        }

        $sql = "
        SELECT internet_users.id, 
            internet_users.cnt_friends, 
            internet_users.cnt_followers, 
            internet_users.soc_network_user_id, 
            internet_users.soc_network_id, 
            (
                SELECT COUNT(post_comments.id) FROM post_comments WHERE 
                    (post_comments.soc_network_id = internet_users.soc_network_id) 
                AND 
                    (post_comments.from_user_id = internet_users.soc_network_user_id)
            ) AS cnt_comments, 
            (
                SELECT COUNT(post_comment_likes.id) FROM post_comment_likes WHERE 
                    (post_comment_likes.soc_network_id = internet_users.soc_network_id) 
                AND 
                    (post_comment_likes.from_user_id = internet_users.soc_network_user_id)
            ) AS cnt_likes
        FROM internet_users 
        WHERE (internet_users.active = 1) AND 
              (internet_users.id > {$index}) 
        LIMIT {$itemsPerPage}";

        $makeQuery = Yii::$app->db->createCommand($sql)->queryAll();
        if (count($makeQuery)) {

            if($debug) {
                $this->stdout("Count query: ". count($makeQuery) . PHP_EOL, Console::FG_GREEN);
            }

            foreach ($makeQuery as $oneItem) {
                $result = [
                    'internet_users_id' => $oneItem['id'],
                    'cnt_friends' => $oneItem['cnt_friends'],
                    'cnt_followers' => $oneItem['cnt_followers'],
                    'cnt_comments' => $oneItem['cnt_comments'],
                    'cnt_likes' => $oneItem['cnt_likes'],
                ];
                Yii::$app->db->createCommand()->upsert('internet_users_statistics', $result, $result)->execute();
                $index = $oneItem['id'];
            }
        } else {

            if($debug) {
                $this->stdout("Set index to 0" . PHP_EOL, Console::FG_YELLOW);
            }

            $index = 0;
        }

        /*Обновляем оффсет*/
        Options::updateAll([
            '[[option_value]]' => $index
        ], [
            '[[option_name]]' => 'scheduler_count_comments_and_likes_with_alignment_index'
        ]);

        $this->execTime('end', $arArgs);

        return ExitCode::OK;

    }

    public function actionSetTempUsersAlignmentUsersLinkTable()
    {
        exec("ps aux|grep 'scheduler/set-temp-users-alignment-users-link-table'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);
        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        /*END DEBUG*/
        $itemsPerPage = 10;
        $offsetLimit = 100;
        $offset = 0;
        $itemsCheckPerPage = 1000;
        $index = 0;
        $chunkedCount = 1000;

        /*Check if is alignment*/
        $optionsQuery = (new Query())->select(['[[option_value]]'])
            ->from(Options::tableName())
            ->where(['[[option_name]]' => 'scheduler_set_temp_users_alignment_users_link_table_check_index'])
            ->one();
        if (isset($optionsQuery['option_value']) && !empty($optionsQuery['option_value'])) {
            $index = intval($optionsQuery['option_value']);
        } elseif (!isset($optionsQuery['option_value'])) {
            $newOption = new Options();
            $newOption->option_name = 'scheduler_set_temp_users_alignment_users_link_table_check_index';
            $newOption->option_value = '0';
            $newOption->save();
        }

        $makeQuery = (new Query())->select(['[[id]]','[[alignment_internet_user_id]]'])
            ->from(TempUsersAlignmentUsersLink::tableName())
            ->where(['>','id',$index])
            ->orderBy('id')
            ->limit($itemsCheckPerPage)
            ->all();

        if (count($makeQuery)) {
            foreach ($makeQuery as $oneCheckId) {
                $arCheck[$oneCheckId['id']] = $oneCheckId['alignment_internet_user_id'];
            }

            /*DEBUG*/
            if ($debug) {
                echo "Check Users (with duplicates): ".count($arCheck)." / Index: ".$index."\n";
            }
            /*END DEBUG*/

            end($arCheck);
            /*Обновляем оффсет*/
            Options::updateAll([
                '[[option_value]]' => key($arCheck)
            ], [
                '[[option_name]]' => 'scheduler_set_temp_users_alignment_users_link_table_check_index'
            ]);
            $arCheck = array_unique($arCheck);

            /*DEBUG*/
            if ($debug) {
                echo "Check Users: ".count($arCheck)."\n";
            }
            /*END DEBUG*/

            $arPresent = [];
            $makeQueryFindface = (new Query())->select(['[[internet_users_id]]'])
                ->from(Findface::tableName())
                ->where(['[[alignment]]' => 'alignment'])
                ->andWhere(['in', '[[soc_network]]', [
                    InternetUser::SOC_NETWORK_SHORT_TITLES[InternetUser::SOC_NETWORK_ID_VK],
                    InternetUser::SOC_NETWORK_SHORT_TITLES[InternetUser::SOC_NETWORK_ID_OK]
                ]])
                ->andWhere(['in','[[internet_users_id]]',$arCheck])
                ->all();
            if (count($makeQueryFindface)) {
                foreach ($makeQueryFindface as $presentInFindface) {
                    $arPresent[] = $presentInFindface['internet_users_id'];
                }
            }
            $needToDelete = array_diff($arCheck, $arPresent);

            /*DEBUG*/
            if ($debug) {
                echo "Need To Delete not alignment users: ".count($needToDelete)."\n";
                print_r($needToDelete);
            }
            /*END DEBUG*/

            if (count($needToDelete)) {
                TempUsersAlignmentUsersLink::deleteAll(['[[alignment_internet_user_id]]' => $needToDelete]);
            }
        } else {
            /*Обновляем оффсет*/
            Options::updateAll([
                '[[option_value]]' => '0'
            ], [
                '[[option_name]]' => 'scheduler_set_temp_users_alignment_users_link_table_check_index'
            ]);
        }
        /*END Check if is alignment*/

        /*Получаем настройку Оффсета*/
        $optionsQuery = (new Query())->select(['[[option_value]]'])
            ->from(Options::tableName())
            ->where(['[[option_name]]' => 'scheduler_set_temp_users_alignment_users_link_table_offset'])
            ->one();
        if (isset($optionsQuery['option_value']) && !empty($optionsQuery['option_value'])) {
            $offset = intval($optionsQuery['option_value']);
        } elseif (!isset($optionsQuery['option_value'])) {
            $newOption = new Options();
            $newOption->option_name = 'scheduler_set_temp_users_alignment_users_link_table_offset';
            $newOption->option_value = '0';
            $newOption->save();
        }
        /*Обновляем оффсет*/
        $maxOffset = $offset + $offsetLimit;
        Options::updateAll([
            '[[option_value]]' => $maxOffset
        ], [
            '[[option_name]]' => 'scheduler_set_temp_users_alignment_users_link_table_offset'
        ]);

        do {
            $makeQuery = (new Query())->select([
                'ff.[[internet_users_id]]',
                'iu.[[soc_network_id]]',
                'iu.[[soc_network_user_id]]'
                ])->distinct()
                ->from(['ff' => Findface::tableName()])
                ->innerJoin(['iu' => InternetUser::tableName()], 'iu.[[id]] = ff.[[internet_users_id]]')
                ->where(['ff.[[alignment]]' => 'alignment'])
                ->andWhere(['in', 'ff.[[soc_network]]', [
                    InternetUser::SOC_NETWORK_SHORT_TITLES[InternetUser::SOC_NETWORK_ID_VK],
                    InternetUser::SOC_NETWORK_SHORT_TITLES[InternetUser::SOC_NETWORK_ID_OK]
                ]])
                ->orderBy('ff.[[id]]')
                ->limit($itemsPerPage)
                ->offset($offset)
                ->all();

            /*DEBUG*/
            if ($debug) {
                echo "Users: ".count($makeQuery)." / Offset: ".$offset."\n";
                print_r($makeQuery);
            }
            /*END DEBUG*/

            if (count($makeQuery)) {
                $offset += $itemsPerPage;
                foreach ($makeQuery as $oneUser) {

                    /*FRIENDS*/
                    $simpleNewFriends = $simpleCurrentFriends = [];

                    $queryFriendsConstruct = (new Query())
                        ->select([
                            'iu.[[id]]'
                        ])
                        ->where(['friends.[[user_id]]' => $oneUser['soc_network_user_id']]);
                    switch ($oneUser['soc_network_id']) {
                        case InternetUser::SOC_NETWORK_ID_VK:
                            $queryFriendsConstruct->from(['friends' => 'vk_users_friends'])
                                ->innerJoin(
                                    ['iu' => InternetUser::tableName()],
                                '(iu.[[soc_network_id]] = '.InternetUser::SOC_NETWORK_ID_VK.') AND (friends.[[friend_id]] = iu.[[soc_network_user_id]])'
                                );
                            break;
                        case InternetUser::SOC_NETWORK_ID_OK:
                            $queryFriendsConstruct->from(['friends' => 'ok_users_friends'])
                                ->innerJoin(
                                    ['iu' => InternetUser::tableName()],
                                '(iu.[[soc_network_id]] = '.InternetUser::SOC_NETWORK_ID_OK.') AND (friends.[[friend_id]] = iu.[[soc_network_user_id]])'
                                );
                            break;
                    }
                    $queryFriends = $queryFriendsConstruct->all();
                    if (count($queryFriends)) {
                        foreach ($queryFriends as $oneFriend) {
                            $simpleNewFriends[] = $oneFriend['id'];
                        }
                    }
                    /*DEBUG*/
                    if ($debug) {
                        echo "\n".$oneUser['soc_network_user_id']." (ID:".$oneUser['internet_users_id'].") New Friends (".count($simpleNewFriends)."): ".implode(', ', $simpleNewFriends)."\n";
                    }
                    /*END DEBUG*/

                    $checkCurrentFriends = (new Query())->select([
                            '[[id]]',
                            '[[internet_user_id]]'
                        ])->from(TempUsersAlignmentUsersLink::tableName())
                        ->where([
                            '[[alignment_internet_user_id]]' => $oneUser['internet_users_id'],
                            '[[type]]' => 'fr'
                        ])->all();
                    if (count($checkCurrentFriends)) {
                        foreach ($checkCurrentFriends as $oneExist) {
                            $simpleCurrentFriends[$oneExist['id']] = $oneExist['internet_user_id'];
                        }
                    }
                    /*DEBUG*/
                    if ($debug) {
                        echo $oneUser['soc_network_user_id']." (ID:".$oneUser['internet_users_id'].") Current Friends (".count($simpleCurrentFriends)."): ".implode(', ', $simpleCurrentFriends)."\n";
                    }
                    /*END DEBUG*/

                    $setOldUsers = array_diff($simpleCurrentFriends, $simpleNewFriends);
                    $setNewUsers = array_diff($simpleNewFriends, $simpleCurrentFriends);

                    /*DEBUG*/
                    if ($debug) {
                        echo $oneUser['soc_network_user_id']." (ID:".$oneUser['internet_users_id'].") Need To Delete (".count($setOldUsers)."): ".implode(', ', $setOldUsers)."\n";
                        echo $oneUser['soc_network_user_id']." (ID:".$oneUser['internet_users_id'].") Need To Add (".count($setNewUsers)."): ".implode(', ', $setNewUsers)."\n";
                    }
                    /*END DEBUG*/

                    if (count($setOldUsers)) {
                        $chunkedOldUsers = array_chunk($setOldUsers, $chunkedCount, true);
                        foreach ($chunkedOldUsers as $oneChunkOldUsers) {
                            $oldFriendsIds = array_keys($oneChunkOldUsers);
                            TempUsersAlignmentUsersLink::deleteAll(['[[id]]' => $oldFriendsIds]);
                        }
                    }

                    if (count($setNewUsers)) {
                        $chunkedNewUsers = array_chunk($setNewUsers, $chunkedCount);
                        foreach ($chunkedNewUsers as $oneChunkNewUsers) {
                            foreach ($oneChunkNewUsers as &$newUser) {
                                $newUser = "('fr',".$newUser.",".$oneUser['internet_users_id'].")";
                            }
                            Yii::$app->db->createCommand("INSERT INTO temp_users_alignment_users_link (type,internet_user_id,alignment_internet_user_id) VALUES ".implode(',', $oneChunkNewUsers))->execute();
                        }
                    }
                    /*END FRIENDS*/

                    /*FOLLOWERS*/
                    if ($oneUser['soc_network_id'] == InternetUser::SOC_NETWORK_ID_VK) {

                    $simpleNewFollowers = $simpleCurrentFollowers = [];

                    $queryFollowersConstruct = (new Query())
                        ->select([
                            'iu.[[id]]'
                        ])
                        ->where(['followers.[[user_id]]' => $oneUser['soc_network_user_id']]);
                    switch ($oneUser['soc_network_id']) {
                        case InternetUser::SOC_NETWORK_ID_VK:
                            $queryFollowersConstruct->from(['followers' => 'vk_users_followers'])
                                ->innerJoin(
                                    ['iu' => InternetUser::tableName()],
                                    '(iu.[[soc_network_id]] = '.InternetUser::SOC_NETWORK_ID_VK.') AND (followers.[[follower_id]] = iu.[[soc_network_user_id]])'
                                );
                            break;
                    }
                    $queryFollowers = $queryFollowersConstruct->all();
                    if (count($queryFollowers)) {
                        foreach ($queryFollowers as $oneFollower) {
                            $simpleNewFollowers[] = $oneFollower['id'];
                        }
                    }
                    /*DEBUG*/
                    if ($debug) {
                        echo $oneUser['soc_network_user_id']." (ID:".$oneUser['internet_users_id'].") New Followers (".count($simpleNewFollowers)."): ".implode(', ', $simpleNewFollowers)."\n";
                    }
                    /*END DEBUG*/

                    $checkCurrentFollowers = (new Query())->select([
                        '[[id]]',
                        '[[internet_user_id]]'
                    ])->from(TempUsersAlignmentUsersLink::tableName())
                        ->where([
                            '[[alignment_internet_user_id]]' => $oneUser['internet_users_id'],
                            '[[type]]' => 'fl'
                        ])->all();
                    if (count($checkCurrentFollowers)) {
                        foreach ($checkCurrentFollowers as $oneExistFollowe) {
                            $simpleCurrentFollowers[$oneExistFollowe['id']] = $oneExistFollowe['internet_user_id'];
                        }
                    }
                    /*DEBUG*/
                    if ($debug) {
                        echo $oneUser['soc_network_user_id']." (ID:".$oneUser['internet_users_id'].") Current Followers (".count($simpleCurrentFollowers)."): ".implode(', ', $simpleCurrentFollowers)."\n";
                    }
                    /*END DEBUG*/

                    $setOldUsers = array_diff($simpleCurrentFollowers, $simpleNewFollowers);
                    $setNewUsers = array_diff($simpleNewFollowers, $simpleCurrentFollowers);

                    /*DEBUG*/
                    if ($debug) {
                        echo $oneUser['soc_network_user_id']." (ID:".$oneUser['internet_users_id'].") Need To Delete (".count($setOldUsers)."): ".implode(', ', $setOldUsers)."\n";
                        echo $oneUser['soc_network_user_id']." (ID:".$oneUser['internet_users_id'].") Need To Add (".count($setNewUsers)."): ".implode(', ', $setNewUsers)."\n";
                    }
                    /*END DEBUG*/

                    if (count($setOldUsers)) {
                        $chunkedOldUsers = array_chunk($setOldUsers, $chunkedCount, true);
                        foreach ($chunkedOldUsers as $oneChunkOldFollowers) {
                            $oldFollowers = array_keys($oneChunkOldFollowers);
                            TempUsersAlignmentUsersLink::deleteAll(['[[id]]' => $oldFollowers]);
                        }
                    }

                    if (count($setNewUsers)) {
                        $chunkedNewUsers = array_chunk($setNewUsers, $chunkedCount);
                        foreach ($chunkedNewUsers as $oneChunkNewFollowers) {
                            foreach ($oneChunkNewFollowers as &$newFollower) {
                                $newFollower = "('fl',".$newFollower.",".$oneUser['internet_users_id'].")";
                            }
                            Yii::$app->db->createCommand("INSERT INTO temp_users_alignment_users_link (type,internet_user_id,alignment_internet_user_id) VALUES ".implode(',', $oneChunkNewFollowers))->execute();
                        }
                    }

                    }
                    /*END FOLLOWERS*/
                }
            } else {
                Options::updateAll([
                    '[[option_value]]' => '0'
                ], [
                    '[[option_name]]' => 'scheduler_set_temp_users_alignment_users_link_table_offset'
                ]);
                break;
            }
        } while ($offset < $maxOffset);

        $this->execTime('end', $arArgs);

        return ExitCode::OK;
    }

    public function actionCountUsersWithAlignmentForPosts()
    {
        exec("ps aux|grep 'scheduler/count-users-with-alignment-for-posts'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $this->execTime('start', func_get_args());

        $itemsPerPage = 100;
        $offset = 0;
        $offsetLimit = 1000;

        $currentTime = time();
        /*Получаем настройку Оффсета*/
        $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'scheduler_count_users_with_alignment_for_posts_offset'")->queryOne();
        if (isset($makeQuery['option_value']) && intval($makeQuery['option_value'])) {
            $offset = intval($makeQuery['option_value']);
        } elseif (!isset($makeQuery['option_value'])) {
            Yii::$app->db->createCommand()->insert('options', [
                'option_name' => 'scheduler_count_users_with_alignment_for_posts_offset',
                'option_value' => '0'
            ])->execute();
        }

        /*Обновляем оффсет*/
        $maxOffset = $offset + $offsetLimit;
        $query = Yii::$app->db->CreateCommand("SELECT COUNT(id) AS items_count FROM posts WHERE up_time > ".$currentTime)->queryOne();
        $itemsCount = $query['items_count'];
        if ($maxOffset < $itemsCount) {
            $saveOffset = $maxOffset;
        } else {
            $saveOffset = 0;
        }
        Yii::$app->db->createCommand()->update('options', ['option_value' => $saveOffset], ['option_name' => 'scheduler_count_users_with_alignment_for_posts_offset'])->execute();

        do {

            $sql = "
SELECT posts.id, posts.post_id, posts.owner_id, posts.soc_network_id, 
       posts.comments, posts.likes, ( IF (posts.comments > 0,
	CASE posts.soc_network_id
		WHEN 1 THEN 
		(
			SELECT COUNT(post_comments.id) FROM post_comments WHERE 
				(post_comments.post_id = posts.post_id) 
			AND 
				(post_comments.owner_id = posts.owner_id) 
			AND 
				(post_comments.soc_network_id = 1) 
			AND 
				(post_comments.from_user_id IN (
						SELECT soc_network_user_id FROM internet_users WHERE (active = 1) AND (soc_network_id = 1)
					)
				)
			AND 
				(post_comments.from_user_id IN (
						SELECT user_id FROM findface WHERE (soc_network = 'vk') AND (alignment = 'alignment')
					)
				)
		)
        WHEN 2 THEN 
		(
        	SELECT COUNT(post_comments.id) FROM post_comments WHERE 
				(post_comments.post_id = posts.post_id) 
			AND 
				(post_comments.owner_id = posts.owner_id) 
			AND 
				(post_comments.soc_network_id = 2) 
			AND 
				(post_comments.from_user_id IN (
						SELECT soc_network_user_id FROM internet_users WHERE (active = 1) AND (soc_network_id = 2)
					)
				)
			AND 
				(post_comments.from_user_id IN (
						SELECT user_id FROM findface WHERE (soc_network = 'ok') AND (alignment = 'alignment')
					)
				)
		)
        WHEN 3 THEN 
		(
            SELECT COUNT(post_comments.id) FROM post_comments WHERE 
				(post_comments.post_id = posts.post_id) 
			AND 
				(post_comments.owner_id = posts.owner_id) 
			AND 
				(post_comments.soc_network_id = 3) 
			AND 
				(post_comments.from_user_id IN (
						SELECT soc_network_user_id FROM internet_users WHERE (active = 1) AND (soc_network_id = 3)
					)
				)
			AND 
				(post_comments.from_user_id IN (
						SELECT user_id FROM findface WHERE (soc_network = 'fb') AND (alignment = 'alignment')
					)
				)
		)
		ELSE 0
	END
, 0)) AS cnt_comments_alignment, ( IF (posts.likes > 0,
	CASE posts.soc_network_id
		WHEN 1 THEN 
		(
            SELECT COUNT(post_comment_likes.id) FROM post_comment_likes WHERE 
				(post_comment_likes.liked_item_id = posts.post_id) 
			AND 
				(post_comment_likes.owner_id = posts.owner_id) 
			AND 
				(post_comment_likes.soc_network_id = 1) 
			AND 
				(post_comment_likes.from_user_id IN (
						SELECT soc_network_user_id FROM internet_users WHERE (active = 1) AND (soc_network_id = 1)
					)
				)
			AND 
				(post_comment_likes.from_user_id IN (
						SELECT user_id FROM findface WHERE (soc_network = 'vk') AND (alignment = 'alignment')
					)
				)
		)
        WHEN 2 THEN 
		(
			SELECT COUNT(post_comment_likes.id) FROM post_comment_likes WHERE 
				(post_comment_likes.liked_item_id = posts.post_id) 
			AND 
				(post_comment_likes.owner_id = posts.owner_id) 
			AND 
				(post_comment_likes.soc_network_id = 2) 
			AND 
				(post_comment_likes.from_user_id IN (
						SELECT soc_network_user_id FROM internet_users WHERE (active = 1) AND (soc_network_id = 2)
					)
				)
			AND 
				(post_comment_likes.from_user_id IN (
						SELECT user_id FROM findface WHERE (soc_network = 'ok') AND (alignment = 'alignment')
					)
				)
		)
        WHEN 3 THEN 
		(
			SELECT COUNT(post_comment_likes.id) FROM post_comment_likes WHERE 
				(post_comment_likes.liked_item_id = posts.post_id) 
			AND 
				(post_comment_likes.owner_id = posts.owner_id) 
			AND 
				(post_comment_likes.soc_network_id = 3) 
			AND 
				(post_comment_likes.from_user_id IN (
						SELECT soc_network_user_id FROM internet_users WHERE (active = 1) AND (soc_network_id = 3)
					)
				)
			AND 
				(post_comment_likes.from_user_id IN (
						SELECT user_id FROM findface WHERE (soc_network = 'fb') AND (alignment = 'alignment')
					)
				)
		)
		ELSE 0
	END
, 0)) AS cnt_likes_alignment FROM posts WHERE posts.up_time > {$currentTime} LIMIT {$itemsPerPage} OFFSET {$offset}
            ";

            $makeQuery = Yii::$app->db->createCommand($sql)->queryAll();
            if (count($makeQuery)) {
                $setNullArray = [];
                foreach ($makeQuery as $oneItem) {

                    if ($oneItem['cnt_comments_alignment'] || $oneItem['cnt_likes_alignment']) {

                        //print_r($oneItem);

                        $proc_comments_alignment = $oneItem['comments']?round($oneItem['cnt_comments_alignment']/$oneItem['comments'], 3)*100:0;
                        $proc_likes_alignment = $oneItem['likes']?round($oneItem['cnt_likes_alignment']/$oneItem['likes'], 3)*100:0;
                        Yii::$app->db->createCommand()->update('posts', [
                            'cnt_comments_alignment' => $oneItem['cnt_comments_alignment'],
                            'cnt_likes_alignment' => $oneItem['cnt_likes_alignment'],
                            'proc_comments_alignment' => $proc_comments_alignment,
                            'proc_likes_alignment' => $proc_likes_alignment
                        ], ['id' => $oneItem['id']])->execute();
                    } else {
                        $setNullArray[] = $oneItem['id'];
                    }
                }
                if (count($setNullArray)) {
                    Yii::$app->db->createCommand("UPDATE posts SET cnt_comments_alignment = 0, cnt_likes_alignment = 0,proc_comments_alignment = 0, proc_likes_alignment = 0 WHERE id IN (".implode(',',$setNullArray).")")->execute();
                }
                $offset += $itemsPerPage;
                //usleep(200000);

                //echo "\n".$offset;

            } else {
                break;
            }

        } while ($offset < $maxOffset);

        $this->execTime('end', func_get_args());

        return ExitCode::OK;

    }

    public function actionUpdateVkUsersFriendsFollowers()
    {

        return ExitCode::OK;


        exec("ps aux|grep 'scheduler/update-vk-users-friends-followers'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > 3) {
            return ExitCode::OK;
        }

        $itemsPerPage = 100;
        $socNetworkId = 1;
        $usersLog = [];
        $counter = 0;
        $offset = 0;

        /*Получаем настройку Оффсета*/
        $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'scheduler_update_vk_users_friends_followers_offset'")->queryOne();
        if (isset($makeQuery['option_value']) && !empty($makeQuery['option_value'])) {
            $offset = intval($makeQuery['option_value']);
        } elseif (!isset($makeQuery['option_value'])) {
            Yii::$app->db->createCommand()->insert('options', [
                'option_name' => 'scheduler_update_vk_users_friends_followers_offset',
                'option_value' => '0'
            ])->execute();
        }

        /*Обновляем оффсет*/
        $maxOffset = $offset + $itemsPerPage;
        Yii::$app->db->createCommand()->update('options', ['option_value' => $maxOffset], ['option_name' => 'scheduler_update_vk_users_friends_followers_offset'])->execute();

        /*получаем юзеров данные по которым необходимо обновить*/
        $makeQuery = Yii::$app->db->createCommand("SELECT soc_network_user_id AS vk_id FROM internet_users WHERE (soc_network_id = 1) AND (active = 1) ORDER BY id ASC LIMIT {$itemsPerPage} OFFSET {$offset}")->queryAll();

        if (count($makeQuery)) {
            foreach ($makeQuery as $user) {

//                $counter++;
//                echo $user['vk_id']." users / counter: ".$counter."\n";

                $arGroups = VkontakteApi::loadVkGroups($user['vk_id']);
                $arFriends = VkontakteApi::loadVkFriends($user['vk_id']);
                $arFollowers = VkontakteApi::loadVkFollowers($user['vk_id']);
                $usersLog = VkontakteApi::loadVkUser($user['vk_id'], $arGroups, $arFriends, $arFollowers);

                /*$presentFriends = $presentFollowers = [];
                $arraysFriends = array_chunk($arFriends, 1000);
                foreach ($arraysFriends as $arrayFiends) {
                    $makeQuery = Yii::$app->db->createCommand("SELECT soc_network_user_id FROM internet_users WHERE soc_network_user_id IN (" . implode(',', $arrayFiends) . ") AND (soc_network_id = {$socNetworkId})")->queryAll();
                    foreach ($makeQuery as $oneUser) {
                        $presentFriends[] = $oneUser['soc_network_user_id'];
                    }
                }

//                echo "Present Friends: "." ".count($presentFriends)."\n";

                $arFriends = array_diff($arFriends, $presentFriends);

//                echo "NeedToGet Friends: ".implode(',',$arFriends)." ".count($arFriends)."\n";

                $arraysFollowers = array_chunk($arFollowers, 1000);
                foreach ($arraysFollowers as $arrayFollowers) {
                    $makeQuery = Yii::$app->db->createCommand("SELECT soc_network_user_id FROM internet_users WHERE soc_network_user_id IN (" . implode(',', $arrayFollowers) . ") AND (soc_network_id = {$socNetworkId})")->queryAll();
                    foreach ($makeQuery as $oneUser) {
                        $presentFollowers[] = $oneUser['soc_network_user_id'];
                    }
                }

//                echo "Present Followers: "." ".count($presentFollowers)."\n";

                $arFollowers = array_diff($arFollowers, $presentFollowers);

//                echo "NeedToGet Followers: ".implode(',',$arFollowers)." ".count($arFollowers)."\n";
//                print_r($usersLog);

                if (count($arFriends)) {
                    foreach ($arFriends as $oneFriend) {

                        Yii::$app->db->createCommand()->upsert('new_soc_network_users', [
                            'soc_network_id' => $socNetworkId,
                            'soc_network_user_id' => $oneFriend
                        ], false)->execute();

//                        $counter++;
//                        echo "user ".$user['vk_id']." / friend ".$oneFriend." / counter: ".$counter."\n";

                    }
                }

                if (count($arFollowers)) {
                    foreach ($arFollowers as $oneFollower) {

                        Yii::$app->db->createCommand()->upsert('new_soc_network_users', [
                            'soc_network_id' => $socNetworkId,
                            'soc_network_user_id' => $oneFollower
                        ], false)->execute();

//                        $counter++;
//                        echo "user ".$user['vk_id']." / follower ".$oneFollower." / counter: ".$counter."\n";

                    }
                }*/

            }
        } else {
            Yii::$app->db->createCommand()->update('options', ['option_value' => '0'], ['option_name' => 'scheduler_update_vk_users_friends_followers_offset'])->execute();
        }

        return ExitCode::OK;
    }

    public function actionUpdateOkUsersFriendsFollowers()
    {

        return ExitCode::OK;


        exec("ps aux|grep 'scheduler/update-ok-users-friends-followers'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }

        $itemsPerPage = 1000;
        $socNetworkId = 2;
        $usersLog = [];
        $counter = 0;
        $offset = 0;

        /*Получаем настройку Оффсета*/
        $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'scheduler_update_ok_users_friends_followers_offset'")->queryOne();
        if (isset($makeQuery['option_value']) && !empty($makeQuery['option_value'])) {
            $offset = intval($makeQuery['option_value']);
        } elseif (!isset($makeQuery['option_value'])) {
            Yii::$app->db->createCommand()->insert('options', [
                'option_name' => 'scheduler_update_ok_users_friends_followers_offset',
                'option_value' => '0'
            ])->execute();
        }

        /*Обновляем оффсет*/
        $maxOffset = $offset + $itemsPerPage;
        Yii::$app->db->createCommand()->update('options', ['option_value' => $maxOffset], ['option_name' => 'scheduler_update_ok_users_friends_followers_offset'])->execute();

        /*получаем юзеров данные по которым необходимо обновить*/
        $makeQuery = Yii::$app->db->createCommand("SELECT soc_network_user_id AS ok_id FROM internet_users WHERE (soc_network_id = 2) AND (active = 1) ORDER BY id ASC LIMIT {$itemsPerPage} OFFSET {$offset}")->queryAll();

        if (count($makeQuery)) {
            foreach ($makeQuery as $user) {

//                $counter++;
//                echo $user['ok_id']." users / counter: ".$counter."\n";

                $arGroups = OdnoklassnikiApi::loadOkGroups($user['ok_id']);
                $arFriends = OdnoklassnikiApi::loadOkFriends($user['ok_id']);
                $usersLog = OdnoklassnikiApi::loadOkUser($user['ok_id'], $arGroups, $arFriends);

                /*$presentFriends = $presentFollowers = [];
                $arraysFriends = array_chunk($arFriends, 1000);
                foreach ($arraysFriends as $arrayFiends) {
                    $makeQuery = Yii::$app->db->createCommand("SELECT soc_network_user_id FROM internet_users WHERE soc_network_user_id IN (" . implode(',', $arrayFiends) . ") AND (soc_network_id = {$socNetworkId})")->queryAll();
                    foreach ($makeQuery as $oneUser) {
                        $presentFriends[] = $oneUser['soc_network_user_id'];
                    }
                }

//                echo "Present Friends: "." ".count($presentFriends)."\n";

                $arFriends = array_diff($arFriends, $presentFriends);

//                echo "NeedToGet Friends: ".implode(',',$arFriends)." ".count($arFriends)."\n";
//                print_r($usersLog);

                if (count($arFriends)) {
                    foreach ($arFriends as $oneFriend) {

                        Yii::$app->db->createCommand()->upsert('new_soc_network_users', [
                            'soc_network_id' => $socNetworkId,
                            'soc_network_user_id' => $oneFriend
                        ], false)->execute();

//                        $counter++;
//                        echo "user ".$user['ok_id']." / friend ".$oneFriend." / counter: ".$counter."\n";

                    }
                }*/
            }
        } else {
            Yii::$app->db->createCommand()->update('options', ['option_value' => '0'], ['option_name' => 'scheduler_update_0k_users_friends_followers_offset'])->execute();
        }

        return ExitCode::OK;
    }

    public function actionGetNewSocNetworkUsers()
    {

        return ExitCode::OK;


        exec("ps aux|grep 'scheduler/get-new-soc-network-users'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }

        $itemsPerPage = 100;
        $offset = 0;
        $maxOffset = 2000;
        $lostItems = [];

        /*Попытка найти потеряные / пропущенные записи из-за краша скрипта*/
        $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'scheduler_get_new_soc_network_users_index'")->queryOne();
        if (isset($makeQuery['option_value']) && !empty($makeQuery['option_value']) && intval($makeQuery['option_value'])) {
            $index = intval($makeQuery['option_value']);
            $lostId = 0;
            /*Получем сохраненную первую из утеряных ИДшок*/
            $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'scheduler_get_new_soc_network_users_lost_id'")->queryOne();
            if (isset($makeQuery['option_value']) && intval($makeQuery['option_value'])) {
                $lostId = intval($makeQuery['option_value']);
            } elseif (!isset($makeQuery['option_value'])) {
                Yii::$app->db->createCommand()->insert('options', [
                    'option_name' => 'scheduler_get_new_soc_network_users_lost_id',
                    'option_value' => '0'
                ])->execute();
            }
            /*Делаем запрос, ищем утеряные записи*/
            $makeQueryLost = Yii::$app->db->createCommand("SELECT id, soc_network_id, soc_network_user_id FROM new_soc_network_users WHERE (id < " . $index . ") ORDER BY id LIMIT " . $itemsPerPage)->queryAll();
            if (is_array($makeQueryLost) && count($makeQueryLost)) {
                $firstLostId = reset($makeQueryLost);
                /*Если записи есть, смотрим меняется ли первая ИДшка*/
                if ($firstLostId['id'] == $lostId) {
                    /*ИДшка не изменилась, тогда проверяем не высткпает ли последняя ИДшка за текущую позицию*/
                    $lastLostId = end($makeQueryLost);
                    if ($lastLostId['id'] < $index) {
                        $lostItems = $makeQueryLost;
                    }
                } else {
                    Yii::$app->db->createCommand()->update('options', ['option_value' => $firstLostId['id']], ['option_name' => 'scheduler_get_new_soc_network_users_lost_id'])->execute();
                }
            } else {
                Yii::$app->db->createCommand()->update('options', ['option_value' => 0], ['option_name' => 'scheduler_get_new_soc_network_users_lost_id'])->execute();
            }
        }

        do {

            if (count($lostItems) == 0) {
                $index = 0;
                /*Получаем настройку ИД*/
                $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'scheduler_get_new_soc_network_users_index'")->queryOne();
                if (isset($makeQuery['option_value']) && !empty($makeQuery['option_value']) && intval($makeQuery['option_value'])) {
                    $index = intval($makeQuery['option_value']);
                } elseif (!isset($makeQuery['option_value'])) {
                    Yii::$app->db->createCommand()->insert('options', [
                        'option_name' => 'scheduler_get_new_soc_network_users_index',
                        'option_value' => '0'
                    ])->execute();
                }
                /*Выборка новых елементов*/
                if ($index) {
                    $sql = "SELECT id, soc_network_id, soc_network_user_id FROM new_soc_network_users WHERE (id > " . $index . ") ORDER BY id LIMIT " . $itemsPerPage;
                } else {
                    $sql = "SELECT id, soc_network_id, soc_network_user_id FROM new_soc_network_users ORDER BY id LIMIT " . $itemsPerPage;
                }

                $newItems = Yii::$app->db->CreateCommand($sql)->queryAll();
                if (!count($newItems)) {
                    break;
                }
                /*Обновляем ИД в настройку*/
                $lastItem = end($newItems);
                Yii::$app->db->createCommand()->update('options', ['option_value' => $lastItem['id']], ['option_name' => 'scheduler_get_new_soc_network_users_index'])->execute();

                $offset += $itemsPerPage;

            } else {
                $newItems = $lostItems;
                $lostItems = [];
            }

            foreach ($newItems as $newItem) {

                $usersLog = false;
                switch ($newItem['soc_network_id']) {
                    case 1:
                        $arGroupsFriend = VkontakteApi::loadVkGroups($newItem['soc_network_user_id']);
                        $arFriendsFriend = VkontakteApi::loadVkFriends($newItem['soc_network_user_id']);
                        $arFollowersFriend = VkontakteApi::loadVkFollowers($newItem['soc_network_user_id']);
                        $usersLog = VkontakteApi::loadVkUser($newItem['soc_network_user_id'], $arGroupsFriend, $arFriendsFriend, $arFollowersFriend);

                        //print_r($usersLog);

                        break;
                    case 2:

                        $arGroupsFriend = OdnoklassnikiApi::loadOkGroups($newItem['soc_network_user_id']);
                        $arFriendsFriend = OdnoklassnikiApi::loadOkFriends($newItem['soc_network_user_id']);
                        $usersLog = OdnoklassnikiApi::loadOkUser($newItem['soc_network_user_id'], $arGroupsFriend, $arFriendsFriend);

                        //print_r($usersLog);

                        break;
                    case 3:
                        break;
                }

                if ($usersLog) {
                    Yii::$app->db->CreateCommand()->delete('new_soc_network_users', ['id' => $newItem['id']])->execute();
                }

            }

        } while ($offset < $maxOffset);

        return ExitCode::OK;
    }

    public function actionGetVkUsersFromWatchGroups()
    {
        exec("ps aux|grep 'scheduler/get-vk-users-from-watch-groups'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > 3) {
            return ExitCode::OK;
        }

        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);
        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        $specWords = [
            'exectime',
            'debug',
        ];
        /*END DEBUG*/

        $arGroupsVkIds = $arGroupsMembers = $arGroupsIds = $makeQuery = [];

        $index = 0;
        /*Получаем настройку Оффсета*/
        $optionsQuery = (new Query())->select(['[[option_value]]'])
            ->from(Options::tableName())
            ->where(['[[option_name]]' => 'scheduler_get_vk_users_from_watch_groups_index'])
            ->one();
        if (isset($optionsQuery['option_value']) && !empty($optionsQuery['option_value'])) {
            $index = intval($optionsQuery['option_value']);
        } elseif (!isset($optionsQuery['option_value'])) {
            $newOption = new Options();
            $newOption->option_name = 'scheduler_get_vk_users_from_watch_groups_index';
            $newOption->option_value = '0';
            $newOption->save();
        }

        if (empty($arArgs)) {
            foreach ($arArgs as $oneArg) {
                if (intval($oneArg) && !in_array($oneArg, $specWords)) {
                    $makeQuery[] = ['vk_id' => $oneArg];
                }
            }
        }
        if (count($makeQuery) == 0) {
            $makeQuery = (new Query())->select(['[[id]]','[[vk_id]]','[[members]]'])
                ->from(VkGroups::tableName())
                ->where(['[[watch]]' => 1,])
                ->orderBy('[[id]]')
                ->all();
        }

        foreach ($makeQuery as $oneGroup) {
            $arGroupsVkIds[$oneGroup['id']] = $oneGroup['vk_id'];
            $arGroupsMembers[$oneGroup['vk_id']] = !empty($oneGroup['members'])?$oneGroup['members']:0;
            $arGroupsIds[] = $oneGroup['id'];
        }

        /*DEBUG*/
        if ($debug) {
            echo "Groups with watch = 1\n";
            print_r($arGroupsVkIds);
        }
        /*END DEBUG*/

        //если ошибки - пробовать изменять параметр, делать меньше
        //потому что слишком большой - будет не влезать запрос с большим количеством ИД
        //если слишком маленький - будет часто дергать апи ВК из-за чего получит отбой, будут пропуска
        //можно увеличить слип внизу цикла, но это увеличит время выполения...
        $itemsPerPage = 50;
        $offsetLimit = 50;
        $usersLimit = 50;

        foreach ($arGroupsVkIds as $oneGroupId => $oneGroup) {

            if ($oneGroupId < $index) {
                continue;
            }

            $makeQuery = Yii::$app->db->createCommand("SELECT members_offset FROM vk_groups WHERE vk_id = ". $oneGroup)->queryOne();
            $arGroupsMembersOffset = !empty($makeQuery['members_offset'])?$makeQuery['members_offset']:0;

            $offset = $arGroupsMembersOffset;

            $maxOffset = $arGroupsMembersOffset + $offsetLimit;
            if ($maxOffset < $arGroupsMembers[$oneGroup] || ($arGroupsMembers[$oneGroup] == 0 && $arGroupsMembersOffset == 0))
                $saveCurrentOffset = $maxOffset;
            else
                $saveCurrentOffset = 0;
            Yii::$app->db->CreateCommand()->update('vk_groups', ['members_offset' => $saveCurrentOffset], ['vk_id' => $oneGroup] )->execute();

            do {

                $resultQuery = VkontakteSDK::makeQuery('groups.getMembers', [
                    'group_id' => $oneGroup,
                    'offset' => $offset,
                    'count' => $itemsPerPage
                ]);

                //usleep(400000);

                /*DEBUG*/
                if ($debug) {
                    echo "Current Group ID: ".$oneGroup."\n";
                    echo "Current Group API-Response: "."\n";
                    print_r($resultQuery);
                }
                /*END DEBUG*/

                $itemsArr = [];
                if(is_object($resultQuery) && property_exists($resultQuery, 'items') && count($resultQuery->items)) {
                    $offset += $itemsPerPage;
                    $itemsArr = $resultQuery->items;
                    $itemsStr = implode(',', $resultQuery->items);
                    $presentUsers = $needToGet = [];

                    /*DEBUG*/
                    if ($debug) {
                        echo "Group: ".$oneGroup." / Users count: ".$resultQuery->count." / Offset: ".$offset."\n\n";
                    }
                    /*END DEBUG*/

                    $makeQuery = Yii::$app->db->createCommand("SELECT soc_network_user_id FROM internet_users WHERE soc_network_user_id IN (" . $itemsStr . ") AND (soc_network_id = 1)")->queryAll();
                    foreach ($makeQuery as $oneUser) {
                        $presentUsers[] = $oneUser['soc_network_user_id'];
                    }
                    $needToGet = array_diff($itemsArr, $presentUsers);

                    /*DEBUG*/
                    if ($debug) {
                        echo "Present(".count($presentUsers)."): ".implode(',',$presentUsers)."\n\n";
                        echo "NeedToGet(".count($needToGet)."): ".implode(',',$needToGet)."\n\n";
                    }
                    /*END DEBUG*/

                    if (count($needToGet)) {
                        foreach ($needToGet as $userId) {
                            $arGroups = VkontakteApi::loadVkGroups($userId,[$oneGroup]);
                            $arFriends = VkontakteApi::loadVkFriends($userId);
                            $arFollowers = VkontakteApi::loadVkFollowers($userId);
                            $usersLog = VkontakteApi::loadVkUser($userId, $arGroups, $arFriends, $arFollowers);
                            $usersLimit--;

                            /*DEBUG*/
                            if ($debug) {
                                echo "Loaded User: ".$userId." / Groups: ".count($arGroups)." / Friends: ".count($arFriends)." / Followers: ".count($arFollowers)."\n";
                            }
                            /*END DEBUG*/
                        }
                    }
                }

            } while (count($itemsArr) && $offset < $maxOffset);

            if (is_object($resultQuery) && property_exists($resultQuery, 'count')) {
                VkGroups::updateAll([
                    '[[members]]' => $resultQuery->count
                ], [
                    '[[id]]' => $oneGroupId
                ]);
            }

            if ($usersLimit <= 0) {
                break;
            }
        }

        if ($oneGroupId == array_key_last($arGroupsVkIds)) {
            $index = array_key_first($arGroupsVkIds);
        } else {
            $key = array_search($oneGroupId, $arGroupsIds);
            $index = $arGroupsIds[$key+1];
        }
        Options::updateAll([
            '[[option_value]]' => $index
        ], [
            '[[option_name]]' => 'scheduler_get_vk_users_from_watch_groups_index'
        ]);

        /*DEBUG*/
        if ($debug) {
            echo "\nRESULT: ".(abs($usersLimit)+$offsetLimit)."\n";
        }
        /*END DEBUG*/

        $this->execTime('end', $arArgs);

        return ExitCode::OK;

    }

    public function actionGetOkUsersFromWatchGroups()
    {
        exec("ps aux|grep 'scheduler/get-ok-users-from-watch-groups'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);
        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        $specWords = [
            'exectime',
            'debug',
        ];
        /*END DEBUG*/

        $arGroupsOkIds = $arGroupsMembers = $arGroupsIds = $makeQuery = [];

        $index = 0;
        /*Получаем настройку Оффсета*/
        $optionsQuery = (new Query())->select(['[[option_value]]'])
            ->from(Options::tableName())
            ->where(['[[option_name]]' => 'scheduler_get_ok_users_from_watch_groups_index'])
            ->one();
        if (isset($optionsQuery['option_value']) && !empty($optionsQuery['option_value'])) {
            $index = intval($optionsQuery['option_value']);
        } elseif (!isset($optionsQuery['option_value'])) {
            $newOption = new Options();
            $newOption->option_name = 'scheduler_get_ok_users_from_watch_groups_index';
            $newOption->option_value = '0';
            $newOption->save();
        }

        if (empty($arArgs)) {
            foreach ($arArgs as $oneArg) {
                if (intval($oneArg) && !in_array($oneArg, $specWords)) {
                    $makeQuery[] = ['ok_id' => $oneArg];
                }
            }
        }
        if (count($makeQuery) == 0) {
            $makeQuery = (new Query())->select(['[[id]]','[[ok_id]]','[[members]]'])
                ->from(OkGroups::tableName())
                ->where(['[[watch]]' => 1,])
                ->orderBy('[[id]]')
                ->all();
        }

        foreach ($makeQuery as $oneGroup) {
            $arGroupsOkIds[$oneGroup['id']] = $oneGroup['ok_id'];
            $arGroupsMembers[$oneGroup['ok_id']] = !empty($oneGroup['members'])?$oneGroup['members']:0;
            $arGroupsIds[] = $oneGroup['id'];
        }

        /*DEBUG*/
        if ($debug) {
            echo "Groups with watch = 1\n";
            print_r($arGroupsOkIds);
        }
        /*END DEBUG*/

        $itemsPerPage = 50;
        $usersLimit = 50;

        foreach ($arGroupsOkIds as $oneGroupId => $oneGroup) {

            if ($oneGroupId < $index) {
                continue;
            }

            $makeQuery = Yii::$app->db->createCommand("SELECT members_offset, members_anchor FROM ok_groups WHERE ok_id = ". $oneGroup)->queryOne();

            $pagerAnchor = !empty($makeQuery['members_anchor'])?$makeQuery['members_anchor']:'';
            $arGroupsMembersOffset = !empty($makeQuery['members_offset'])?$makeQuery['members_offset']:0;

            do {

                $itemsArr = [];
                $res = OdnoklassnikiSDK::makeRequest(            //МАКСИМУМ 100
                    "group.getMembers",
                    array(
                        "uid" => $oneGroup,
                        "count" => $itemsPerPage,
                        "anchor" => $pagerAnchor,
                    )
                );

                /*DEBUG*/
                if ($debug) {
                    echo "Current Group ID: ".$oneGroup."\n";
                    echo "Current Group API-Response: "."\n";
                    print_r($res);
                }
                /*END DEBUG*/

                if (isset($res['anchor']) && $res['anchor']) {
                    $pagerAnchor = $res['anchor'];
                    $maxOffset = $arGroupsMembersOffset + $itemsPerPage;
                    if ($maxOffset < $arGroupsMembers[$oneGroup] || ($arGroupsMembers[$oneGroup] == 0 && $arGroupsMembersOffset == 0))
                        $saveCurrentOffset = [
                            'members_offset' => $maxOffset,
                            'members_anchor' => $pagerAnchor
                        ];
                    else
                        $saveCurrentOffset = [
                            'members_offset' => 0,
                            'members_anchor' => ''
                        ];
                    Yii::$app->db->CreateCommand()->update('ok_groups', $saveCurrentOffset, ['ok_id' => $oneGroup] )->execute();
                }
                else
                    $pagerAnchor = "";
                if (isset($res['members']) && is_array($res['members']) && count($res['members'])) {
                    foreach ($res['members'] as $member) {
                        $itemsArr[] = $member['userId'];
                    }
                }

                if (count($itemsArr)) {
                    $itemsStr = implode(',', $itemsArr);
                    $presentUsers = $needToGet = $presentUsersInGroup = [];

                    /*DEBUG*/
                    if ($debug) {
                        echo "Group: ".$oneGroup." / Users count: ".count($itemsArr)." / Offset: ".$pagerAnchor."\n\n";
                    }
                    /*END DEBUG*/

                    $makeQuery = Yii::$app->db->createCommand("SELECT soc_network_user_id FROM internet_users WHERE (soc_network_id = 2) AND (soc_network_user_id IN (".$itemsStr."))")->queryAll();
                    foreach ($makeQuery as $oneUser) {
                        $presentUsers[] = $oneUser['soc_network_user_id'];
                    }
                    $needToGet = array_diff($itemsArr, $presentUsers);

                    /*DEBUG*/
                    if ($debug) {
                        echo "Present(".count($presentUsers)."): ".implode(',',$presentUsers)."\n\n";
                        echo "NeedToGet(".count($needToGet)."): ".implode(',',$needToGet)."\n\n";
                    }
                    /*END DEBUG*/

                    if (count($needToGet)) {
                        foreach ($needToGet as $userId) {

                            $arGroups = OdnoklassnikiApi::loadOkGroups($userId,[$oneGroup]);
                            $arFriends = OdnoklassnikiApi::loadOkFriends($userId);
                            $usersLog = OdnoklassnikiApi::loadOkUser($userId, $arGroups, $arFriends);
                            $usersLimit--;

                            /*DEBUG*/
                            if ($debug) {
                                echo "Loaded User: ".$userId." / Groups: ".count($arGroups)." / Friends: ".count($arFriends)."\n";
                            }
                            /*END DEBUG*/
                        }
                    }

                }

            } while (/*is_array($res['members']) && count($res['members'])*/false);

            $resGroups = OdnoklassnikiSDK::makeRequest(            //МАКСИМУМ 100
                "group.getInfo",
                array("uids" => $oneGroup,
                    "fields" => "members_count"
                ), '', true
            );
            if (count($resGroups)) {
                $oneCountMembers = reset($resGroups);
                if(isset($oneCountMembers['members_count'])) {
                    OkGroups::updateAll([
                        '[[members]]' => $oneCountMembers['members_count']
                    ], [
                        '[[id]]' => $oneGroupId
                    ]);
                }
            }

            if ($usersLimit <= 0) {
                break;
            }
        }

        if ($oneGroupId == array_key_last($arGroupsOkIds)) {
            $index = array_key_first($arGroupsOkIds);
        } else {
            $key = array_search($oneGroupId, $arGroupsIds);
            $index = $arGroupsIds[$key+1];
        }
        Options::updateAll([
            '[[option_value]]' => $index
        ], [
            '[[option_name]]' => 'scheduler_get_ok_users_from_watch_groups_index'
        ]);

        /*DEBUG*/
        if ($debug) {
            echo "\nRESULT: ".(abs($usersLimit)+$itemsPerPage)."\n";
        }
        /*END DEBUG*/

        $this->execTime('end', $arArgs);

        return ExitCode::OK;

    }



    public function actionCalculateGeneralSocNetworksStatistics()
    {

        return ExitCode::OK;


        exec("ps aux|grep 'scheduler/calculate-general-soc-networks-statistics'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > 1) {
            return ExitCode::OK;
        }
        $this->execTime('start', func_get_args());

        $arResult = [];
        $arResult['groupsStatistics']['internet'] = 0;
        $arResult['groupsStatistics']['database'] = 0;
        $arResult['groupsStatistics']['title'] = 'Количество аккаунтов интернет пользователей в просматриваемых группах';
        $arResult['friendsStatistics']['internet'] = 0;
        $arResult['friendsStatistics']['database'] = 0;
        $arResult['friendsStatistics']['title'] = 'Количество аккаунтов друзей интересующих интернет пользователей в просматриваемых группах';
        $arResult['followersStatistics']['internet'] = 0;
        $arResult['followersStatistics']['database'] = 0;
        $arResult['followersStatistics']['title'] = 'Количество аккаунтов подписчиков интересующих интернет пользователей в просматриваемых группах';
        $arResult['postsStatistics']['internet'] = 0;
        $arResult['postsStatistics']['database'] = 0;
        $arResult['postsStatistics']['title'] = 'Количество публикаций в просматриваемых группах';
        $arResult['commentsStatistics']['internet'] = 0;
        $arResult['commentsStatistics']['database'] = 0;
        $arResult['commentsStatistics']['title'] = 'Количество комментариев в загруженных публикациях в просматриваемых группах';
        $arResult['likesStatistics']['internet'] = 0;
        $arResult['likesStatistics']['database'] = 0;
        $arResult['likesStatistics']['title'] = 'Количество лайков в загруженных публикациях в просматриваемых группах';

        /*Пользователи*/
        /*Сумма всех юзеров в просматриваемых группах*/
        $rsVkGroups = Yii::$app->db->createCommand("SELECT SUM(members) as summ FROM vk_groups WHERE watch = 1")->queryOne();
        $arResult['groupsStatistics']['internet'] += $rsVkGroups['summ'];

        $rsOkGroups = Yii::$app->db->createCommand("SELECT SUM(members) as summ FROM ok_groups WHERE watch = 1")->queryOne();
        $arResult['groupsStatistics']['internet'] += $rsOkGroups['summ'];

        $rsFbGroups = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ACCOUNT_ID) as summ FROM fb_group_user WHERE FB_GROUP_ID IN (SELECT FB_GROUP_ID FROM fb_groups WHERE watch = 1) AND (ACCOUNT_ID IN (SELECT fb_accounts.ACCOUNT_ID FROM fb_accounts WHERE fb_accounts.ACCOUNT_ID = fb_group_user.ACCOUNT_ID))")->queryOne();
        $arResult['groupsStatistics']['internet'] += $rsFbGroups['summ'];

        /*Сумма всех юзеров которые имеются в системе*/
        $rsVkGroups = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT vk_users_id) as summ FROM vk_users_groups WHERE (vk_groups_id IN (SELECT vk_id FROM vk_groups WHERE watch = 1)) AND (EXISTS (SELECT soc_network_user_id FROM internet_users WHERE (soc_network_id = 1) AND (internet_users.soc_network_user_id = vk_users_groups.vk_users_id) LIMIT 1))")->queryOne();
        $arResult['groupsStatistics']['database'] += $rsVkGroups['summ'];

        $rsOkGroups = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ok_users_id) as summ FROM ok_users_groups WHERE (ok_groups_id IN (SELECT ok_id FROM ok_groups WHERE watch = 1)) AND (EXISTS (SELECT soc_network_user_id FROM internet_users WHERE (soc_network_id = 2) AND (internet_users.soc_network_user_id = ok_users_groups.ok_users_id) LIMIT 1))")->queryOne();
        $arResult['groupsStatistics']['database'] += $rsOkGroups['summ'];

        $rsFbGroups = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ACCOUNT_ID) as summ FROM fb_group_user WHERE (FB_GROUP_ID IN (SELECT FB_GROUP_ID FROM fb_groups WHERE watch = 1)) AND (EXISTS (SELECT soc_network_user_id FROM internet_users WHERE (soc_network_id = 3) AND (internet_users.soc_network_user_id = fb_group_user.ACCOUNT_ID) LIMIT 1))")->queryOne();
        $arResult['groupsStatistics']['database'] += $rsFbGroups['summ'];

        /*Вычисление процента для групп*/
        $arResult['groupsStatistics']['procent'] = $arResult['groupsStatistics']['internet']?(round($arResult['groupsStatistics']['database']/$arResult['groupsStatistics']['internet'], 3))*100:0;

        /*Друзья*/
        /*Сумма всех друзей просматриваемых пользователей*/
        $rsVkFriends = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT friend_id) as summ FROM vk_users_friends WHERE (user_id IN (SELECT soc_network_user_id FROM internet_users WHERE (active = 1) AND (soc_network_id = 1)))")->queryOne();
        $arResult['friendsStatistics']['internet'] += $rsVkFriends['summ'];

        $rsOkFriends = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT friend_id) as summ FROM ok_users_friends WHERE (user_id IN (SELECT soc_network_user_id FROM internet_users WHERE (active = 1) AND (soc_network_id = 2)))")->queryOne();
        $arResult['friendsStatistics']['internet'] += $rsOkFriends['summ'];

        /*Сумма присутствующих друзей просматриваемых пользователей*/
        $rsVkFriends = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT friend_id) as summ FROM vk_users_friends WHERE (user_id IN (SELECT soc_network_user_id FROM internet_users WHERE (active = 1) AND (soc_network_id = 1))) AND (EXISTS (SELECT soc_network_user_id FROM internet_users WHERE (soc_network_id = 1) AND (internet_users.soc_network_user_id = vk_users_friends.friend_id) LIMIT 1))")->queryOne();
        $arResult['friendsStatistics']['database'] += $rsVkFriends['summ'];

        $rsOkFriends = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT friend_id) as summ FROM ok_users_friends WHERE (user_id IN (SELECT soc_network_user_id FROM internet_users WHERE (active = 1) AND (soc_network_id = 2))) AND (EXISTS (SELECT soc_network_user_id FROM internet_users WHERE (soc_network_id = 2) AND (internet_users.soc_network_user_id = ok_users_friends.friend_id) LIMIT 1))")->queryOne();
        $arResult['friendsStatistics']['database'] += $rsOkFriends['summ'];

        /*Вычисление процента для друзей*/
        $arResult['friendsStatistics']['procent'] = $arResult['friendsStatistics']['internet']?(round($arResult['friendsStatistics']['database']/$arResult['friendsStatistics']['internet'], 3))*100:0;

        /*Подписчики*/
        /*Сумма всех подписчиков просматриваемых пользователей*/
        $rsVkFollowers = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT follower_id) as summ FROM vk_users_followers WHERE (user_id IN (SELECT soc_network_user_id FROM internet_users WHERE (active = 1) AND (soc_network_id = 1)))")->queryOne();
        $arResult['followersStatistics']['internet'] += $rsVkFollowers['summ'];

        /*Сумма присутствующих подписчиков просматриваемых пользователей*/
        $rsVkFriends = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT follower_id) as summ FROM vk_users_followers WHERE (user_id IN (SELECT soc_network_user_id FROM internet_users WHERE (active = 1) AND (soc_network_id = 1))) AND (EXISTS (SELECT soc_network_user_id FROM internet_users WHERE (soc_network_id = 1) AND (internet_users.soc_network_user_id = vk_users_followers.follower_id) LIMIT 1))")->queryOne();
        $arResult['followersStatistics']['database'] += $rsVkFriends['summ'];

        /*Вычисление процента для друзей*/
        $arResult['followersStatistics']['procent'] = $arResult['followersStatistics']['internet']?(round($arResult['followersStatistics']['database']/$arResult['followersStatistics']['internet'], 3))*100:0;

        /*Посты*/
        /*Сумма всех постов в просматриваемых группах*/
        $rsVkPosts = Yii::$app->db->createCommand("SELECT SUM(posts) as summ FROM vk_groups WHERE watch = 1")->queryOne();
        $arResult['postsStatistics']['internet'] += $rsVkPosts['summ'];

        /*Сумма всех постов которые имеются в системе*/
        $rsVkPosts = Yii::$app->db->createCommand("SELECT COUNT(id) as summ FROM posts WHERE (soc_network_id = 1) AND (group_id IN (SELECT vk_id FROM vk_groups WHERE watch = 1)) AND (is_parent = 0)")->queryOne();
        $arResult['postsStatistics']['database'] += $rsVkPosts['summ'];

        /*Вычисление процента для постов*/
        $arResult['postsStatistics']['procent'] = $arResult['postsStatistics']['internet']?(round($arResult['postsStatistics']['database']/$arResult['postsStatistics']['internet'], 3))*100:0;

        /*Комментарии*/
        /*Сумма всех комментариев из загруженых постов в просматриваемых группах*/
        $rsVkComments = Yii::$app->db->createCommand("SELECT SUM(comments) as summ FROM posts WHERE (soc_network_id = 1) AND (group_id IN (SELECT vk_id FROM vk_groups WHERE watch = 1))")->queryOne();
        $arResult['commentsStatistics']['internet'] += $rsVkComments['summ'];

//        $rsOkComments = Yii::$app->db->createCommand("SELECT SUM(comments) as summ FROM posts WHERE (soc_network_id = 2) AND (group_id IN (SELECT ok_id FROM ok_groups WHERE watch = 1))")->queryOne();
//        $arResult['commentsStatistics']['internet'] += $rsOkComments['summ'];
//
//        $rsFbComments = Yii::$app->db->createCommand("SELECT SUM(comments) as summ FROM posts WHERE (soc_network_id = 3) AND (group_id IN (SELECT FB_GROUP_ID FROM fb_groups WHERE watch = 1))")->queryOne();
//        $arResult['commentsStatistics']['internet'] += $rsFbComments['summ'];

        /*Сумма всех комментариев из загруженых постов которые имеются в системе*/
        $rsVkComments = Yii::$app->db->createCommand("SELECT COUNT(post_comments.id) as summ FROM post_comments INNER JOIN posts ON (post_comments.owner_id = posts.owner_id AND post_comments.post_id = posts.post_id AND post_comments.soc_network_id = posts.soc_network_id) WHERE (post_comments.soc_network_id = 1) AND (posts.group_id IN (SELECT vk_id FROM vk_groups WHERE watch = 1))")->queryOne();
        $arResult['commentsStatistics']['database'] += $rsVkComments['summ'];

//        $rsOkComments = Yii::$app->db->createCommand("SELECT COUNT(post_comments.id) as summ FROM post_comments INNER JOIN posts ON (post_comments.owner_id = posts.owner_id AND post_comments.post_id = posts.post_id AND post_comments.soc_network_id = posts.soc_network_id) WHERE (post_comments.soc_network_id = 2) AND (posts.group_id IN (SELECT ok_id FROM ok_groups WHERE watch = 1))")->queryOne();
//        $arResult['commentsStatistics']['database'] += $rsOkComments['summ'];
//
//        $rsFbComments = Yii::$app->db->createCommand("SELECT COUNT(post_comments.id) as summ FROM post_comments INNER JOIN posts ON (post_comments.owner_id = posts.owner_id AND post_comments.post_id = posts.post_id AND post_comments.soc_network_id = posts.soc_network_id) WHERE (post_comments.soc_network_id = 3) AND (posts.group_id IN (SELECT FB_GROUP_ID FROM fb_groups WHERE watch = 1))")->queryOne();
//        $arResult['commentsStatistics']['database'] += $rsFbComments['summ'];

        /*Вычисление процента для постов*/
        $arResult['commentsStatistics']['procent'] = $arResult['commentsStatistics']['internet']?(round($arResult['commentsStatistics']['database']/$arResult['commentsStatistics']['internet'], 3))*100:0;

        /*Лайки*/
        /*Сумма всех лайков из загруженых постов в просматриваемых группах*/
        $rsVkComments = Yii::$app->db->createCommand("SELECT SUM(likes) as summ FROM posts WHERE (soc_network_id = 1) AND (group_id IN (SELECT vk_id FROM vk_groups WHERE watch = 1))")->queryOne();
        $arResult['likesStatistics']['internet'] += $rsVkComments['summ'];

//        $rsOkComments = Yii::$app->db->createCommand("SELECT SUM(likes) as summ FROM posts WHERE (soc_network_id = 2) AND (group_id IN (SELECT ok_id FROM ok_groups WHERE watch = 1))")->queryOne();
//        $arResult['likesStatistics']['internet'] += $rsOkComments['summ'];
//
//        $rsFbComments = Yii::$app->db->createCommand("SELECT SUM(likes) as summ FROM posts WHERE (soc_network_id = 3) AND (group_id IN (SELECT FB_GROUP_ID FROM fb_groups WHERE watch = 1))")->queryOne();
//        $arResult['likesStatistics']['internet'] += $rsFbComments['summ'];

        /*Сумма всех комментариев из загруженых постов которые имеются в системе*/
        $rsVkComments = Yii::$app->db->createCommand("SELECT COUNT(post_comment_likes.id) as summ FROM post_comment_likes INNER JOIN posts ON (post_comment_likes.owner_id = posts.owner_id AND post_comment_likes.liked_item_id = posts.post_id AND post_comment_likes.soc_network_id = posts.soc_network_id) WHERE (post_comment_likes.soc_network_id = 1) AND (posts.group_id IN (SELECT vk_id FROM vk_groups WHERE watch = 1))")->queryOne();
        $arResult['likesStatistics']['database'] += $rsVkComments['summ'];

//        $rsOkComments = Yii::$app->db->createCommand("SELECT COUNT(post_comment_likes.id) as summ FROM post_comment_likes INNER JOIN posts ON (post_comment_likes.owner_id = posts.owner_id AND post_comment_likes.liked_item_id = posts.post_id AND post_comment_likes.soc_network_id = posts.soc_network_id) WHERE (post_comment_likes.soc_network_id = 2) AND (posts.group_id IN (SELECT ok_id FROM ok_groups WHERE watch = 1))")->queryOne();
//        $arResult['likesStatistics']['database'] += $rsOkComments['summ'];
//
//        $rsFbComments = Yii::$app->db->createCommand("SELECT COUNT(post_comment_likes.id) as summ FROM post_comment_likes INNER JOIN posts ON (post_comment_likes.owner_id = posts.owner_id AND post_comment_likes.liked_item_id = posts.post_id AND post_comment_likes.soc_network_id = posts.soc_network_id) WHERE (post_comment_likes.soc_network_id = 3) AND (posts.group_id IN (SELECT FB_GROUP_ID FROM fb_groups WHERE watch = 1))")->queryOne();
//        $arResult['likesStatistics']['database'] += $rsFbComments['summ'];

        /*Вычисление процента для постов*/
        $arResult['likesStatistics']['procent'] = $arResult['likesStatistics']['internet']?(round($arResult['likesStatistics']['database']/$arResult['likesStatistics']['internet'], 3))*100:0;

        //print_r($arResult);

        if (!file_exists(__DIR__ . '/cronTempFiles/')) {
            mkdir(__DIR__ . '/cronTempFiles/');
        }

        $handle = @fopen(__DIR__ . '/cronTempFiles/cronGeneralStatistics.txt', "w+");
        if ($handle) {
            fwrite($handle, json_encode($arResult));
            fclose($handle);
        }

        $this->execTime('end', func_get_args());

        return ExitCode::OK;
    }

    public function actionGetNewInternetUsersGroupsFriendsFollowers()
    {
        exec("ps aux|grep 'scheduler/get-new-internet-users-groups-friends-followers'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);
/*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
/*END DEBUG*/

        $itemsPerPage = 100;
        $arUsers = $tempIU = [];

        $makeQuery = (new Query())->select([
                'tempIu.[[id]]',
                'iu.[[soc_network_id]]',
                'iu.[[soc_network_user_id]]'
            ])
            ->from(['tempIu' => TempNewInternetUsers::tableName()])
            ->innerJoin(['iu' => InternetUser::tableName()], 'tempIu.[[internet_user_id]] = iu.[[id]]')
            ->limit($itemsPerPage)
            ->all();

        if (is_array($makeQuery) && count($makeQuery)) {
            foreach ($makeQuery as $oneUser) {
                $arUsers[$oneUser['soc_network_id']][] = $oneUser['soc_network_user_id'];
                $tempIU[$oneUser['soc_network_id']][$oneUser['soc_network_user_id']] = $oneUser['id'];
            }
        }

/*DEBUG*/
        if ($debug) {
            echo "arUsers\n";
            print_r($arUsers);
            echo "tempIU\n";
            print_r($tempIU);
        }
/*END DEBUG*/

        if (count($arUsers)) {
            foreach ($arUsers as $socNetworkType => $socNetworkUsers) {
                try {
                    $successAddedIds = SocialFactory::factory($socNetworkType)->getNewInternetUsersGroupsFriendsFollowers($socNetworkUsers);
/*DEBUG*/
                    if ($debug) {
                        echo "successAddedIds\n";
                        print_r($successAddedIds);
                    }
/*END DEBUG*/
                    $needToDelete = [];
                    foreach ($successAddedIds as $oneUserId) {
                        if (isset($tempIU[$socNetworkType][$oneUserId])) {
                            $needToDelete[] = $tempIU[$socNetworkType][$oneUserId];
                        }
                    }
/*DEBUG*/
                    if ($debug) {
                        echo "needToDelete\n";
                        print_r($needToDelete);
                    }
/*END DEBUG*/
                    if (count($needToDelete)) {
                        TempNewInternetUsers::deleteAll(['id'=>$needToDelete]);
                    }
                } catch (\InvalidArgumentException $error) {
/*DEBUG*/
                    if ($debug) {
                        echo $error->getMessage()."\n";
                    }
/*END DEBUG*/
                }
            }
        }

        $this->execTime('end', $arArgs);

        return ExitCode::OK;
    }

}