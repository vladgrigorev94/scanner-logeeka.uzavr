<?php

namespace app\commands\workers;

use app\components\socialapi\SocialFactory;
use app\helpers\SocketIOEmitter;
use app\traits\Sleepable;
use yii\console\Controller;
use \app\models\search\PhotosSearch;
use \app\helpers\HelperFunctions;
use \app\models\data\Findface;
use \app\models\data\Photos;
use \app\models\data\PeopleApi;
use yii\console\ExitCode;
use \yii\db\Query;
use app\models\data\InternetUser;

class SocialController extends Controller
{
    use Sleepable, Streamable;

    const SET_INTERNET_USERS_IDS_TO_FF_MAX_PER_SOCIAL_NETWORK = 50;

    public function actionSetInternetUsersIdToFindface(int $stream_number, int $streams_count)
    {
        self::$sleepLimit = 10;

        $filter_ids = $this->getIdsFilterForStream($stream_number, $streams_count);

        while (true) {
            $start_time = microtime(true);
            $processed_findfaces = 0;
            $arUsers = $arUsersIds = [];

            $makeQuery = (new Query())->select(['id', 'user_id', 'soc_network'])
                ->from(Findface::tableName())
                ->where([
                    'internet_users_id' => NULL,
                    'soc_network' => InternetUser::SOC_NETWORK_SHORT_TITLES[InternetUser::SOC_NETWORK_ID_VK]
                ])
                ->andWhere(['in', 'right(id, 1)', $filter_ids])
                ->limit(self::SET_INTERNET_USERS_IDS_TO_FF_MAX_PER_SOCIAL_NETWORK)->orderBy(['id' => SORT_DESC])
                ->union(
                    (new Query())->select(['id', 'user_id', 'soc_network'])
                        ->from(Findface::tableName())
                        ->where([
                            'internet_users_id' => NULL,
                            'soc_network' => InternetUser::SOC_NETWORK_SHORT_TITLES[InternetUser::SOC_NETWORK_ID_OK]
                        ])
                        ->andWhere(['in', 'right(id, 1)', $filter_ids])
                        ->limit(self::SET_INTERNET_USERS_IDS_TO_FF_MAX_PER_SOCIAL_NETWORK)->orderBy(['id' => SORT_DESC])
                )->union(
                    (new Query())->select(['id', 'user_id', 'soc_network'])
                        ->from(Findface::tableName())
                        ->where([
                            'internet_users_id' => NULL,
                            'soc_network' => InternetUser::SOC_NETWORK_SHORT_TITLES[InternetUser::SOC_NETWORK_ID_FB]
                        ])
                        ->andWhere(['in', 'right(id, 1)', $filter_ids])
                        ->limit(self::SET_INTERNET_USERS_IDS_TO_FF_MAX_PER_SOCIAL_NETWORK)->orderBy(['id' => SORT_DESC])
                )->union(
                    (new Query())->select(['id', 'user_id', 'soc_network'])
                        ->from(Findface::tableName())
                        ->where([
                            'internet_users_id' => NULL,
                            'soc_network' => InternetUser::SOC_NETWORK_SHORT_TITLES[InternetUser::SOC_NETWORK_ID_NS]
                        ])
                        ->andWhere(['in', 'right(id, 1)', $filter_ids])
                        ->limit(self::SET_INTERNET_USERS_IDS_TO_FF_MAX_PER_SOCIAL_NETWORK)->orderBy(['id' => SORT_DESC])
                )->union(
                    (new Query())->select(['id', 'user_id', 'soc_network'])
                        ->from(Findface::tableName())
                        ->where([
                            'internet_users_id' => NULL,
                            'soc_network' => InternetUser::SOC_NETWORK_SHORT_TITLES[InternetUser::SOC_NETWORK_ID_INSTA]
                        ])
                        ->andWhere(['in', 'right(id, 1)', $filter_ids])
                        ->limit(self::SET_INTERNET_USERS_IDS_TO_FF_MAX_PER_SOCIAL_NETWORK)->orderBy(['id' => SORT_DESC])
                )
                ->all();

            if (is_array($makeQuery) && count($makeQuery)) {
                foreach ($makeQuery as $oneUser) {
                    $arUsers[$oneUser['soc_network']][] = $oneUser['user_id'];
                    $arUsersIds[$oneUser['soc_network']][] = $oneUser['id'];
                }
            }

            if (count($arUsers)) {
                $this->resetAccumulator();

                foreach ($arUsers as $socNetworkType => $socNetworkUsers) {
                    try {
                        $toFindFace = SocialFactory::factory($socNetworkType)->checkAndCopyInternetUsersIdsToFindface(array_unique($socNetworkUsers));
                        foreach ($toFindFace as $oneUserId => $toFindfaceId) {
                            if (!isset($toFindfaceId)) $toFindfaceId = 0;
                            \Yii::$app->db->createCommand("UPDATE findface SET internet_users_id = {$toFindfaceId} WHERE (soc_network = '{$socNetworkType}') AND (user_id = {$oneUserId}) AND ((internet_users_id IS NULL) OR (internet_users_id = 0))")->execute();
                            $processed_findfaces++;
                        }
                    } catch (\Exception $error) {
                        echo $error->getMessage(). PHP_EOL;
                    }
                }

                $this->stdout(date('d.m.Y H:i:s') . ' Время выполнения итерации: '.round(microtime(true) - $start_time, 4).' сек. Обработано - ' . $processed_findfaces  . PHP_EOL);
            } else {
                $this->sleep();
            }

        }
    }

}
