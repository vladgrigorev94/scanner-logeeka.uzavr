<?php

namespace app\commands\workers;

trait Streamable
{
    /**
     * @var int Максимальное колличество потоков
     */
    public static $max_streams_limit = 10;

    /*
     * var int $numer_of_stream (from 0 to $streams_count-1)
     *
     */
    public function getIdsFilterForStream(int $numer_of_stream, int $streams_count)
    {
        if ($numer_of_stream >= 0 && $numer_of_stream < $streams_count && $streams_count <= self::$max_streams_limit) {
            for ($i=0; $i<10; $i++) {
                $chunkering_array[] = str_pad ($i, 1, "0", STR_PAD_LEFT);
            }

            $chunked_array = array_chunk($chunkering_array, ceil(count($chunkering_array)/$streams_count));

            return $chunked_array[$numer_of_stream];
        } else {
            throw new \Exception("Smth went wrong with number of stream or stream count");
        }
    }
}