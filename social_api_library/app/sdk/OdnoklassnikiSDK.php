<?php

namespace SocialApiLibrary\sdk;

use GuzzleHttp\Client;

class OdnoklassnikiSDK
{
    public static $id = "";
    private static $app_id = "";
    private static $app_public_key = "";
    private static $app_secret_key = "";
    private static $API_REQUSET_ADDRESS = "http://api.odnoklassniki.ru/fb.do";
    private static $access_token;
    private static $refresh_token;
    private static $isInternalSetting = false;
    private $client;

    public function __construct($options = [])
    {
        if (!self::$access_token) {
            self::$id = $options['id'];
            self::$app_id = $options['app_id'];
            self::$app_public_key = $options['app_public_key'];
            self::$app_secret_key = $options['app_secret_key'];
            self::$access_token = $options['access_token'];
            self::$refresh_token = $options['refresh_token'];
        }

        $this->client = new Client();
    }

    public static function getCode()
    {
        return $_GET["code"] ?? null;
    }

    public function setIsInternalSetting($value)
    {
        self::$isInternalSetting = $value;
    }

    public function makeRequest($methodName, array $parameters = [])
    {
        if (!self::$isInternalSetting) {
            $parameters['access_token'] = self::$access_token;
        }
        $parameters['application_key'] = self::$app_public_key;
        $parameters['method'] = $methodName;
        $parameters['sig'] = self::calcSignature($methodName, $parameters, self::$access_token);

        $response = $this->client->post(self::$API_REQUSET_ADDRESS, [
            'form_params' => $parameters
        ]);
        $response = json_decode($response->getBody()->getContents(), true);
        if (!empty($response['error_code'])) {
            throw new \Exception($response['error_msg'], $response['error_code']);
        }

        return $response;
    }

    private static function calcSignature($methodName, $parameters = null, $access_token = '')
    {
        if (is_null(self::$app_id) ||
            is_null(self::$app_public_key) ||
            is_null(self::$app_secret_key) ||
            is_null($access_token) ||
            !(is_null($parameters) ||
                is_array($parameters))
        ) {
            return null;
        }
        if (!is_null($parameters)) {
            if (!self::isAssoc($parameters)) {
                return null;
            }
        } else {
            $parameters = array();
        }
        $parameters["application_key"] = self::$app_public_key;
        $parameters["method"] = $methodName;
        if (!ksort($parameters)) {
            return null;
        } else {
            $requestStr = "";
            foreach ($parameters as $key => $value) {
                $requestStr .= $key . "=" . $value;
            }
            if (self::$isInternalSetting)
                return md5($requestStr . self::$app_secret_key);
            $requestStr .= md5($access_token . self::$app_secret_key);
            return md5($requestStr);
        }
    }

    private static function isAssoc($arr)
    {
        return array_keys($arr) !== range(0, count($arr) - 1);
    }
}
