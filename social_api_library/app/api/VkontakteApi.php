<?php

namespace SocialApiLibrary\api;

use VK\Client\VKApiClient;

class VkontakteApi extends SocialApi
{
    const TOO_MANY_REQUESTS_CODE = 6;

    private $vk;
    protected $offset;

    public function __construct($options)
    {
        parent::__construct($options);
        $this->vk = new VKApiClient();
    }

    public function getGroupUsers(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $params['extended'] = 1;

            $res = $this->vk->groups()->getMembers(
                $this->options['access_token'],
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getGroupById(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $fields = ['description', 'members_count'];
            $params['fields'] = $this->createFields($params, $fields);
            $res = $this->vk->groups()->getById(
                $this->options['access_token'],
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getUserInfo(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);

            $fields = ['bdate', 'counters', 'city', 'photo_max_orig'];
            $params['fields'] = $this->createFields($params, $fields);

            $params['user_ids'] = implode(',', $params['user_ids']);
            $res = $this->vk->users()->get(
                $this->options['access_token'],
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    private function createFields($params, $fields)
    {
        $params['fields'] = array_merge($params['fields'] ?? [], $fields);

        return implode(',', $params['fields']);
    }

    public function getUserGroups(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $res = $this->vk->groups()->get(
                $this->options['access_token'],
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getUserFriends(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $res = $this->vk->friends()->get(
                $this->options['access_token'],
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getUserFollowers(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $res = $this->vk->users()->getFollowers(
                $this->options['access_token'],
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getGroupPosts(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $res = $this->vk->wall()->get(
                $this->options['access_token'],
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getGroupPostsById(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $res = $this->vk->wall()->getById(
                $this->options['access_token'],
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getPostLikes(array $params = [], $type = self::LIKES_TYPE_POST): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $params['type'] = $type;
            $params['extended'] = 1;
            $res = $this->vk->likes()->getList(
                $this->options['access_token'],
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getPostComments(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $params['extended'] = 1;
            $params['need_likes'] = 1;
            $res = $this->vk->wall()->getComments(
                $this->options['access_token'],
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getPostAttachments(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $params['posts'] = $params['owner_id'] . '_' . $params['post_id'];
            $res = $this->vk->wall()->getById(
                $this->options['access_token'],
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getAlbums(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $res = $this->vk->photos()->getAlbums(
                $this->options['access_token'],
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getPhotos(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $res = $this->vk->photos()->get(
                $this->options['access_token'],
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getVideo(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $params['extended'] = 1;
            $res = $this->vk->video()->get(
                $this->options['access_token'],
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function setOffsetAndCount($offset, $count)
    {
        if (!empty($context->count)) {
            $this->count = $context->count;
        }
        if (!empty($context->offset)) {
            $this->offset = $context->offset;
        }
    }

    public function addOffsetAndCountToSendData($params)
    {
        if (!empty($this->count)) {
            $params['count'] = $this->count;
        }
        if (!empty($this->offset)) {
            $params['offset'] = $this->offset;
        }

        return $params;
    }

    public function response($data): ?array
    {
        return [
            'success' => true,
            'result' => $data,
            'code' => 200,
            'offset' => ($this->options['offset'] ?? 0) + count($data['items'] ?? []),
        ];
    }
}
