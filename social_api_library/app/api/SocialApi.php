<?php

namespace SocialApiLibrary\api;

abstract class SocialApi
{
    const ID_VK = 1;
    const ID_OK = 2;

    const LIKES_TYPE_POST = 'post';
    const LIKES_TYPE_COMMENT = 'comment';

    protected $options;

    protected $count;

    public function __construct($options = [])
    {
        $this->options = $options;
    }

    abstract public function setOffsetAndCount($offset, $count);

    abstract public function addOffsetAndCountToSendData($params);

    abstract public function getGroupUsers(array $params = []): ?array;

    abstract public function getGroupById(array $params = []): ?array;

    abstract public function getUserInfo(array $params = []): ?array;

    abstract public function getUserGroups(array $params = []): ?array;

    abstract public function getUserFriends(array $params = []): ?array;

    abstract public function getUserFollowers(array $params = []): ?array;

    abstract public function getGroupPosts(array $params = []): ?array;

    abstract public function getGroupPostsById(array $params = []): ?array;

    abstract public function getPostLikes(array $params = [], $type = self::LIKES_TYPE_POST): ?array;

    abstract public function getPostComments(array $params = []): ?array;

    abstract public function getPostAttachments(array $params = []): ?array;

    abstract public function getAlbums(array $params = []): ?array;

    abstract public function getPhotos(array $params = []): ?array;

    abstract public function getVideo(array $params = []): ?array;

    abstract public function response($data): ?array;

    protected function error($text, $code = 500): ?array
    {
        return [
            'success' => false,
            'code' => $code,
            'message' => $text,
        ];
    }
}
