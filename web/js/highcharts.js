'use strict';

$(document).ready(function () {
    Highcharts.setOptions({
        chart: {
            spacingTop: 20,
            spacingLeft: 10,
            spacingRight: 10,
            style: {
                fontFamily: "'Gotham Pro', Arial, Helvetica, sans- serif",
            }
        },
        title: {
            align: 'left',
            x: 0,
            y: 0,
            style: {
                fontWeight: 'bold',
            }
        },
        yAxis: {
            title: { text: null },
        },
        legend: { enabled: false, },
    });


    $.ajax({
        url: '/api/default/get-info',
        type: 'GET',
        success: function (data) {
            var allModels = data,
                eventGraphValue = [],
                eventTitle = [],
                eventPhotosValue = [],
                eventWhiteCntValue = [],
                eventBlackValue = [],
                eventSocValue = [],
                galleriesGraphTitle = [],
                galleriesGraphValue = [],
                galleriesTitle = [],
                galleriesValue = [],
                groupTitles = [],
                groupValues = [],
                ageGraphTitles = [],
                ageGraphValues = [],
                genderMGraphTitles = [],
                genderMGraphValues = [],
                genderFGraphTitles = [],
                genderFGraphValues = [];

            $(allModels.eventsProvider).each(function(index, value){
                var date = new Date(value.date*1000);
                var photosCount = Number(value.photos_count);
                var whiteCount = Number(value.white_count);
                var blackCount = Number(value.black_count);
                var galleryCntPeople = Number(value.gallery_cnt_people);

                console.log(value);
                eventTitle.unshift(('0' + date.getDate()).slice(-2)+'.'+('0' + (date.getMonth()+1)).slice(-2));
                eventPhotosValue.unshift(photosCount);
                eventWhiteCntValue.unshift(whiteCount);
                eventBlackValue.unshift(blackCount);
                eventGraphValue.unshift(galleryCntPeople);
            });

            $('#graph1').highcharts({
                chart: {
                    type: 'column'
                },
                title: { text: 'Мероприятия' },
                xAxis: {
                    categories: eventTitle
                },
                series: [
                    {
                        name: 'Распознано объектов',
                        data: eventPhotosValue
                    },
                    {
                        name: whiteCntTitle,
                        data: eventWhiteCntValue,
                        color: '#81C784'
                    },
                    {
                        name: blackCntTitle,
                        data: eventBlackValue,
                        color: '#c78b43'
                    },
                    {
                        name: eventGraphTitle,
                        data: eventGraphValue,
                        color: '#c75255'
                    }
                ],
                legend: { enabled: true, }
            });

            /*$(allModels.eventsGraphBlock).each(function(index, value){
                var date = new Date(value.date*1000);
                eventGraphTitle.unshift(('0' + date.getDate()).slice(-2)+'.'+('0' + (date.getMonth()+1)).slice(-2));
                eventGraphValue.unshift(Number(value.photos_count));
            });
            $('#graph2').highcharts({
                chart: {
                    type: 'column'
                },
                title: { text: 'Объекты на мероприятиях' },
                xAxis: {
                    categories: eventGraphTitle
                },
                series: [{
                    name: 'Объекты на мероприятиях',
                    data: eventGraphValue
                }]
            });*/

            $(allModels.groupProvider).each(function(index, value){
                groupTitles.push(value.title);
                groupValues.push(Number(value.members));
            });
            $('#graph3').highcharts({
                chart: {
                    type: 'bar'
                },
                title: { text: 'Группы' },
                xAxis: {
                    categories: groupTitles
                },
                series: [{
                    name: 'интернет-пользователей',
                    data: groupValues
                }]
            });

            $(allModels.eventsProvider).each(function(index, value){
                eventSocValue.unshift(Number(value.social_count));
            });
            $('#graph5').highcharts({
                chart: {
                    type: 'column'
                },
                title: { text: 'Интернет-пользователи' },
                xAxis: {
                    categories: eventTitle
                },
                series: [{
                    name: 'Интернет-пользователи',
                    data: eventSocValue
                }],
            });

            $(allModels.galleriesProvider).each(function(index, value){
                var date = new Date(value.date*1000);
                galleriesTitle.unshift(('0' + date.getDate()).slice(-2)+'.'+('0' + (date.getMonth()+1)).slice(-2));
                galleriesValue.unshift(Number(value.galleries_count));
            });
            $('#graph6').highcharts({
                chart: {
                    type: 'column'
                },
                title: { text: 'Источники видеоматериалов' },
                xAxis: {
                    categories: galleriesTitle
                },
                series: [{
                    name: 'Источники видеоматериалов',
                    data: galleriesValue
                }]
            });
            for (var prop in allModels.ageGraphBlock['GENDER_M']) {
                genderMGraphValues.push(Number(allModels.ageGraphBlock['GENDER_M'][prop]));
            }
            for (var prop in allModels.ageGraphBlock['GENDER_F']) {
                genderFGraphValues.push(Number(allModels.ageGraphBlock['GENDER_F'][prop]));
            }
            $('#graph7').highcharts({
                chart: {
                    type: 'column'
                },
                title: { text: 'Распределение возраста' },
                xAxis: {
                    categories: allModels.ageGraphParams
                },
                series: [
                    {
                        name: 'Количество мужчин',
                        data: genderMGraphValues,
                        color: '#81C784'
                    },
                    {
                        name: 'Количество женщин',
                        data: genderFGraphValues,
                        color: '#d44'
                    }
                ],
                legend: { enabled: true, },
                yAxis: {
                    labels: {
                        format: '{value}%'
                    }
                },
                tooltip: {
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}%</b><br/>'
                },
            });
        }
    });

    /*$('#graph4').highcharts({
      chart: {
        type: 'pie'
      },
      title: { text: 'Группы' },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false
          },
          showInLegend: true,
        }
      },
      series: [{
        name: 'Интересы',
        data: [{
            name: 'Личностный рост',
          y: 11.84
        }, {
            name: 'Любовь',
          y: 10.85
        }, {
            name: 'Карьера',
          y: 4.67
        }, {
            name: 'Детское воспитание',
          y: 4.18
        }, {
            name: 'Кризис',
          y: 1.64
        }, {
            name: 'Бизнес',
          y: 1.6
        }]
      }],
      legend: {
        enabled: true,
        align: 'left',
        verticalAlign: 'middle',
        layout: 'vertical',
        itemMarginBottom: 26,
      },
    });*/
});