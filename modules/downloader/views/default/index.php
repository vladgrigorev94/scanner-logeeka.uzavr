<?php

/* @var $this View */
/* @var $model Downloader */
/* @var $dataProvider ActiveDataProvider */
/* @var $searchModel Downloader */

use app\modules\downloader\models\data\Downloader;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = 'Загрузчик';
?>

<div class="site-index">
    <div class="body-content">
        <h3><?= $this->title ?></h3>
        <?php $form = ActiveForm::begin([
            'method' => 'POST',
        ]) ?>
            <?= $form->field($model, 'from')->textInput() ?>
            <?= $form->field($model, 'to')->textInput() ?>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end() ?>
    </div>
    <div class="clearfix"></div>

    <div class="jumbotron">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'headerOptions' => ['style' => 'width:5%']
                ],
                [
                    'attribute' => 'from',
                    'headerOptions' => ['style' => 'width:500px']
                ],
                'to',
                [
                    'attribute' => 'addTime',
                    'content' => function ($model) {
                        return date('d.m.Y H:i:s', $model->addTime);
                    }
                ],
                [
                    'attribute' => 'startTime',
                    'content' => function ($model) {
                        return !empty($model->startTime)
                            ? date('d.m.Y H:i:s', $model->startTime)
                            : null;
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}'
                ]
            ]
        ]) ?>
    </div>
</div>
