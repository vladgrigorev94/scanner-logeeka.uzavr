<?php

namespace app\modules\downloader\models\search;

use app\modules\downloader\models\data\Downloader;
use yii\data\ActiveDataProvider;

class DownloaderSearch extends Downloader
{
    public function search()
    {
        $query = Downloader::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $dataProvider;
    }
}