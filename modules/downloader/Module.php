<?php

namespace app\modules\downloader;

use Yii;
use yii\base\Module AS ParentModule;
use yii\console\Application;

class Module extends ParentModule
{
    public $controllerNamespace = 'app\modules\downloader\controllers';

    public function init()
    {
        if (Yii::$app instanceof Application) {
            $this->controllerNamespace = 'app\modules\downloader\commands';
        }

        parent::init(); // TODO: Change the autogenerated stub

        Yii::configure($this, require __DIR__ . '/config.php');

        Yii::setAlias("@vendor/downloader", __DIR__);
    }
}