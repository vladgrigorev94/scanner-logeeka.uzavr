<?php

namespace app\models\data;

use Yii;

/**
 * This is the model class for table "ok_groups".
 *
 * @property int $id
 * @property int $ok_id
 * @property string $title
 * @property int $members
 * @property int $date
 * @property int $watch
 */
class OkGroups extends \yii\db\ActiveRecord
{
    const WATCH = 1;
    const NO_WATCH = 0;
    const SOC_FIELD = 'ok_id';
    const SOC_NAME_FIELD = 'title';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ok_groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ok_id', 'members', 'date', 'watch'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['ok_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ok_id' => 'Ok ID',
            'title' => 'Title',
            'members' => 'Members',
            'date' => 'Date',
            'watch' => 'Watch',
        ];
    }

    public function getInternetUsers(){
        return $this->hasMany(InternetUser::class, ['soc_network_user_id' => 'ok_users_id'])->viaTable(OkUsersGroups::tableName(), ['ok_groups_id'=>'ok_id']);
    }
}
