<?php

namespace app\models\data;

use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "{{%vk_users_groups}}".
 *
 * @property int $id
 * @property int $vk_groups_id
 * @property int $vk_users_id
 *
 * @property VkGroups $vkGroups
 */
class VkUsersGroups extends ActiveRecord
{
    /**
     * Наименование поля, содержащего id юзера соц.сети
     */
    const USER_FIELD = 'vk_users_id';

    /**
     * Наименование поля, содержащего id группы соц.сети
     */
    const GROUP_FIELD = 'vk_groups_id';

    /**
     * Наименование связи в модели Findface
     */
    const FINDFACE_LINK_NAME = 'vkUsersGroups';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%vk_users_groups}}';
    }

    /**
     * Get common information by vk users and there groups
     * not using ActiveRecord
     *
     * @param array $arUsers
     * @param array $arGroupsData
     * @return array
     */
    public static function getRelationByUsersAndGroups(array $arUsers, array $arGroupsData)
    {
        return (new Query())->select(['`vk_users_id`, `vk_groups_id`'])
            ->from(VkUsersGroups::tableName())
            ->where([
                    'vk_groups_id' => array_keys($arGroupsData['VK_GROUPS_DATA']),
                    'vk_users_id' => $arUsers]
            )->all();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vk_groups_id', 'vk_users_id'], 'integer'],
            [
                ['vk_groups_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => VkGroups::class,
                'targetAttribute' => ['vk_groups_id' => 'id']
            ],
            /*[
                ['vk_users_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => VkUsers::class,
                'targetAttribute' => ['vk_users_id' => 'id']
            ],*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'vk_groups_id' => 'Vk Groups ID',
            'vk_users_id'  => 'Vk Users ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVkGroups()
    {
        return $this->hasOne(VkGroups::class, ['id' => 'vk_groups_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVkUsers()
    {
        return $this->hasOne(InternetUser::class, ['soc_network_user_id' => 'vk_users_id'])
            ->andOnCondition(['soc_network_id' => 1]);
    }
}
