<?php

namespace app\models\data;

use yii\helpers\ArrayHelper;

class SocialActivityStatistic extends AbstractStatistic
{
    public $groupDetails;

    public $allCountsGroups;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'groupDetails' => 'Детали по группам',
        ]);
    }

    /**
     * @param $media
     * @return array
     */
    private function getFormattedMedias($media)
    {
        $dates = $this->getFormattedGraphDates();

        $mediaDates = ArrayHelper::getColumn($media, 'date');

        foreach ($dates as $date) {
            if (!in_array($date, $mediaDates, true)) {
                $mediaDates[] = $date;
                $media[] = [
                    'date' => $date,
                    'count' => 0,
                ];
            }
        }

        ArrayHelper::multisort($media, 'date', SORT_ASC, SORT_NATURAL);

        return $media;
    }

    /**
     * @return array
     */
    public function getFormattedLikes()
    {
        return $this->getFormattedMedias($this->likes);
    }

    /**
     * @return array
     */
    public function getFormattedComments()
    {
        return $this->getFormattedMedias($this->comments);
    }

    /**
     * @return array
     */
    public function getFormattedPosts()
    {
        return $this->getFormattedMedias($this->posts);
    }

    /**
     * @return array
     */
    public function getFormattedGraphDates()
    {
        $dates = array_unique(ArrayHelper::merge(
            ArrayHelper::getColumn($this->likes ?? [], 'date'),
            ArrayHelper::getColumn($this->comments ?? [], 'date'),
            ArrayHelper::getColumn($this->posts ?? [], 'date')
        ));
        sort($dates, SORT_NATURAL);
        return $dates;
    }
}
