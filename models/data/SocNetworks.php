<?php

namespace app\models\data;

use Yii;

/**
 * This is the model class for table "soc_networks".
 *
 * @property int $id
 * @property string $name
 *
 * @property InternetUser[] $internetUsers
 */
class SocNetworks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'soc_networks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app\soc-networks', 'ID'),
            'name' => Yii::t('app\soc-networks', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInternetUsers()
    {
        return $this->hasMany(InternetUser::className(), ['soc_network_id' => 'id']);
    }
}
