<?php

namespace app\models\data;

use app\components\validators\UserPasswordValidator;
use yii\base\Event;
use yii\base\Exception;
use yii\db\Exception as DBException;
use yii\web\User as WebUser;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use Yii;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $auth_key
 * @property string $access_token
 * @property int $rules_group_id
 * @property RulesGroup $group
 * @property array $rules
 * @property string $created_date
 * @property string $updated_date
 * @property int $password_expire
 * @property int $last_visit
 * @property int $updated_user
 * @property int $active
 * @property int $deleted
 * @property int $screenshot_help_status
 */
class User extends ActiveRecord implements IdentityInterface
{
    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    const NOT_DELETE = 0;
    const DELETE = 1;

    const SCREENSHOT_HELP_ACTIVE = 0;

    const SCREENSHOT_HELP_INACTIVE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritDoc}
     */
    public function __construct($config = [])
    {
        parent::__construct($config);

        Event::on(WebUser::class,WebUser::EVENT_AFTER_LOGIN, [$this, 'setPasswordExpireNotification']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['login', 'rules_group_id'],
                'required',
                'message' => Yii::t('app/users-manager', 'Field cannot be blank')
            ],
            [['password'], 'validateNewPassword'],
            [['password'], UserPasswordValidator::class, 'skipOnEmpty' => false],
            [
                ['password_expire'],
                'filter',
                'filter' => function ($value) {
                    return (
                        $this->isNewRecord
                        || (!empty($this->getDirtyAttributes()['password']) && !empty($this->password))
                    )
                        ? Settings::calculatePasswordExpire()
                        : $value;
                }
            ],
            [
                ['login'],
                'unique',
                'message' => Yii::t('app/users-manager', 'This login is already registered')
            ],
            ['email', 'email'],
            [
                [
                    'rules_group_id',
                    'updated_user',
                    'active',
                    'deleted',
                    'password_expire',
                    'last_visit',
                    'screenshot_help_status',
                ],
                'integer'
            ],
            [['created_date', 'updated_date'], 'safe'],
            [
                ['login', 'password', 'first_name', 'last_name', 'email', 'auth_key', 'access_token'],
                'string',
                'max' => 255
            ],
            [
                ['rules_group_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => RulesGroup::class,
                'targetAttribute' => ['rules_group_id' => 'id'],
                'message'         => Yii::t('app/users-manager', 'Invalid group')
            ],
            ['email', 'default', 'value' => 'test@email.ru'],
            [['login'], 'trim'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('app/users-manager', 'ID'),
            'login'          => Yii::t('app/users-manager', 'Login'),
            'password'       => Yii::t('app/users-manager', 'Password'),
            'first_name'     => Yii::t('app/users-manager', 'First name'),
            'last_name'      => Yii::t('app/users-manager', 'Last name'),
            'email'          => Yii::t('app/users-manager', 'Email'),
            'auth_key'       => Yii::t('app/users-manager', 'Auth Key'),
            'access_token'   => Yii::t('app/users-manager', 'Access Token'),
            'rules_group_id' => Yii::t('app/users-manager', 'Group'),
            'created_date'   => Yii::t('app/users-manager', 'Created Date'),
            'updated_date'   => Yii::t('app/users-manager', 'Updated Date'),
            'updated_user'   => Yii::t('app/users-manager', 'Updated User'),
            'password_expire'       => Yii::t('app/users-manager', 'Password expire'),
            'last_visit'      => Yii::t('app/users-manager', 'Last visit'),
            'active'         => Yii::t('app/users-manager', 'Active'),
            'deleted'        => Yii::t('app/users-manager', 'Deleted'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return User::findOne(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return User::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return User::findOne(['login' => $username]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return User::isIdentity() ? Yii::$app->user->identity->id : $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @return ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(RulesGroup::class, ['id' => 'rules_group_id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function checkRules(string $ruleName)
    {
        $result = false;

        if (User::isIdentity()) {
            foreach (Yii::$app->user->identity->group->rules as $rule) {
                switch ($rule->name) {
                    case $ruleName:
                        $result = true;

                        break;
                    default:
                        break;
                }
            }
        }

        return $result;
    }

    /**
     * @param $group_id
     * @return bool
     */
    public static function checkGroup($group_id)
    {
        return User::isIdentity() ? Yii::$app->user->identity->rules_group_id === $group_id : false;
    }

    /**
     * @return bool
     */
    public static function isIdentity()
    {
        return is_object(Yii::$app->user->identity);
    }

    /**
     * Checking user group to admin rights
     *
     * @return mixed
     */
    public static function isAdmin()
    {
        return self::checkGroup(RulesGroup::GROUP_ADMINISTRATORS);
    }

    /**
     * @return bool
     */
    public static function isModerator()
    {
        $result = false;

        if (
            self::checkRules(Rules::RULE_OBJECTS_MODERATION) ||
            self::checkRules(Rules::RULE_OBJECTS_MODERATION_GR_1) ||
            self::checkRules(Rules::RULE_OBJECTS_MODERATION_GR_2) ||
            self::checkRules(Rules::RULE_OBJECTS_MODERATION_GR_3) ||
            self::checkRules(Rules::RULE_OBJECTS_MODERATION_GR_4) ||
            self::checkRules(Rules::RULE_OBJECTS_MODERATION_GR_5) ||
            self::checkRules(Rules::RULE_OBJECTS_MODERATION_GR_6) ||
            self::checkRules(Rules::RULE_OBJECTS_MODERATION_GR_7)

        ) {
            $result = true;
        }

        return $result;
    }

    /**
     * @return bool
     */
    public static function isMainModerator()
    {
        return self::checkRules(Rules::RULE_OBJECTS_MODERATION);
    }

    /**
     * @return bool
     */
    public static function canEnterAdminZone()
    {
        return self::checkRules(Rules::RULE_CABINET_ACCESS);
    }

    /**
     * @return bool
     */
    public static function hasJoinStatisticsRights()
    {
        $result = false;

        if (self::isAdmin() || self::isMainModerator() || User::checkRules(Rules::RULE_ALIGNMENT_PEOPLE_TOGETHER_MODERATION)) {
            $result = true;
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->getSecurity()->generateRandomString(32);

        return $this->auth_key;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @param bool $old
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password, $old = false)
    {
        return Yii::$app->getSecurity()
            ->validatePassword(
                $password,
                $old ? $this->getOldAttribute('password') : $this->password
            );
    }

    /**
     * Валидация нового пароля.
     *
     * @param $attribute
     * @param $params
     */
    public function validateNewPassword($attribute, $params)
    {
        if ($this->isNewRecord || (!empty($this->getDirtyAttributes()['password']) && !empty($this->password))) {
            if (!$this->isPasswordMatchRegex($this->password)) {
                $this->addError(
                    $attribute,
                    'Пароль должен сожержать не менее 8 символов,'
                        . ' среди которых не менее 1-й цифры, 1-й латинской буквы и 1-го не буквенно-цифрового символа'
                );
            }
        }
    }

    /**
     * Проверка незахешированного пароля на соответствие паттерну
     *
     * @param $password
     * @return bool
     */
    public function isPasswordMatchRegex($password)
    {
        return (bool)preg_match(
            '/^(?=.{8,})(?=.*[\d])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9])/s',
            $password
        );
    }

    /**
     * @param string $password
     * @throws Exception
     */
    public function setPassword(string $password)
    {
        if ($this->isNewRecord) {
            $this->password = Yii::$app->getSecurity()->generatePasswordHash($password);
        } else {
            $this->password = $password;
        }
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Истек ли срок действия текущего пароля
     *
     * @return bool
     */
    public function isPasswordExpired()
    {
        return $this->password_expire <= time();
    }

    /**
     * @return bool
     */
    public function isAllowedAuth()
    {
        return !$this->isPasswordExpired()
            && $this->active === 1 &&  $this->deleted === 0;
    }

    /**
     * Создание уведомления, если до конца срока действия пароля осталось меньше 7-ми дней
     */
    public function setPasswordExpireNotification($event)
    {
        if ($this->checkExpirePasswordNotification()) {
            Yii::$app->session->setFlash(
                'password_expires',
                str_replace(
                    '{relativeTime}',
                    Yii::$app->formatter->asRelativeTime($this->password_expire),
                    Yii::t('app/users-manager', 'Password expires with time')
                )
            );
        }

        $event->handled = true;
    }

    /**
     * Определение
     *
     * @return bool
     */
    public function checkExpirePasswordNotification()
    {
        return $this->password_expire < Settings::calculatePasswordExpireNotice();
    }

    /**
     * @param $id
     * @return bool
     */
    public static function setLastVisit($id)
    {
        try {
            $result = (bool)Yii::$app->db->createCommand()
                ->update(self::tableName(), ['last_visit' => time()], ['id' => $id])
                ->execute();
        } catch (DBException $e) {
            $result = false;
        }

        return $result;
    }

    /**
     * @param int|int[] $id
     * @param int $activeStatus
     * @return bool
     */
    public static function setActive($id, $activeStatus)
    {
        try {
            $result = (bool)Yii::$app->db->createCommand()
                ->update(self::tableName(), ['active' => $activeStatus], ['id' => $id])
                ->execute();
        } catch (DBException $e) {
            $result = false;
        }

        return $result;
    }

    /**
     * @return bool
     */
    public static function checkVisibleScreenshotHelp()
    {
        $result = false;

        if (($user = Yii::$app->user->identity) && (int)$user->screenshot_help_status === self::SCREENSHOT_HELP_ACTIVE) {
            self::setScreenshotHelpStatus($user->id, self::SCREENSHOT_HELP_INACTIVE);
            $result = true;
        }

        return $result;
    }

    /**
     * @param int|int[] $id
     * @param int $status
     * @return bool
     */
    public static function setScreenshotHelpStatus($id, $status)
    {
        try {
            $result = (bool)Yii::$app->db->createCommand()
                ->update(self::tableName(), ['screenshot_help_status' => $status], ['id' => $id])
                ->execute();
        } catch (DBException $e) {
            $result = false;
        }

        return $result;
    }
}
