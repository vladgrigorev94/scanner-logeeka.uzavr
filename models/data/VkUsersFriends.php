<?php

namespace app\models\data;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class VkUsersFriends extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vk_users_friends';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'friend_id'], 'integer'],
            [['user_id', 'friend_id'], 'unique', 'targetAttribute' => ['user_id', 'friend_id']],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => VkUsers::class,
                'targetAttribute' => ['user_id' => 'vk_id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'friend_id' => 'Friend ID',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(VkUsers::class, ['vk_id' => 'user_id']);
    }
}