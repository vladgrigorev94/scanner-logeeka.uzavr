<?php

namespace app\models\data;

use Yii;
use yii\db\ActiveRecord;

class Photos extends ActiveRecord
{
    const IMAGE_PATH_ORIGINAL = '/tasks/{title_lat}/images/{link}';
    const IMAGE_PATH_THUMB = '/tasks/{title_lat}/thumbs/{link}';

    public static function tableName()
    {
        return '{{%photos}}';
    }

    public function rules()
    {
        return [
            [['gallery_id', 'timestamp', 'locked'], 'integer'],
            [['original_link', 'preview_link', 'o_thumbnail', 'source'], 'string', 'max' => 255],
            [
                ['gallery_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Galleries::class,
                'targetAttribute' => ['gallery_id' => 'id']
            ],
        ];
    }

    public function attributes()
    {
        return [
            'id',
            'l_name',
            'l_source',
            'source',
            'people_api_old',
            'b_top',
            'b_left',
            'b_right',
            'b_bottom',
            'score',
            'date_create',
            'locked',
            'title_lat',
            'gallery_id',
            'gallery_id2',
            'original_link',
            'preview_link',
            'people_api',
            'camera',
            'date_detect',
            'o_photo',
            'o_thumbnail',
            'timestamp',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'l_name' => '',
            'l_source' => '',
            'title_lat' => 'Название галереи латинское',
            'gallery_id' => 'Галерея',
            'gallery_id2' => 'Галерея 2',
            'original_link' => 'Ссылка на оригинал',
            'preview_link' => 'Ссылка на превью',
            'o_thumbnail' => 'Thumbnail'
        ];
    }

    public function getFindface()
    {
        return $this->hasMany(Findface::class, ['photos_id' => 'id']);
    }

    public function getHasFindfaces()
    {
        return !empty($this->getFindface()->select(['id'])->limit(1)->one());
    }

    public function getGallery($toTitleLat = false)
    {
        if ($toTitleLat) {
            $result = $this->hasOne(Galleries::class, ['id' => 'gallery_id']);
        } else {
            $result = $this->hasOne(Galleries::class, [
                'id' => (empty($this->gallery_id2))
                    ? 'gallery_id'
                    : 'gallery_id2'
            ]);
        }

        return $result;
    }

    public function getGalleryFirst()
    {
        return $this->hasOne(Galleries::class, ['id' => 'gallery_id']);
    }

    public function getGallerySecond()
    {
        return $this->hasOne(Galleries::class, ['id' => 'gallery_id2']);
    }

    public function getTitleLat()
    {
        return $this->getGallery(true)->one()->title_lat;
    }

    public function getImageUrlOriginal()
    {
        return self::getRelativeImageUrl($this->gallery->title_lat, $this->original_link, self::IMAGE_PATH_ORIGINAL);
    }

    public function getImageUrlThumb()
    {
        return self::getRelativeImageUrl($this->galleryFirst->title_lat, $this->preview_link, self::IMAGE_PATH_THUMB);
    }

    private static function getRelativeImageUrl($title_lat, $link, $template)
    {
        return str_replace(
            [
                '{title_lat}',
                '{link}',
            ],
            [
                $title_lat,
                $link
            ],
            $template
        );
    }

    public static function generateFullPath($title_lat, $link)
    {
        return implode(DIRECTORY_SEPARATOR, [
            Yii::getAlias('@app'),
            'web' . self::getRelativeImageUrl($title_lat, $link, self::IMAGE_PATH_ORIGINAL),
        ]);
    }

    public static function getDirectoryOriginalImage($title_lat)
    {
        return implode(DIRECTORY_SEPARATOR, [
            Yii::getAlias('@app'),
            'web',
            'tasks',
            $title_lat,
            'images'
        ]);
    }

    public function getFaceCoord()
    {
        return [
            'top' => $this->b_top,
            'left' => $this->b_left,
            'right' => $this->b_right,
            'bottom' => $this->b_bottom,
        ];
    }

    public function getFaceCoordinateForCanvas()
    {
        return [
            'sx' => $this->b_left ?? 0,
            'sy' => $this->b_top ?? 0,
            'swidth' => ($this->b_right ?? 0) - ($this->b_left ?? 0),
            'sheight' => ($this->b_bottom ?? 0) - ($this->b_top ?? 0),
        ];
    }


    public static function savePhotos($photos)
    {
        $res = false;

        foreach ($photos as $photo) {
            $url = $photo['sizes'][6]['url'];
            $thumbnail = $photo['sizes'][5]['url'];
            $obj = self::find()->where(['o_photo' => $url])->one();
            if (!$obj) {
                $obj = new self();
            }
            $obj->gallery_id = $photo['owner_id'];
            $obj->o_thumbnail = $thumbnail;
            $obj->o_photo = $url;
            $obj->timestamp = $photo['date'];
            $obj->locked = false;
            $obj->source = '';
            $obj->date_create = date('Y-m-d H:i:s');
            $res = $obj->save();

            if (!$res) {
                break;
            }
        }

        return $res;
    }
}
