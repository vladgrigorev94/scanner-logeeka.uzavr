<?php

namespace app\models\data;

use yii\db\ActiveRecord;
use yii\db\Query;

class VkGroups extends ActiveRecord
{
    const WATCH = 1;
    const SOC_FIELD = 'vk_id';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%vk_groups}}';
    }

    public static function getGroups()
    {
        return (new Query())->select(['`vk_id`, `title`'])
            ->from(self::tableName())
            ->where(['watch' => 1])
            ->indexBy('vk_id')
            ->all();
    }

    public function rules()
    {
        return [
            [[
                'id', 'vk_id', 'members',
                'date', 'watch', 'posts',
                'posts_offset', 'members_offset', 'update_wall_mode'
            ], 'integer'],
            [['title', 'type', 'error'], 'string', 'max' => 255],
        ];
    }

    public function attributes()
    {
        return [
            'id',
            'vk_id',
            'title',
            'members',
            'date',
            'watch',
            'posts',
            'posts_offset',
            'members_offset',
            'update_wall_mode',
            'type',
            'error'
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vk_id' => 'ID Группа',
            'title' => 'Название группы',
        ];
    }

    public function getFindfaces()
    {
        return $this->hasMany(Findface::class, ['id' => 'findface_id'])
            ->viaTable(VkGroups::tableName(), ['groups_id' => 'id']);
    }

    public function getInternetUsers()
    {
        return $this->hasMany(InternetUser::class, ['soc_network_user_id' => 'vk_users_id'])->viaTable(VkUsersGroups::tableName(), ['vk_groups_id' => 'vk_id']);
    }

    public function getPosts()
    {
        return $this->hasMany(Posts::class, ['group_id' => 'vk_id']);
    }

    public static function saveGroup($group)
    {
        $obj = self::find()->where(['vk_id' => $group['id']])->one();
        if (!$obj) {
            $obj = new self();
        }
        $obj->vk_id = $group['id'];
        $obj->title = $group['name'];
        $obj->members = $group['members_count']??0;
        $obj->type = $group['type'];
        $obj->error = 'none';
        $obj->date = $group['start_date']??0;
        $obj->watch = 0;
        $obj->posts = 0;
        $obj->posts_offset = 0;
        $obj->members_offset = 0;
        $obj->update_wall_mode = 0;

        return $obj->save();
    }
}
