<?php

namespace app\models\data;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * This is the model class for table "options".
 *
 * @property int $id
 * @property string $option_name
 * @property string $option_value
 */
class Options extends ActiveRecord
{
    const NAME_INTERNET_USERS_ID_TO_CONSOLE = 'internetusers_id_to_console';
    const NAME_LAST_PHOTOS_ID = 'last_photos_id';
    const NAME_VIDEO_SERVICE_TOKEN = 'video_service_token';
    const NAME_VIDEO_SERVICE_USERNAME = 'video_service_username';
    const NAME_VIDEO_SERVICE_PASS = 'video_service_pass';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'options';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['option_name'], 'required'],
            [['option_value'], 'string'],
            [['option_name'], 'string', 'max' => 191],
            [['option_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'option_name' => 'Option Name',
            'option_value' => 'Option Value',
        ];
    }

    /**
     * @param $name
     * @return array|ActiveRecord|Options|null
     */
    public static function findByName($name)
    {
        return self::find()
            ->where(['option_name' => $name])
            ->limit(1)
            ->one();
    }

    public static function updateOptionValueByName($value, $name)
    {
        $result = true;

        try {
            $result = (bool)Yii::$app->db->createCommand()
                ->update(self::tableName(), ['option_value' => $value], ['option_name' => $name])
                ->execute();
        } catch (Exception $e) {
            $result = false;
        }

        return $result;
    }
}
