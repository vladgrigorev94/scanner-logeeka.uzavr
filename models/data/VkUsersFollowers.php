<?php

namespace app\models\data;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "vk_users_followers".
 *
 * @property int $id
 * @property int $user_id
 * @property string $follower_id
 */
class VkUsersFollowers extends ActiveRecord
{
    /**
     * Наименование поля, содержащее id того, к кому подписан
     */
    const LEADER_FIELD = 'user_id';

    /**
     * Наименование поля, содержащее id того, кто подписан
     */
    const FOLLOWER_FIELD = 'follower_id';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%vk_users_followers}}';
    }

    public function attributes()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID user',
            'follower_id' => 'ID follower',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(VkUsers::class, ['vk_id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getFollower()
    {
        return $this->hasOne(VkUsers::class, ['vk_id' => 'follower_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserInternetUser()
    {
        return $this->hasOne(InternetUser::class, ['soc_network_user_id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getFollowerInternetUser()
    {
        return $this->hasOne(InternetUser::class, ['soc_network_user_id' => 'follower_id']);
    }
}
