<?php

namespace app\models\data;

use Yii;

/**
 * This is the model class for table "univ".
 *
 * @property int $id
 * @property string $title
 */
class University extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'univ';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app\university', 'ID'),
            'title' => Yii::t('app\university', 'Title'),
        ];
    }
}
