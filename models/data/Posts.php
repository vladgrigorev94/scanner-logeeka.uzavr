<?php

namespace app\models\data;

use yii\db\ActiveRecord;

class Posts extends ActiveRecord
{
    const TITLE = 'Публикации в группах';

    public static function tableName()
    {
        return 'posts';
    }

    public function rules()
    {
        return [
            [[
                'post_id', 'owner_id', 'date', 'soc_network_id',
                'group_id', 'comments', 'likes', 'reposts',
                'views', 'parent_post_id', 'parent_owner_id',
                'is_parent', 'up_time', 'likes_parse',
                'comments_parse'
            ], 'integer'],
            [['post_text'], 'string'],
            [['soc_network_id', 'post_id', 'owner_id'], 'unique', 'targetAttribute' => ['soc_network_id', 'post_id', 'owner_id']],
        ];
    }

    public function attributes()
    {
        return [
            'id',
            'post_id',
            'owner_id',
            'date',
            'soc_network_id',
            'group_id',
            'post_text',
            'comments',
            'likes',
            'reposts',
            'views',
            'parent_post_id',
            'parent_owner_id',
            'is_parent',
            'up_time',
            'cnt_comments_alignment',
            'proc_comments_alignment',
            'cnt_likes_alignment',
            'proc_likes_alignment',
            'events_id',
            'likes_parse',
            'comments_parse',
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'ID публикации',
            'owner_id' => 'ID автора публикации',
            'date' => 'Дата публ.',
            'soc_network_id' => 'ID соц сети',
            'group_id' => 'ID группы',
            'post_text' => 'Текст публикации',
            'comments' => 'Количество коммент.',
            'likes' => 'Количество лайков',
            'reposts' => 'Количество репостов',
            'views' => 'Количество просмотров',
            'parent_post_id' => 'ID родительской публикации',
            'parent_owner_id' => 'ID автора родительской публикации',
            'is_parent' => 'Является родительской',
            'up_time' => 'Время актуальности',
            'cnt_comments_alignment' => 'Количество коммент. от польз. в статусе Подходит',
            'proc_comments_alignment' => 'Процент коммент. от польз. в статусе Подходит',
            'cnt_likes_alignment' => 'Количество лайков от польз. в статусе Подходит',
            'proc_likes_alignment' => 'Процент лайков от польз. в статусе Подходит',
            'likes_parse' => 'Лайки обработаны',
            'comments_parse' => 'Комментарии обработаны',
        ];
    }

    public function getPostAttachments()
    {
        return $this->hasMany(PostAttachments::class, ['post_id' => 'post_id']);
    }

    public function getPostComments()
    {
        return $this->hasMany(PostComments::className(), ['post_id' => 'post_id']);
    }

    public function getFindface()
    {
        return $this->hasMany(Findface::class, ['internet_users_id' => 'internet_users_id']);
    }

    public function getInternetUser()
    {
        return $this->hasOne(InternetUser::class, ['id' => 'internet_users_id']);
    }

    public function getSocNetwork()
    {
        return $this->hasOne(SocNetworks::class, ['id' => 'soc_network_id']);
    }

    public static function saveDataByPostId(
        $postId,
        $attachments,
        array $data = [],
        $socNetwork = 1,
        $result = []
    )
    {
        $post = self::findOne(['post_id' => $postId]);

        if (!$post) {
            $post = new self();
        }

        $post->post_id = $postId;

        foreach ($data as $key => $value) {
            $post[$key] = $value;
        }

        $postSave = $post->save();

        $attachmentSave = false;

        if (!empty($attachments[0])) {
            foreach ($attachments as $attachment) {
                if (empty($attachment['type'])) {
                    codecept_debug($attachments);
                    die;
                }
                $type = $attachment['type'];
                $attachmentInfo = $attachment[$type];
                $postAttachments = PostAttachments::find()
                    ->where(['post_id' => $postId])
                    ->andWhere(['attachment_id' => $attachmentInfo['id']??0])
                    ->one();

                if (!$postAttachments) {
                    $postAttachments = new PostAttachments();
                }

                $postAttachments->post_id = $postId;
                $postAttachments->attachment_id = $attachmentInfo['id']??0;
                $postAttachments->owner_id = $attachmentInfo['owner_id']??0;
                $postAttachments->title = $attachmentInfo['text'] ?? '';
                $postAttachments->type = $type;
                $postAttachments->date = $attachmentInfo['date'] ?? '';
                $postAttachments->soc_network_id = $socNetwork;
                $postAttachments->access_key = $attachmentInfo['access_key'] ?? '';
                $postAttachments->url = $attachmentInfo['photo_1280'] ?? '';
                $postAttachments->attachment_content = $attachmentInfo['photo_1280'] ?? '';
                $postAttachments->comment_id = 0;
                $attachmentSave = $postAttachments->save();

                if (!$attachmentSave) {
                    break;
                }
            }
        } else {
            $postAttachments = new PostAttachments();
            $postAttachments->post_id = $postId;
            $postAttachments->attachment_id = 0;
            $postAttachments->owner_id = $attachments['owner_id']??0;
            $postAttachments->title = $attachments['text'] ?? '';
            $postAttachments->type = 'video';
            $postAttachments->date = $attachments['date'] ?? '';
            $postAttachments->soc_network_id = $socNetwork;
            $postAttachments->access_key = $attachments['access_key'] ?? '';
            $postAttachments->url = $attachments['photo_1280'] ?? '';
            $postAttachments->attachment_content = $attachments['photo_1280'] ?? '';
            $postAttachments->comment_id = 0;
            $attachmentSave = $postAttachments->save();
        }

        return $postSave && $attachmentSave;
    }

    /**
     * @param $socNetworkId
     * @return self[]
     */
    public static function getUnparsedLikesPosts($socNetworkId): array
    {
        return self::find()
            ->where(['likes_parse' => 0])
            ->andWhere(['soc_network_id' => $socNetworkId])
            ->all();
    }

    /**
     * @param $socNetworkId
     * @return self[]
     */
    public static function getUnparsedCommentsPosts($socNetworkId): array
    {
        return self::find()
            ->where(['comments_parse' => 0])
            ->andWhere(['soc_network_id' => $socNetworkId])
            ->all();
    }

    public function setLikesParsed()
    {
        $this->likes_parse = 1;

        return $this->update();
    }

    public function setCommentsParsed()
    {
        $this->comments_parse = 1;

        return $this->update();
    }
}
