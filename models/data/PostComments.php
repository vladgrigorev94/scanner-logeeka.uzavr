<?php

namespace app\models\data;

use yii\db\ActiveRecord;

class PostComments extends ActiveRecord
{

    public static function tableName()
    {
        return 'post_comments';
    }

    public function rules()
    {
        return [
            [[
                'post_id', 'comment_id', 'owner_id',
                'from_user_id', 'soc_network_id', 'date',
                'likes', 'update_time', 'comment_likes_parse', 'user_parse'
            ], 'integer'],
            [['comment_text'], 'string'],
            [['soc_network_id', 'post_id', 'owner_id', 'comment_id'], 'unique', 'targetAttribute' => ['soc_network_id', 'post_id', 'owner_id', 'comment_id']],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Posts::class, 'targetAttribute' => ['post_id' => 'post_id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'comment_id' => 'Comment ID',
            'owner_id' => 'Owner ID',
            'from_user_id' => 'From User ID',
            'soc_network_id' => 'Soc Network ID',
            'date' => 'Date',
            'comment_text' => 'Comment Text',
            'likes' => 'Likes',
            'update_time' => 'Update Time',
            'comment_likes_parse' => 'Лайки комментариев обработаны',
            'comment_comment_parse' => 'Комментарии комментариев обработаны',
            'user_parse' => 'Юзеры обработаны',
        ];
    }

    public function getPost()
    {
        return $this->hasOne(Posts::class, ['post_id' => 'post_id']);
    }

    public function getFindface()
    {
        return $this->hasMany(Findface::class, ['internet_users_id' => 'internet_users_id']);
    }

    public function getInternetUser()
    {
        return $this->hasOne(InternetUser::class, ['id' => 'internet_users_id']);
    }

    public function getSocNetwork()
    {
        return $this->hasOne(SocNetworks::class, ['id' => 'soc_network_id']);
    }

    public static function saveComments($comments = [], $ownerId = 0, $postId = 0)
    {
        foreach ($comments as $comment) {
            $obj = self::find()
                ->where(['post_id' => $postId])
                ->andWhere(['comment_id' => $comment['id']])
                ->andWhere(['owner_id' => $ownerId])
                ->one();
            if (!$obj) {
                $obj = new self();
            }
            $obj->post_id = $postId;
            $obj->comment_id = $comment['id'];
            $obj->owner_id = $ownerId;
            $obj->from_user_id = $comment['from_id'];
            //TODO::ADD CONSTANT
            $obj->soc_network_id = 1;
            //TODO::ADD RIGHT DATE
            $obj->date = $comment['date'];
            $obj->comment_text = $comment['text'];
            $obj->likes = $comment['likes']['count'];
            $obj->update_time = 123;
            $obj->group_id = $ownerId;
            $obj->save();
        }
    }

    /**
     * @param $socNetworkId
     * @return self[]
     */
    public static function getUncheckedCommentsWithLikes($socNetworkId): array
    {
        return self::find()
            ->where(['comment_likes_parse' => 0])
            ->andWhere(['soc_network_id' => $socNetworkId])
            ->all();
    }

    public function setParsedCommentsLikes()
    {
        $this->comment_likes_parse = 1;

        return $this->update();
    }

    public static function getUnparsedCommentsComments($socNetworkId): array
    {
        return self::find()
            ->where(['comment_likes_parse' => 0])
            ->andWhere(['soc_network_id' => $socNetworkId])
            ->all();
    }

    public function setCommentsCommentsParsed()
    {
        $this->comment_comment_parse = 1;

        return $this->update();
    }

    public static function getUnparsedCommentsUsers($socNetworkId): array
    {
        return self::find()
            ->where(['user_parse' => 0])
            ->andWhere(['soc_network_id' => $socNetworkId])
            ->all();
    }
}
