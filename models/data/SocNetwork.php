<?php

namespace app\models\data;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "soc_networks".
 *
 * @property int $id
 * @property string $name
 *
 * @property InternetUser[] $internetUsers
 */
class SocNetwork extends ActiveRecord
{
    const ID_VK = 1;
    const ID_OK = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'soc_networks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInternetUsers()
    {
        return $this->hasMany(InternetUser::className(), ['soc_network_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getAllSocialNetworks()
    {
        return self::find()
            ->select('name')
            ->indexBy('id')
            ->asArray()
            ->column();
    }
}
