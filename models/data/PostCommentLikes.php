<?php

namespace app\models\data;

use SocialApiLibrary\api\SocialApi;
use yii\db\ActiveRecord;

class PostCommentLikes extends ActiveRecord
{


    public static function tableName()
    {
        return 'post_comment_likes';
    }

    public function rules()
    {
        return [
            [['liked_item_id', 'owner_id', 'from_user_id', 'soc_network_id', 'update_time', 'group_id', 'user_parse'], 'integer'],
            [['type'], 'string', 'max' => 255],
            [['soc_network_id', 'liked_item_id', 'owner_id', 'from_user_id'], 'unique', 'targetAttribute' => ['soc_network_id', 'liked_item_id', 'owner_id', 'from_user_id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'liked_item_id' => 'Liked Item ID',
            'owner_id' => 'Owner ID',
            'from_user_id' => 'From User ID',
            'type' => 'Type',
            'soc_network_id' => 'Soc Network ID',
            'update_time' => 'Update Time',
            'group_id' => 'Group ID',
            'user_parse' => 'Юзеры обработаны',
        ];
    }

    public function getComment()
    {
        return $this->hasOne(PostComments::class, ['comment_id' => 'liked_item_id']);
    }

    public function getPost()
    {
        return $this->hasOne(Posts::class, ['post_id' => 'liked_item_id']);
    }

    public function getSocNetwork()
    {
        return $this->hasOne(SocNetworks::class, ['id' => 'soc_network_id']);
    }

    public static function saveLikes(
        $likes = [],
        $ownerId = 0,
        $itemId = 0,
        $likedItemId = 0,
        $type = SocialApi::LIKES_TYPE_POST
    )
    {
        foreach ($likes as $like) {
            $obj = self::find()
                ->where(['liked_item_id' => $itemId])
                ->andWhere(['from_user_id' => $like['id']])
                ->one();
            if (!$obj) {
                $obj = new self();
            }
            $obj->liked_item_id = $likedItemId ?? $itemId;
            $obj->owner_id = $ownerId;
            $obj->from_user_id = $like['id'];
            //TODO::ADD CONSTANT
            $obj->type = $type;
            //TODO::ADD CONSTANT
            $obj->soc_network_id = 1;
            $obj->update_time = 123;
            $obj->group_id = $ownerId;
            $obj->save();
        }
    }

    /**
     * @param $socNetworkId
     * @param bool $asArray
     * @param array $options
     * @return self[]
     */
    public static function getUnparsedLikesUsers(
        $socNetworkId,
        $asArray = true,
        $options = []
    ): array
    {
        $res = self::find()
            ->where(['user_parse' => false])
            ->andWhere(
                'from_user_id != :from_user_id',
                [
                    'from_user_id' => 1
                ]
            )
            ->andWhere(['soc_network_id' => $socNetworkId])
            ->orderBy(['id' => SORT_ASC]);
        if ($asArray) {
            $res = $res->asArray();
        }
        if ($options['offset']) {
            $res = $res->offset($options['offset']);
        }
        if ($options['limit']) {
            $res = $res->limit($options['limit']);
        }
        $res = $res->all();

        return $res;
    }

    public static function setUsersParsed($users)
    {
        foreach ($users as $user) {
            $obj = self::find()
                ->where(['from_user_id' => $user])
                ->one();

            if ($obj) {
                $obj->user_parse = 1;

                $obj->update();
            }
        }
    }
}
