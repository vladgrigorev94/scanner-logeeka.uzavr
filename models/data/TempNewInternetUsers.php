<?php

namespace app\models\data;

use Yii;

/**
 * This is the model class for table "temp_new_internet_users".
 *
 * @property int $id
 * @property int $internet_user_id
 */
class TempNewInternetUsers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temp_new_internet_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['internet_user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'internet_user_id' => 'Internet User ID',
        ];
    }
}
