<?php

namespace app\models\data;

use app\models\data\query\FindfaceQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;

/**
 * This is the model class for table "findface".
 *
 * @property int $id ID
 * @property double $similarity
 * @property double $confidence
 * @property double $bbox_0
 * @property double $bbox_1
 * @property double $bbox_2
 * @property double $bbox_3
 * @property double $width
 * @property double $height
 * @property string $photo_url_hash
 * @property int $photo_id
 * @property int $user_id
 * @property string $photo_url
 * @property string $profile_url
 * @property string $first_name Имя
 * @property string $last_name Фамилия
 * @property string $avatar Аватар
 * @property int $photos_id
 * @property int $social
 * @property string $soc_network
 * @property string $error
 * @property integer $eventsCount
 * @property string $galleryTitle
 * @property string $alignment
 * @property int $internet_users_id
 *
 *
 * @property Photos $photos
 *
 * @property InternetUser $internetUser
 * @property VkUsersGroups $vkUsersGroups
 * @property OkUsersGroups $okUsersGroups
 * @property FbGroupUser $fbUsersGroups
 * @property InstaUsersFollowers $instaUsersFollowers
 */
class Findface extends ActiveRecord
{
    const TITLE = 'Интернет-пользователи';
    const FB_ACCOUNTS_LINK = 'https://www.facebook.com/';
    const VK_ACCOUNTS_LINK = 'https://vk.com/';
    const OK_ACCOUNTS_LINK = 'https://ok.ru/profile/';
    const FB_REGEXP_ACCOUNTS_LINK = 'https:\/\/www\.facebook\.com\/(.*)\/?';
    const VK_REGEXP_ACCOUNTS_LINK = 'https:\/\/vk\.com\/([_a-zA-Z0-9]*)';
    const OK_REGEXP_ACCOUNTS_LINK = 'https:\/\/ok\.ru\/profile\/([_a-zA-Z0-9]*)';

    const ALIGNMENT_ALIGNMENT = 'alignment';
    const ALIGNMENT_NOT_ALIGNMENT = 'not alignment';
    const ALIGNMENT_PROBABLY = 'probably';
    const ALIGNMENT_NOT_CHOOSE = 'not choose';

    // Хорошие
    const ALIGNMENT_PROS = [self::ALIGNMENT_ALIGNMENT, self::ALIGNMENT_PROBABLY];
    // НЕ хорошие
    const ALIGNMENT_CONS = [self::ALIGNMENT_NOT_CHOOSE, self::ALIGNMENT_NOT_ALIGNMENT];

    // Гаратированно одобренные
    const ALIGNMENT_APPROVED = [self::ALIGNMENT_ALIGNMENT];
    // Могущие стать гарантированно одобренными
    const ALIGNMENT_MAYBE = [self::ALIGNMENT_PROBABLY, self::ALIGNMENT_NOT_CHOOSE];
    // НЕ отклоненные
    const ALIGNMENT_NOT_DISCARD = [self::ALIGNMENT_PROBABLY, self::ALIGNMENT_NOT_CHOOSE, self::ALIGNMENT_ALIGNMENT];
    // Все
    const ALIGNMENT_ALL = [self::ALIGNMENT_PROBABLY, self::ALIGNMENT_NOT_CHOOSE, self::ALIGNMENT_NOT_ALIGNMENT, self::ALIGNMENT_ALIGNMENT];





    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%findface}}';
    }

    public static function getFindfacesByPeopleApi(array $peopleApiIds)
    {
        $findfacesData = self::getFindfacesByPeopleApiQuery($peopleApiIds)->all();

        $findfaces = [];

        foreach ($findfacesData as $item) {
            $findfaces[$item['id']][$item['soc_network']][$item['user_id']][] = $item;
        }

        return $findfaces;
    }

    /**
     * @param array|int $peopleId
     * @return Query
     */
    public static function getFindfacesByPeopleApiQuery($peopleId)
    {
        return (new Query())
            ->select([
                'p.[[people_api]] AS id',
                'ff.[[user_id]]',
                'ff.[[id]] AS findface',
                'iu.[[name]]',
                'ff.[[alignment]]',
                'iu.[[avatar]]',
                'ff.[[photo_url]]',
                'ff.[[profile_url]]',
                'iu.[[events_count]]',
                'ff.[[confidence]]',
                'ff.[[soc_network]]',
                'ff.[[photos_id]]',
                'iu.[[groups_count]]',
                'iu.[[confirmed]]',
                'iu.[[id]] AS internet_users_id'
            ])
            ->from(['p' => Photos::tableName()])
            ->innerJoin(['ff' => self::tableName()],'p.[[id]] = ff.[[photos_id]]')
            ->innerJoin(['iu' => InternetUser::tableName()],'iu.[[id]] = ff.[[internet_users_id]]')
            ->where(['p.[[people_api]]' => $peopleId])
            ->andWhere('ff.[[alignment]] IN ("alignment","probably")')
            ->orderBy([
                new Expression(
                    'FIELD (alignment, ' . implode(',',
                        [
                            '"alignment"',
                            '"probably"'
                        ]
                    ) . ')')
            ]);
    }

    public static function getFindfacesByIds(array $find_face_ids)
    {
        return (new Query())->select(['*'])
            ->from(self::tableName())
            ->where(['id' => $find_face_ids])
            ->indexBy('id')
            ->all();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['similarity', 'bbox_0', 'bbox_1', 'bbox_2', 'bbox_3', 'width', 'height'], 'number'],
            [['id','events_count','photo_id', 'user_id', 'photos_id', 'social'], 'integer'],
            [['error','name'], 'string'],
            [['photo_url_hash', 'first_name', 'last_name'], 'string', 'max' => 255],
            [['soc_network'], 'string', 'max' => 2],
            [
                [
                    'photos_id'
                ],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Photos::class,
                'targetAttribute' => [
                    'photos_id' => 'id'
                ]
            ],
        ];
    }

    public function attributes()
    {
        return [
            'id',
            'similarity',
            'confidence',
            'bbox_0',
            'bbox_1',
            'bbox_2',
            'bbox_3',
            'width',
            'height',
            'photo_url_hash',
            'photo_id',
            'user_id',
            'photo_url',
            'profile_url',
            'photos_id',
            'soc_network',
            'error',
            'alignment',
            'events_count',
            'galleryTitle',
            'name',
            'events_id',
            'galleries_id',
            'groups_id',
            'avatar',
            'first_name',
            'last_name',
            'social',
            'internet_users_id',
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'similarity'     => 'Similarity',
            'confidence'     => 'Confidence',
            'bbox_0'         => 'Bbox 0',
            'bbox_1'         => 'Bbox 1',
            'bbox_2'         => 'Bbox 2',
            'bbox_3'         => 'Bbox 3',
            'width'          => 'Width',
            'height'         => 'Height',
            'photo_url_hash' => 'Photo Url Hash',
            'photo_id'       => 'Photo ID',
            'user_id'        => 'User ID',
            'photo_url'      => 'Фото',
            'profile_url'    => 'Профиль',
            'name'           => 'ФИО',
            'first_name'     => 'Имя',
            'last_name'      => 'Фамилия',
            'avatar'         => 'Аватар',
            'photos_id'      => 'Photos ID',
            'social'         => 'Social',
            'soc_network'    => 'Soc Network',
            'error'          => 'Error',
            'events_count'    => 'Количество мероприятий',
            'galleryTitle'   => 'Название галереи',
            'groups_count'   => 'Количество групп'
        ];
    }

    /**
     * {@inheritdoc}
     * @return FindfaceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FindfaceQuery(get_called_class());
    }

    /**
     * @return ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(VkGroups::class, ['id' => 'groups_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPhoto()
    {
        return $this->hasOne(Photos::class, ['id' => 'photos_id']);
    }

    /**
     * @return array
     */
    public function getGalleries()
    {
        $galleries = [];

        /** @var Photos $photo */
        foreach ($this->photos as $photo) {
            $galleries[] = $photo->gallery;
        }

        return $galleries;
    }

    /**
     * @return array
     */
    public function getEvents()
    {
        $events = [];

        /** @var Galleries $gallery */
        foreach ($this->getGalleries() as $gallery) {
            $events[] = $gallery->event;
        }

        return $events;
    }


    /**
     * @return ActiveQuery
     */
    public function getInternetUser()
    {
        return $this->hasOne(InternetUser::class, ['id' => 'internet_users_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getFindfaces()
    {
        return $this->hasMany(self::class, ['internet_users_id' => 'internet_users_id'])
            ->from(['ff' => self::tableName()]);
    }

    /**
     * @return ActiveQuery
     */
    public function getVkUsersGroups()
    {
        return $this->hasMany(VkUsersGroups::class, ['vk_users_id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOkUsersGroups()
    {
        return $this->hasMany(OkUsersGroups::class, ['ok_users_id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getFbGroupsUsers()
    {
        return $this->hasMany(FbGroupUser::class, ['ACCOUNT_ID' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInstaUsersFollowers()
    {
        return $this->hasMany(InstaUsersFollowers::class, ['insta_follower_id' => 'user_id']);
    }

    /**
     * Get Findfaces for update InternetUser counters.
     *
     * @param int|array $internetUserId
     * @return array
     */
    public static function getFindfacesForUpdateIuCounters(int $internetUserId)
    {
        return self::find()
            ->alias('ff')
            ->select([
                'ff.user_id',
                'ff.soc_network',
                'ff.alignment',
                'gallery_id' => 'g.id',
                'g.event_id'
            ])
            ->innerJoinWith('photo.gallerySecond g', false)
            ->innerJoinWith('photo.peopleApi pa', false)
            ->where([
                'ff.alignment' => self::ALIGNMENT_NOT_DISCARD,
                'pa.active' => PeopleApi::ACTIVE,
                'ff.internet_users_id' => $internetUserId,
            ])
            ->asArray()
            ->all();
    }
}
