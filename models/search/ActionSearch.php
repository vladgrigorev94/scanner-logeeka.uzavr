<?php

namespace app\models\search;

use app\models\data\Action;
use yii\data\ActiveDataProvider;

class ActionSearch extends Action
{
    /**
     * @var string
     */
    public $rules_group_id;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'action_type_id', 'rules_group_id'], 'integer'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Action::find()
            ->joinWith(['user.group', 'actionType', 'detailView', 'detailCreate', 'detailEdit', 'detailDownload']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'forcePageParam' => false,
                'pageSizeParam' => false,
                'pageSize' => 10
            ]
        ]);

        $dataProvider->sort->attributes['rules_group_id'] = [
            'asc' => ['rules_group.id' => SORT_ASC],
            'desc' => ['rules_group.id' => SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'actions.user_id' => $this->user_id,
            'actions.action_type_id' => $this->action_type_id,
            'rules_group.id' => $this->rules_group_id,
        ]);

        return $dataProvider;
    }
}
