<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23.09.2019
 * Time: 17:42
 */

namespace app\models\search;


use app\models\data\Findface;
use app\models\data\InternetUser;
use app\models\data\Posts;
use yii\data\ArrayDataProvider;
use yii\db\Query;


class PostsSearch extends Posts
{
    public $page = 1;
    public $pageSize = 50;
    public $totalCount = 0;

    public function search($params)
    {
        $this->pageSize = $params['per-page'] ?? $this->pageSize;
        $this->page = $params['page'] ?? $this->page;

        $main_query = (new Query())->select([
            'pst.[[id]]',
            'pst.[[owner_id]]',
            'pst.[[post_id]]',
            'pst.[[date]]',
            'pst.[[soc_network_id]]',
            'pst.[[group_id]]',
            'pst.[[post_text]]',
            'pst.[[comments]]',
            'pst.[[likes]]',
            'pst.[[reposts]]',
            'pst.[[views]]',
            'pst.[[cnt_comments_alignment]]',
            'pst.[[proc_comments_alignment]]',
            'pst.[[cnt_likes_alignment]]',
            'pst.[[proc_likes_alignment]]',
            'pst.[[parent_post_id]]',
            'pst.[[parent_owner_id]]',
        ]);


        $main_query->limit($this->pageSize);
        $main_query->offset(($this->page - 1) * $this->pageSize);

        $main_query->from(['pst' => Posts::tableName()]);
        $main_query->where(['`pst`.[[is_parent]]' => 0]);

        if (!empty($params['sort'])) {
            if (substr_count($params['sort'], '-',0,1) > 0) {
                $main_query->orderBy(['`pst`.'.substr($params['sort'],1) => SORT_DESC]);
            } else {
                $main_query->orderBy(['`pst`.'.$params['sort'] => SORT_ASC]);
            }
        } else {
            $main_query->orderBy([
                'pst.[[cnt_comments_alignment]]' => SORT_DESC,
                'pst.[[cnt_likes_alignment]]' => SORT_DESC,
            ]);
        }

        if (!empty($params['PostsSearch'])) {
            $search = $params['PostsSearch'];

            foreach ($search as $key => $val) {
                if (!empty($val)) {
                    switch ($key) {
                        case 'id':
                            $main_query->andWhere(['pst.[[id]]' => (integer)$val]);
                            break;
                        case 'post_text':
                            $main_query->andWhere(['LIKE','pst.[[post_text]]',$val]);
                            break;
                        case 'group_id':
                            $groups_filter_array = explode(':', $val);
                            if (count($groups_filter_array) == 2) {
                                $main_query->andWhere([
                                    '`pst`.[[soc_network_id]]' => $groups_filter_array[0],
                                    '`pst`.[[group_id]]' => $groups_filter_array[1]
                                ]);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            if(!empty($search['events_id'])) {
                $main_query->andWhere(['id' =>
                    (new Query())->select('item_id')
                        ->from('post_events')
                        ->where(['event_id' => $search['events_id'], 'type' => 'post'])
                ]);
            }

        }

        $result = $main_query->indexBy('id')->all();

        $this->totalCount = $main_query->cache(1)-> count('pst.id');

        return new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => [
                'pageSize' => $this->pageSize,
                'totalCount' => $this->totalCount
            ],
            'sort' => [
                'attributes' => [
                    'id',
                    'comments',
                    'likes',
                    'reposts',
                    'views'
                ]
            ]
        ]);
    }

    public function searchParentPosts(array $parentPostIds = [], array $parentOwnerIds = [])
    {
        $arParentPosts = [];
        if (count($parentPostIds) && count($parentOwnerIds)) {
            $arParentPosts = (new Query())->select([
                'pst.[[id]]',
                'pst.[[owner_id]]',
                'pst.[[post_id]]',
                'pst.[[date]]',
                'pst.[[soc_network_id]]',
                'pst.[[group_id]]',
                'pst.[[post_text]]',
                'pst.[[comments]]',
                'pst.[[likes]]',
                'pst.[[reposts]]',
                'pst.[[views]]',
                'pst.[[cnt_comments_alignment]]',
                'pst.[[proc_comments_alignment]]',
                'pst.[[cnt_likes_alignment]]',
                'pst.[[proc_likes_alignment]]',
                'pst.[[parent_post_id]]',
                'pst.[[parent_owner_id]]',
            ])->from(['pst' => Posts::tableName()])->where([
                'post_id' => $parentPostIds,
                'owner_id' => $parentOwnerIds
            ])->all();
        }

        return $arParentPosts;
    }

    /*Temporary code 18.09.2020*/
    public function searchPagesSpec($params)
    {
        $this->pageSize = $params['per-page'] ?? $this->pageSize;
        $this->page = $params['page'] ?? $this->page;

        $main_query = (new Query())->select([
            'pst.[[id]]',
            'pst.[[owner_id]]',
            'pst.[[post_id]]',
            'pst.[[date]]',
            'pst.[[soc_network_id]]',
            'pst.[[group_id]]',
            'pst.[[post_text]]',
            'pst.[[comments]]',
            'pst.[[likes]]',
            'pst.[[reposts]]',
            'pst.[[views]]',
            'pst.[[cnt_comments_alignment]]',
            'pst.[[proc_comments_alignment]]',
            'pst.[[cnt_likes_alignment]]',
            'pst.[[proc_likes_alignment]]',
            'pst.[[parent_post_id]]',
            'pst.[[parent_owner_id]]',
        ]);


        $main_query->limit($this->pageSize);
        $main_query->offset(($this->page - 1) * $this->pageSize);

        $main_query->from(['pst' => Posts::tableName()]);
        $main_query->where(['`pst`.[[is_parent]]' => 0]);

        if (!empty($params['sort'])) {
            if (substr_count($params['sort'], '-',0,1) > 0) {
                $main_query->orderBy(['`pst`.'.substr($params['sort'],1) => SORT_DESC]);
            } else {
                $main_query->orderBy(['`pst`.'.$params['sort'] => SORT_ASC]);
            }
        } else {
            $main_query->orderBy([
                'pst.[[cnt_comments_alignment]]' => SORT_DESC,
                'pst.[[cnt_likes_alignment]]' => SORT_DESC,
            ]);
        }

        if (!empty($params['PostsSearch'])) {
            $search = $params['PostsSearch'];

            foreach ($search as $key => $val) {
                if (!empty($val)) {
                    switch ($key) {
                        case 'id':
                            $main_query->andWhere(['pst.[[id]]' => (integer)$val]);
                            break;
                        case 'post_text':
                            $main_query->andWhere(['LIKE','pst.[[post_text]]',$val]);
                            break;
                        default:
                            break;
                    }
                }
            }

            if(!empty($search['events_id'])) {
                $main_query->andWhere(['id' =>
                    (new Query())->select('item_id')
                        ->from('post_events')
                        ->where(['event_id' => $search['events_id'], 'type' => 'post'])
                ]);
            }
        }

        if(isset($params['PostsSearch']) && !empty($params['PostsSearch']['owner_id'])) {
            $owner_filter_array = explode(':', $params['PostsSearch']['owner_id']);
            if (count($owner_filter_array) == 2) {
                $main_query->andWhere([
                    //'`pst`.[[soc_network_id]]' => intval($owner_filter_array[0]),
                    '`pst`.[[owner_id]]' => intval($owner_filter_array[1])
                ]);
            }
        } else {
            /*Get filtered IDs*/
            $specIuQuery = (new Query())->select([
                '[[soc_network_id]]',
                '[[soc_network_user_id]]'
            ])
                ->from('temp_spec_internet_users')
                ->where(['>','[[posts]]',0])
                ->all();
            foreach ($specIuQuery as $specItem) {
                $spec['soc_network_user_id'][] = $specItem['soc_network_user_id'];
                $spec['soc_network_id'][$specItem['soc_network_id']] = $specItem['soc_network_id'];
            }
            if (isset($spec) && is_array($spec)) {
                $main_query->andWhere([
                    'in','`pst`.[[owner_id]]',$spec['soc_network_user_id']
                ]);
            }
        }

        $result = $main_query->indexBy('id')->all();

        $this->totalCount = $main_query->cache(1)-> count('pst.id');

        return new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => [
                'pageSize' => $this->pageSize,
                'totalCount' => $this->totalCount
            ],
            'sort' => [
                'attributes' => [
                    'id',
                    'comments',
                    'likes',
                    'reposts',
                    'views'
                ]
            ]
        ]);
    }

    public function searchGroupsSpec($params)
    {
        $this->pageSize = $params['per-page'] ?? $this->pageSize;
        $this->page = $params['page'] ?? $this->page;

        $main_query = (new Query())->select([
            'pst.[[id]]',
            'pst.[[owner_id]]',
            'pst.[[post_id]]',
            'pst.[[date]]',
            'pst.[[soc_network_id]]',
            'pst.[[group_id]]',
            'pst.[[post_text]]',
            'pst.[[comments]]',
            'pst.[[likes]]',
            'pst.[[reposts]]',
            'pst.[[views]]',
            'pst.[[cnt_comments_alignment]]',
            'pst.[[proc_comments_alignment]]',
            'pst.[[cnt_likes_alignment]]',
            'pst.[[proc_likes_alignment]]',
            'pst.[[parent_post_id]]',
            'pst.[[parent_owner_id]]',
        ]);


        $main_query->limit($this->pageSize);
        $main_query->offset(($this->page - 1) * $this->pageSize);

        $main_query->from(['pst' => Posts::tableName()]);
        $main_query->where(['`pst`.[[is_parent]]' => 0]);

        if (!empty($params['sort'])) {
            if (substr_count($params['sort'], '-',0,1) > 0) {
                $main_query->orderBy(['`pst`.'.substr($params['sort'],1) => SORT_DESC]);
            } else {
                $main_query->orderBy(['`pst`.'.$params['sort'] => SORT_ASC]);
            }
        } else {
            $main_query->orderBy([
                'pst.[[cnt_comments_alignment]]' => SORT_DESC,
                'pst.[[cnt_likes_alignment]]' => SORT_DESC,
            ]);
        }

        if (!empty($params['PostsSearch'])) {
            $search = $params['PostsSearch'];

            foreach ($search as $key => $val) {
                if (!empty($val)) {
                    switch ($key) {
                        case 'id':
                            $main_query->andWhere(['pst.[[id]]' => (integer)$val]);
                            break;
                        case 'post_text':
                            $main_query->andWhere(['LIKE','pst.[[post_text]]',$val]);
                            break;
                        case 'group_id':
                            $groups_filter_array = explode(':', $val);
                            if (count($groups_filter_array) == 2) {
                                $main_query->andWhere([
                                    '`pst`.[[soc_network_id]]' => $groups_filter_array[0],
                                    '`pst`.[[group_id]]' => $groups_filter_array[1]
                                ]);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            if(!empty($search['events_id'])) {
                $main_query->andWhere(['id' =>
                    (new Query())->select('item_id')
                        ->from('post_events')
                        ->where(['event_id' => $search['events_id'], 'type' => 'post'])
                ]);
            }

        }

        if(!isset($params['PostsSearch']['group_id']) || empty($params['PostsSearch']['group_id'])) {
            $main_query->andWhere([
                '`pst`.[[soc_network_id]]' => 1,
            ])->andWhere([
                'in',
                '`pst`.[[group_id]]',
                (new Query())->select('group_id')
                    ->from('temp_spec_groups')
            ]);
        }

        $result = $main_query->indexBy('id')->all();

        $this->totalCount = $main_query->cache(1)-> count('pst.id');

        return new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => [
                'pageSize' => $this->pageSize,
                'totalCount' => $this->totalCount
            ],
            'sort' => [
                'attributes' => [
                    'id',
                    'comments',
                    'likes',
                    'reposts',
                    'views'
                ]
            ]
        ]);
    }
    /*END Temporary code*/

}