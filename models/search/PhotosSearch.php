<?php

namespace app\models\search;

use app\models\data\ExternalSourcesData;
use app\models\data\Galleries;
use app\models\data\Options;
use app\models\data\PeopleApi;
use app\models\data\Photos;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\db\Expression;

class PhotosSearch extends Photos
{
    public $page = 1;
    public $pageSize = 50;
    public $totalCount = 0;
    public $lastId = 0;
    public $perPage = [
        10 => 10,
        25 => 25,
        50 => 50,
        75 => 75,
        100 => 100,
        500 => 500
    ];

    /*
     * TODO: refactor query for best_person_preview_link, when we will consider how it would be better
     */
    public function searchDetects($params)
    {
        $this->pageSize = $params['per-page'] ?? $this->pageSize;
        $this->page = $params['page'] ?? $this->page;
        $this->lastId = $params['last-id'] ?? $this->lastId;

        $main_query = (new Query())->select([
            'photos.[[id]]',
            'photos.[[people_api]]',
            'photos.[[camera]]',
            'photos.[[date_detect]]',
            'photos.[[preview_link]]',
            'photos.[[o_thumbnail]]',
            'photos.[[confidence]]',
			'photos.[[face_match_id]]',
            'cameras.[[address]]',
            'galleries.[[id]] as gallery_id',
            'galleries.[[title_lat]]',
            'people_api.[[guid]] as people_api_guid',
            'people_api.[[id]] as people_api_id',
            'people_api.[[full_name]] as people_api_full_name',
            'people_api.[[status]] as people_api_status',
            "(
                select concat('/tasks/',galleries.title_lat,'/thumbs/', ph.preview_link)
                from photos as ph
                inner join galleries on ph.gallery_id = galleries.id
                where ph.people_api = photos.people_api and ph.id != photos.id and ph.preview_link != ''
                order by galleries.etalon_gallery desc, abs(ph.score) desc
                limit 1
            ) best_person_photo_preview_link",
            "if((
                select findface.id
                from photos
                inner join findface on findface.photos_id = photos.id
                where photos.people_api = people_api.id and findface.alignment = 'alignment'
                limit 1
            ) is null, 0 ,1) as people_api_has_alignment",
            "(
                select GROUP_CONCAT(DISTINCT galleries.event_id  SEPARATOR ',')
                from photos
                inner join galleries on galleries.id = photos.gallery_id2
                where photos.people_api = people_api.id
            ) as people_api_events",
            "(
                select GROUP_CONCAT(people_api_tags_junction.people_api_tags_id  SEPARATOR ',')
                from people_api_tags_junction
                where people_api_tags_junction.people_api_id = people_api.id
            ) as people_api_tags"
            /*"if((
                SELECT photos.id
                from photos photos_other
                where photos_other.people_api = people_api.id and photos_other.gallery_id != photos.gallery_id
                limit 1
            ) is null, 0 ,1) as people_api_matched"*/
        ]);

        $main_query->from(['photos' => Photos::tableName()]);
        $main_query->leftJoin('people_api', 'people_api.id = photos.people_api');
        $main_query->leftJoin(['external_data' => ExternalSourcesData::tableName()], 'external_data.[[people_api_id]] = photos.[[people_api]]');
        $main_query->leftJoin(['cameras' => 'cameras'], 'cameras.[[code]] = photos.[[camera]]');
        $main_query->innerJoin(['galleries' => Galleries::tableName()], 'galleries.[[id]] = photos.[[gallery_id]]');
        $main_query->where(['not', ['photos.[[date_detect]]' => null]]);
        $main_query->orderBy(['photos.[[id]]' => SORT_DESC]);
        $main_query->groupBy('photos.[[id]]');
        if (isset($params['index-by']) && !empty($params['index-by'])) {
            $main_query->indexBy($params['index-by']);
        }

        if ($this->lastId) {
            $main_query->andWhere(['<', 'photos.[[id]]', $this->lastId]);
        } else {
            $main_query->offset(($this->page - 1) * $this->pageSize);
        }

        if ($params['search-next-after-id']) {
            $main_query->andWhere(['>', 'photos.[[id]]', $params['search-next-after-id']]);
        }

        if (isset($params['date-from']) && !empty($params['date-from'])) {
            $main_query->andWhere(['>','photos.[[date_detect]]',$params['date-from']]);
        }
        if (isset($params['date-to']) && !empty($params['date-to'])) {
            $main_query->andWhere(['<','photos.[[date_detect]]',$params['date-to']]);
        }
        if (isset($params['search']) && !empty($params['search'])) {
            $search = trim($params['search']);
            $main_query->andWhere('photos.[[camera]] LIKE "%'.$search.'%"');
        }
        if (isset($params['gallery_id']) && !empty($params['gallery_id'])) {
            $main_query->andWhere(['photos.[[gallery_id]]'=> $params['gallery_id']]);
        }

        $main_query->limit($this->pageSize);

        $result = $main_query->all();

        $this->totalCount = $main_query->limit($this->pageSize)
            ->offset($this->page * $this->pageSize)
            ->exists()
            ? $this->pageSize * $this->page + 1
            : $this->pageSize * $this->page;

        return new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => [
                'pageSize' => $this->pageSize,
                'totalCount' => $this->totalCount
            ],
            'sort' => [
                'attributes' => [
                    'id',
                    'date_detect'
                ]
            ]
        ]);
    }

    public function countNewDetects($id)
    {
        $main_query = (new Query())->select([
            'COUNT([[id]]) as newItemsCount',
        ]);
        $main_query->from(Photos::tableName());
        $main_query->where(['not', ['[[date_detect]]' => null]]);
        $main_query->andWhere(['>', '[[id]]', $id]);
        $result = $main_query->one();

        return new ArrayDataProvider([
            'allModels' => $result,
        ]);
    }

    public function maxDetectId()
    {
        $main_query = (new Query())->select([
            '[[id]]',
        ]);
        $main_query->from(Photos::tableName());
        $main_query->orderBy(['[[id]]' => SORT_DESC]);
        $main_query->limit(1);
        $result = $main_query->one();

        return $result['id'];
    }

    /**
     * @return ArrayDataProvider
     */
    public function searchNewPhotosForIdentifierPeople()
    {
        $lastId = Options::find()
            ->select('option_value')
            ->where([
                'option_name' => Options::NAME_LAST_PHOTOS_ID,
            ])
            ->scalar();

        $models = $lastId !== false
            ? Photos::find()
                ->alias('ph')
                ->select([
                    'ph.id',
                    'ph.original_link',
                    'ph.people_api',
                    'ph.gallery_id',
                    'ph.gallery_id2',
                    'people_api.main_photo_id',
                ])
                ->innerJoinWith(['peopleApi' => static function (ActiveQuery $q) use ($lastId) {
                    $q->with([
                        'mainPhoto.gallery',
                        'limitedPhotosAndGalleries' => static function (ActiveQuery $activeQuery) use ($lastId) {
                            $activeQuery->andWhere(['<=', 'photos.id', (int)$lastId]);
                        }
                    ]);
                }])
                ->innerJoinWith('gallery g')
                ->where([
                    'people_api.status' => PeopleApi::STATUS_IDENTIFIED,
                ])
                ->andWhere(['is not', 'ph.original_link', null])
                ->andWhere(['is not', 'g.title_lat', null])
                ->andWhere(['>', 'ph.id', (int)$lastId])
                ->orderBy(['ph.id' => SORT_ASC])
                ->limit(1)
                ->all()
            : [];

        return new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => false,
        ]);
    }
}
