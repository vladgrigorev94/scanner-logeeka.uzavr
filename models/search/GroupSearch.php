<?php

namespace app\models\search;

use app\models\data\VkGroups;
use yii\data\ArrayDataProvider;
use yii\db\Query;

class GroupSearch
{
    public function searchRequireGroups()
    {
        $query = (new Query())->select([
            'gr.[[id]]',
            'gr.[[title]]',
            'SUM(gr.[[members]]) as members',
            'gr.[[vk_id]]'
        ])->from(['gr' => VkGroups::tableName()])
            ->groupBy('gr.[[title]]')
            ->where(['watch' => 1]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
            'pagination' => [
                'pageSize' => 7
            ]
        ]);

        return $dataProvider;
    }

    public function searchRequireGroupsApi()
    {
        $query = (new Query())->select([
            'gr.[[id]]',
            'gr.[[title]]',
            'SUM(gr.[[members]]) as members',
            'gr.[[vk_id]]'
        ])->from(['gr' => VkGroups::tableName()])
            ->groupBy('gr.[[title]]')
            ->orderBy(['members' => SORT_DESC])
            ->where(['watch' => 1])
            ->limit(7);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->all()
        ]);

        return $dataProvider;
    }
}