<?php

namespace app\models\search;

use app\models\data\Events;
use app\models\data\Galleries;
use Yii;
use yii\data\ArrayDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Query;

class GalleriesSearch extends Galleries
{
    public $page = 1;
    public $pageSize = 50;
    public $totalCount = 0;

    public function search($params)
    {
        $this->pageSize = $params['per-page'] ?? $this->pageSize;
        $this->page = $params['page'] ?? $this->page;

        $query = (new Query())->from(Galleries::tableName().' AS g')
            ->select([
                'g.`id`',
                'g.`title`',
                'g.`event_id`',
                'g.`cnt_people` as photos_count',
                'g.`cnt_soc` as social_count',
                'e.`title` as event_title'
            ])->innerJoin(Events::tableName().' e','g.`event_id` = e.`id`')
            ->andWhere(['not', ['g.`title`' => '']])
            ->andWhere(['filterable_for_photos' => 1]);

        if (!empty($params['GalleriesSearch'])) {
            $this->load($params);
            if ($this->validate()) {
                if ($this->id) {
                    $query->andFilterWhere([
                        'g.`id`' => $this->id
                    ]);
                }
                if ($this->title) {
                    $query->andFilterWhere([
                        'LIKE',
                        'g.`title`',
                        $this->title
                    ]);
                }
                if ($this->event_id) {
                    $query->andFilterWhere([
                        'g.`event_id`' => $this->event_id
                    ]);
                }
            }
        }

        $this->totalCount = $query->count();
        if ($this->page == 1 && $this->pageSize > $this->totalCount) {
            $this->pageSize = $this->totalCount;
        }

        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'pagination' => [
                'pageSize' => $this->pageSize,
                'totalCount' => $this->totalCount
            ],
            'sort' => [
                'attributes' => [
                    'id',
                    'title',
                    'event_id',
                    'event_title',
                    'photos_count',
                    'social_count',
                ]
            ]
        ]);

        return $dataProvider;
    }

    public function searchDefault()
    {
        $query = (new Query())->from(Galleries::tableName().' g')
            ->select([
                'COUNT(g.`id`) as galleries_count',
                'e.`title`',
                'e.`date`',
                'g.`event_id`'
            ])
            ->innerJoin(Events::tableName().' e', 'e.`id` = g.`event_id`')
            ->andWhere(['not', ['g.`title`' => '']])
            ->andWhere(['filterable_for_photos' => 1])
            ->groupBy('g.`event_id`')
            ->orderBy('e.`date` DESC');

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
            'pagination' => [
                'pageSize' => 9
            ],
            'sort' => false,
        ]);

        return $dataProvider;
    }

    public function searchGraphBlock($filter = array())
    {
        $query = (new Query())->from(Galleries::tableName().' g')
            ->select([
                'g.`event_id`',
                'g.`cnt_people`',
                'g.`cnt_soc`',
                'e.`date`'
            ])
            ->innerJoin(Events::tableName().' e', 'e.`id` = g.`event_id`')
            ->orderBy('e.`date` DESC')
            ->andWhere($filter);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
            'pagination' => [
                'pageSize' => 9
            ],
            'sort' => false,
        ]);

        return $dataProvider;
    }
}