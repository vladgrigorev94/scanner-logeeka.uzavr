<?php

namespace app\helpers;

class Constants
{
    const UTF16_SYMBOLS = [
        "\u00253A",
        "\u00252F",
        "\u00253F",
        "\u00253D",
        "\u002526",
        "\u00257B",
        "\u00257D",
        "\u002522",
        "\u00252C",
        "\u00255C",
        "\u00255D",
        "\u00255B",
        "\\"
    ];

    const UTF8_SYMBOLS = [
        ":",
        "/",
        "?",
        "=",
        "&",
        "{",
        "}",
        "\"",
        ",",
        "\\",
        "]",
        "[",
        ""
    ];
}
