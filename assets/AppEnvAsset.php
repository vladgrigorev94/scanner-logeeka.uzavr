<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppEnvAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/foundation.css?v=0.2',
        'css/jquery.fancybox.min.css?v=0.1',
    ];
    public $js = [
        'js/foundation.js?v=0.3',
        'js/jquery.fancybox.min.js?v=0.1',
        'js/jquery.bpopup.min.js?v=0.1',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset'
    ];
}
